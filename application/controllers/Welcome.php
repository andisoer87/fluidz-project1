<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->library("form_validation");
		$this->load->model("tb_admin_m");
	}

	public function index()
	{
		if($this->session->userdata("logged_in")){
			redirect(base_url("Page_control"));  
		}
		$this->load->view('header_footer/header');
		$this->load->view('nosidebar');
		$this->load->view('login/index');//load view content
		$this->load->view('header_footer/footer');
	}

	public function login()
	{
		$response = array();
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$user = $this->tb_admin_m->login($username, $password)->row();

		if(empty($user)){ // Jika row kosong

			$this->session->set_flashdata("error_message", "User tidak ditemukan");
			redirect(base_url("welcome"));  
		
		}else{

			if(password_verify($password, $user->password)){ //Jika password sama

				$session_data = array(
					'logged_in' => true,
					'user_id' => $user->user_id,
					'user_level' => $user->admin_level
				);

				$this->session->set_userdata($session_data);

				redirect(base_url("Page_control"));
			}else{
				$this->session->set_flashdata("error_message", "Username atau password salah !");
				redirect(base_url("welcome"));  
			}
		}
	}

	public function changePassword()
	{
		$user_id = $this->session->userdata("user_id");

		$old_password = $this->input->post("old_password");
		$new_password = $this->input->post("new_password");
		$new_password_hash = password_hash($new_password, PASSWORD_BCRYPT);

		$user = $this->tb_admin_m->checkPassword($user_id)->row();

		// $this->form_validation->set_rules('new_password', 'Isi passowrd baru', 'trim|required');
		// $this->form_validation->set_rules('new_passwordconf', 'Konfirmasi password baru harus sama', 'trim|required|mathces[new_password]');
		// $this->form_validation->set_rules('old_password', 'Password lama tidak cocok !', )

		if(password_verify($old_password, $user->password)){
		
			if($this->tb_admin_m->updatePassword($new_password_hash, $user_id) > 0){
				redirect(base_url("Page_control"));
			}else{
				//Gagal update password
			}
		
		}else{
			//Password salah
		}
	}

	public function editProfile()
	{
		$user_id = $this->session->userdata("user_id");
		$email = $this->input->post("emailProfileModal");
		$username = $this->input->post("usernameProfileModal");
		if(!empty($_FILES['customFile']['name'])){
			$photo = $this->uploadImage();
		}else if(!empty($this->input->post("oldCustomFile"))){
			$photo = $this->input->post("oldCustomFile");
		}else{
			$photo = null;
		}

		if($this->tb_admin_m->editProfile($user_id, $email, $username, $photo) > 0){
			redirect(base_url("Page_control"));
		}else{
			//Error
			redirect(base_url("Page_control"));
		}
	}

	public function uploadImage()
	{
		$config['upload_path'] = './assets/img_profile/';
		$config['allowed_types'] = 'jpg|png';
		$config['file_name'] = $_FILES['customFile']['name'];
		$config['overwrite'] = true;
		$config['max_size'] = 2048;

		$this->load->library('upload', $config);

		if($this->upload->do_upload('customFile')){
			return $config['upload_path'].$this->upload->data("file_name");
		}
		return null;
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url("welcome"));  
	}
}
