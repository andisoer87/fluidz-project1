<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page_control extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library("form_validation");
		if (!$this->session->userdata("logged_in")) {
			redirect(base_url("welcome"));
		}
		$this->load->model('get_data');
	}

	public function index()
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['page'] = "Dashboard";

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('dashboard/dashboard_index'); //load view content
		$this->load->view('header_footer/footer');
	}

	public function usr_manage($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_admin', 'WHERE is_delete_admin = \'0\'');
		$result['page'] = "Dashboard";

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add User : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit User : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete User : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('user/user_management'); //load view content
		$this->load->view('header_footer/footer');
	}

	public function usr_add()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('user_level', 'User Level', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if ($this->form_validation->run() == FALSE) {

			$last_row = $this->get_data->get_data('tb_admin', 'GROUP BY user_id ORDER BY user_id DESC'); //Select Fuel ID Descending //Dari terbaru or terbesar
			if ($last_row == null) { // Jika data kosong return IN-0001
				$result['user_id'] = 'USR-0001';
			} else {
				$last_user_id = $last_row[0]->user_id; //Ambil fuel_in_id
				$last_user_id_num = preg_split('#(?<=[a-z])-(?=\d)#i', $last_user_id); //Split String between 'PO' dan '000n'
				$last_user_id_num_temp = str_pad($last_user_id_num[1] + 1, 4, 0, STR_PAD_LEFT); // Nilai 000n ditambah 1 = 000n+1, 
				$result['user_id'] = 'USR-' . $last_user_id_num_temp; //Merge dan return string 'PO-000n+1';		
			}

			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "User Management";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('user/user_add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'user_id' => $this->input->post('user_id'),
				'email' => $this->input->post('email'),
				'username' => $this->input->post('username'),
				'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
				'admin_level' => $this->input->post('user_level'),
				'photo' => '',
				'admin_insert_date' => date("Y-m-d H:i:s"),
				'is_delete_admin' => '0'
			];
			$this->db->insert('tb_admin', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create User ( " . $this->input->post('user_level') . " ) dengan ID : " . $this->input->post('user_id');
			$history_year = date("Y");
			$history_refrence = "tb_admin";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/usr_manage/SuccessAdd/' . $this->input->post('user_id'));
		}
	}

	public function usr_edit($admin_id = null)
	{

		$this->form_validation->set_rules('email', 'Email', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('user_level', 'User Level', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['data_user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$admin_id'"); // Load Profile

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('user/user_edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			if(!empty($this->input->post("password"))){
				// echo "with password";
				// die();
				$data = array(
					'email' => $this->input->post('email'),
					'username' => $this->input->post('username'),
					'admin_level' => $this->input->post('user_level'),
					'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT)
				);
				$this->db->where('user_id', $this->input->post('user_id'));
				$this->db->update('tb_admin', $data);
				
			}else{
				// echo "without password";
				// die();
				$data = array(
					'email' => $this->input->post('email'),
					'username' => $this->input->post('username'),
					'admin_level' => $this->input->post('user_level')
				);
				$this->db->where('user_id', $this->input->post('user_id'));
				$this->db->update('tb_admin', $data);
			}
			// $this->db->set('email', $this->input->post('email'));
			// $this->db->set('username', $this->input->post('username'));
			// $this->db->set('admin_level', $this->input->post('user_level'));
			// $this->db->where('user_id', $this->input->post('user_id'));
			// $this->db->update('tb_admin');

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update User ( " . $this->input->post('user_level') . " ) dengan Nomor : " . $this->input->post('user_id');
			$history_year = date("Y");
			$history_refrence = "tb_material";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/usr_manage/SuccessEdit/' . $this->input->post('user_id'));
		}
	}

	public function usr_delete($user_id)
	{
		if ($hasil = $this->get_data->update_data('tb_admin', "is_delete_admin = '1' WHERE user_id = '$user_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Delete user dengan ID : " . $user_id;
			$history_year = date("Y");
			$history_refrence = "tb_admin";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/usr_manage/SuccessDelete/' . $user_id);
		} else {
			//error
		}
	}

	public function dash_datefilter()
	{
		$date_data = [
			'tgl-1' => $this->input->post('date1'),
			'tgl-2' => $this->input->post('date2')
		];
		$this->session->set_userdata($date_data);
		redirect('Page_control');
	}

	public function dash_datereset()
	{
		$this->session->unset_userdata('tgl-1');
		$this->session->unset_userdata('tgl-2');
		redirect('Page_control');
	}

	public function dash_do()
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_sales', 'WHERE is_delete_sales = \'0\'');
		$result['page'] = "Dashboard";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['customer'] = 'notset';
		$result['material'] = 'notset';
		$result['location'] = 'notset';
		$result['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0' ")->row_array();

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('dashboard/list_report'); //load view content
		$this->load->view('header_footer/footer');
	}

	public function do_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$location = $this->input->post('Location');
		$customer = $this->input->post('Customer');
		$material = $this->input->post('Material');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Dashboard";

		if (!empty($location) && !empty($customer)) {
			if (!empty($material) && !empty($to) && !empty($from)) {
				//query lokasi, customer, material, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'  And material_type = '$material'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = $customer;
				$data['material'] = $material;
				$data['location'] = $location;
			}elseif (empty($material) && !empty($to) && !empty($from)) {
				//query lokasi, customer, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = $location;
			}elseif (!empty($material) && empty($to) && empty($from)) {
				//query material, location, customer
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_name_costumer = '$customer' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_name_costumer = '$customer' And material_type = '$material'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = $customer;
				$data['material'] = $material;
				$data['location'] = $location;
			}else {
				//query lokasi dan customer
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_name_costumer = '$customer'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_name_costumer = '$customer'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = $location;
			}

		}elseif (empty($location) && empty($customer)) {
			if (!empty($material) && !empty($to) && !empty($from)) {
				//query material, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_date Between '$from' And '$to' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_date Between '$from' And '$to' And material_type = '$material'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = 'notset';
				$data['material'] = $material;
				$data['location'] = 'notset';
			}elseif (empty($material) && !empty($to) && !empty($from)) {
				//query date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_date Between '$from' And '$to'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_date Between '$from' And '$to'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = 'notset';
				$data['material'] = 'notset';
				$data['location'] = 'notset';

			}elseif (!empty($material) && empty($to) && empty($from)) {
				//query material
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And material_type = '$material' ");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And material_type = '$material' ")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = 'notset';
				$data['material'] = $material;
				$data['location'] = 'notset';
			}else {
				//query all
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0' ")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = 'notset';
				$data['material'] = 'notset';
				$data['location'] = 'notset';
			}
		}elseif (!empty($location) && empty($customer)) {
			if (!empty($material) && !empty($to) && !empty($from)) {
				//query location, material, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_date Between '$to' And '$from' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_date Between '$to' And '$from'  And material_type = '$material'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = 'notset';
				$data['material'] = $material;
				$data['location'] = $location;
			}elseif (empty($material) && !empty($to) && !empty($from)) {
				//query location, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_date Between '$to' And '$from'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_date Between '$to' And '$from'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = 'notset';
				$data['material'] = 'notset';
				$data['location'] = $location;
			}elseif (!empty($material) && empty($to) && empty($from)) {
				//query material, location
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location'  And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And material_type = '$material'" )->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = 'notset';
				$data['material'] = $material;
				$data['location'] = $location;
			}else {
				//query location
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = 'notset';
				$data['material'] = 'notset';
				$data['location'] = $location;
			}
		}elseif (empty($location) && !empty($customer)) {
			if (!empty($material) && !empty($to) && !empty($from)) {
				//query customer, material, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to' And material_type = '$material'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = 'notset';
			}elseif (empty($material) && !empty($to) && !empty($from)) {
				//query customer, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = 'notset';
			}elseif (!empty($material) && empty($to) && empty($from)) {
				//query material, customer
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_name_costumer = '$customer' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_name_costumer = '$customer' And material_type = '$material'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = $customer;
				$data['material'] = $material;
				$data['location'] = 'notset';
			}else {
				//query customer
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_name_costumer = '$customer'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_name_costumer = '$customer'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = 'notset';
			}
		}
		else {
			$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0'");
			$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0' ")->row_array();
			
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['customer'] = 'notset';
			$data['material'] = 'notset';
			$data['location'] = 'notset';
			
		}	

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('dashboard/list_report', $data); //load view content
		$this->load->view('header_footer/footer');
	}

	public function dash_fu()
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_fuel_in', 'WHERE gr_status = "1" && is_delete_fuel_in = \'0\'');
		$result['data1'] = $this->get_data->get_data('tb_fuel_out', 'WHERE is_delete_fuel_out = \'0\'');
		$result['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');
		$result['status1'] = "All";
		$result['page'] = "Dashboard";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['status'] = 'notset';
		$result['supplier'] = 'notset';
		$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' ")->row_array(); 
		$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' ")->row_array();
		$result['total'] = $query['count1'] - $query1['count2'];

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('dashboard/stock_report', $result); //load view content
		$this->load->view('header_footer/footer');
	}
	
	public function fu_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$status = $this->input->post('Status');
		$supplier = $this->input->post('Supplier/Out');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Dashboard";

		if (!empty($status) && empty($supplier)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_status = '$status' And fuel_in_date Between '$from' And '$to' ");
				$data['data1'] = $this->get_data->get_data('tb_fuel_out', "Where is_delete_fuel_out = '0' And fuel_out_status = '$status' And fuel_out_date Between '$from' And '$to' " );
				$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');

				$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_status = '$status' And fuel_in_date Between '$from' And '$to'")->row_array(); 
				$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_status = '$status' And fuel_out_date Between '$from' And '$to'")->row_array();

				$data['total'] = $query['count1'] - $query1['count2'];

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['status'] = $status;
				$data['supplier'] = 'notset';

			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_status = '$status' ");
				$data['data1'] = $this->get_data->get_data('tb_fuel_out', "Where is_delete_fuel_out = '0' And fuel_out_status = '$status'" );
				$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');

				$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_status = '$status'")->row_array(); 
				$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_status = '$status'")->row_array();

				$data['total'] = $query['count1'] - $query1['count2'];
				
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['status'] = $status;
				$data['supplier'] = $supplier;

			}

		}else if (empty($status) && !empty($supplier)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_supplier = '$supplier' And fuel_in_date Between '$from' And '$to' ");
				$data['data1'] = $this->get_data->get_data('tb_fuel_out', "Where is_delete_fuel_out = '0' And fuel_out_name = '$supplier' And fuel_out_date Between '$from' And '$to' " );
				$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');

				$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_supplier = '$supplier' And fuel_in_date Between '$from' And '$to'")->row_array(); 
				$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_name = '$supplier' And fuel_out_date Between '$from' And '$to'")->row_array();

				$data['total'] = $query['count1'] - $query1['count2'];

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['status'] = $status;
				$data['supplier'] = 'notset';

			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_supplier = '$supplier' ");
				$data['data1'] = $this->get_data->get_data('tb_fuel_out', "Where is_delete_fuel_out = '0' And fuel_out_name = '$supplier' " );
				$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');

				$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_supplier = '$supplier'")->row_array(); 
				$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_name = '$supplier' ")->row_array();

				$data['total'] = $query['count1'] - $query1['count2'];
				
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['status'] = $status;
				$data['supplier'] = $supplier;

			}

		}
		else if (empty($status) && empty($supplier)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_date Between '$from' And '$to' ");
				$data['data1'] = $this->get_data->get_data('tb_fuel_out', "Where is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' ");
				$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');
				$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_date Between '$from' And '$to' ")->row_array(); 
				$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to'")->row_array();
				$data['total'] = $query['count1'] - $query1['count2'];

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['status'] = 'notset';
				$data['supplier'] = 'notset';

			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0' ");
				$data['data1'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' ");
				$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');
				$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' ")->row_array(); 
				$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' ")->row_array();
				$data['total'] = $query['count1'] - $query1['count2'];

				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['status'] = 'notset';
				$data['supplier'] = 'notset';
				
			}

		}else if (!empty($status) && !empty($supplier)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_date Between '$from' And '$to' And fuel_in_status = '$status' And fuel_in_supplier = '$supplier'");
				$data['data1'] = $this->get_data->get_data('tb_fuel_out', "Where is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' And fuel_out_status = '$status' And fuel_out_name = '$supplier'");
				$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');
				$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_date Between '$from' And '$to' And fuel_in_status = '$status' And fuel_in_supplier = '$supplier'")->row_array(); 
				$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' And fuel_out_status = '$status' And fuel_out_name = '$supplier'")->row_array();
				$data['total'] = $query['count1'] - $query1['count2'];

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['status'] = 'notset';
				$data['supplier'] = 'notset';

			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_status = '$status' And fuel_in_supplier = '$supplier' ");
				$data['data1'] = $this->get_data->get_data('tb_fuel_out', "Where is_delete_fuel_out = '0' And fuel_out_status = '$status' And fuel_out_name = '$supplier' " );
				$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');

				$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' And fuel_in_status = '$status' And fuel_in_supplier = '$supplier'")->row_array(); 
				$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_status = '$status' And fuel_out_name = '$supplier' ")->row_array();

				$data['total'] = $query['count1'] - $query1['count2'];

				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['status'] = $status;
				$data['supplier'] = $supplier;
				
			}

		}else {
			$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' && is_delete_fuel_in = '0'");
			$data['data1'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' ");
			$data['data2'] = $this->get_data->get_data_rows('tb_fuel_in', '');
			$query = $this->db->query(" SELECT SUM(fuel_in_real) AS count1 FROM tb_fuel_in Where gr_status = '1' && is_delete_fuel_in = '0' ")->row_array(); 
			$query1 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count2 FROM tb_fuel_out Where is_delete_fuel_out = '0' ")->row_array();
			$data['total'] = $query['count1'] - $query1['count2'];

			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['status'] = 'notset';

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('dashboard/stock_report', $data); //load view content
		$this->load->view('header_footer/footer');

	}

	public function dash_ci()
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0'");
		$result['page'] = "Dashboard";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['cust'] = 'notset';
		$result['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And is_date_cash_in = '0'")->row_array();
		$result['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And is_date_cash_in = '0'")->row_array();
		$result['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where is_date_cash_in = '0'")->row_array();

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('dashboard/cashin_report', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function ci_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$cust = $this->input->post('customer');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Dashboard";

		if (!empty($cust)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0' And costumer_name = '$cust' And cash_in_date Between '$from' and '$to'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And costumer_name = '$cust' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And costumer_name = '$cust' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where costumer_name = '$cust' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = $cust;

			}else {
				$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0' And costumer_name = '$cust'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And costumer_name = '$cust' And is_date_cash_in = '0'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And costumer_name = '$cust' And is_date_cash_in = '0'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where costumer_name = '$cust' And is_date_cash_in = '0'")->row_array();
				
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = $cust;

			}

		}else if (empty($cust)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = 'notset';
				

			}else {
				$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And is_date_cash_in = '0'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And is_date_cash_in = '0'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where is_date_cash_in = '0'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = 'notset';
				
			}

		}else {
			$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0'");
			$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And is_date_cash_in = '0'")->row_array();
			$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And is_date_cash_in = '0'")->row_array();
			$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where is_date_cash_in = '0'")->row_array();

			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['cust'] = 'notset';

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('dashboard/cashin_report', $data); //load view content
		$this->load->view('header_footer/footer');
	}

	public function dash_ma()
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0'");
		$result['page'] = "Dashboard";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['cust'] = 'notset';
		$result['start'] = $this->db->query(" SELECT SUM(machine_start) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();
		$result['finish'] = $this->db->query(" SELECT SUM(machine_finish) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();
		$result['usage'] = $this->db->query(" SELECT SUM(machine_range) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('dashboard/machine_report', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function ma_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$cust = $this->input->post('unit');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Dashboard";

		if (!empty($cust)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0' And machine_unit = '$cust' And machine_date Between '$from' and '$to'");			
				$data['start'] = $this->db->query(" SELECT SUM(machine_start) AS count FROM tb_machine Where is_delete_machine = '0' And machine_unit = '$cust' And machine_date Between '$from' and '$to' ")->row_array();
				$data['finish'] = $this->db->query(" SELECT SUM(machine_finish) AS count FROM tb_machine Where is_delete_machine = '0' And machine_unit = '$cust' And machine_date Between '$from' and '$to' ")->row_array();
				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = $cust;

			}else {
				$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0' And machine_unit = '$cust'");
				$data['start'] = $this->db->query(" SELECT SUM(machine_start) AS count FROM tb_machine Where is_delete_machine = '0' And machine_unit = '$cust' ")->row_array();
				$data['finish'] = $this->db->query(" SELECT SUM(machine_finish) AS count FROM tb_machine Where is_delete_machine = '0' And machine_unit = '$cust' ")->row_array();
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = $cust;

			}

		}else if (empty($cust)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0' And machine_date Between '$from' and '$to'");
				$data['start'] = $this->db->query(" SELECT SUM(machine_start) AS count FROM tb_machine Where is_delete_machine = '0' And machine_date Between '$from' and '$to' ")->row_array();
				$data['finish'] = $this->db->query(" SELECT SUM(machine_finish) AS count FROM tb_machine Where is_delete_machine = '0' And machine_date Between '$from' and '$to' ")->row_array();
				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = 'notset';
				

			}else {
				$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0'");
				$data['start'] = $this->db->query(" SELECT SUM(machine_start) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();
				$data['finish'] = $this->db->query(" SELECT SUM(machine_finish) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = 'notset';
				
			}

		}else {
			$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0'");
			$data['start'] = $this->db->query(" SELECT SUM(machine_start) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();
			$data['finish'] = $this->db->query(" SELECT SUM(machine_finish) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();
			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['cust'] = 'notset';

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('dashboard/machine_report', $data); //load view content
		$this->load->view('header_footer/footer');
	}

	public function dash_co()
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0'");
		$result['page'] = "Dashboard";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['cust'] = 'notset';
		$result['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And is_delete_cash_out = '0'")->row_array();
		$result['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And is_delete_cash_out = '0'")->row_array();
		$result['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0'")->row_array();

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('dashboard/cashout_report', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function co_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$cust = $this->input->post('operation');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Dashboard";

		if (!empty($cust)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0' And operation_type = '$cust' And cash_out_date Between '$from' and '$to'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And operation_type = '$cust' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And operation_type = '$cust' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where operation_type = '$cust' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = $cust;

			}else {
				$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0' And operation_type = '$cust'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And operation_type = '$cust' And is_delete_cash_out = '0'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And operation_type= '$cust' And is_delete_cash_out = '0'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where operation_type = '$cust' And is_delete_cash_out = '0'")->row_array();
				
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = $cust;

			}

		}else if (empty($cust)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = 'notset';
				

			}else {
				$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And is_delete_cash_out = '0'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And is_delete_cash_out = '0'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = 'notset';
				
			}

		}else {
			$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0'");
			$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And is_delete_cash_out = '0'")->row_array();
			$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And is_delete_cash_out = '0'")->row_array();
			$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0'")->row_array();

			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['cust'] = 'notset';

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('dashboard/cashout_report', $data); //load view content
		$this->load->view('header_footer/footer');
	}

	public function dash_sa()
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0'");
		$result['data1'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0'");
		$result['page'] = "Dashboard";
		$result['from'] = 'notset';
		$result['to'] = 'notset';

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('dashboard/saldo_report', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function sa_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Dashboard";

		if (!empty($from) && !empty($to)) {
			$data['data1'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'");
			$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'");
			$data['status1'] = 'All';
			$data['from'] = $from;
			$data['to'] = $to;
			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $data);
			$this->load->view('dashboard/saldo_report', $data); //load view content
			$this->load->view('header_footer/footer');
		}else {
			$data['data1'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0'");
			$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0'");
			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $data);
			$this->load->view('dashboard/saldo_report', $data); //load view content
			$this->load->view('header_footer/footer');
		}
	}

	public function ms_list($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['page'] = "Material Sales";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['customer'] = 'notset';
		$result['material'] = 'notset';
		$result['location'] = 'notset';
		$result['data'] = $this->get_data->get_data('tb_sales', 'WHERE is_delete_sales = \'0\'');
		$result['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0' ")->row_array();

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Material Sales : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Material Sales : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('material_sales/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function ms_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$location = $this->input->post('Location');
		$customer = $this->input->post('Customer');
		$material = $this->input->post('Material');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Material Sales";

		if (!empty($location) && !empty($customer)) {
			if (!empty($material) && !empty($to) && !empty($from)) {
				//query lokasi, customer, material, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'  And material_type = '$material'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = $customer;
				$data['material'] = $material;
				$data['location'] = $location;
			}elseif (empty($material) && !empty($to) && !empty($from)) {
				//query lokasi, customer, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = $location;
			}elseif (!empty($material) && empty($to) && empty($from)) {
				//query material, location, customer
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_name_costumer = '$customer' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_name_costumer = '$customer' And material_type = '$material'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = $customer;
				$data['material'] = $material;
				$data['location'] = $location;
			}else {
				//query lokasi dan customer
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_name_costumer = '$customer'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_name_costumer = '$customer'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = $location;
			}

		}elseif (empty($location) && empty($customer)) {
			if (!empty($material) && !empty($to) && !empty($from)) {
				//query material, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_date Between '$from' And '$to' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_date Between '$from' And '$to' And material_type = '$material'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = 'notset';
				$data['material'] = $material;
				$data['location'] = 'notset';
			}elseif (empty($material) && !empty($to) && !empty($from)) {
				//query date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_date Between '$from' And '$to'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_date Between '$from' And '$to'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = 'notset';
				$data['material'] = 'notset';
				$data['location'] = 'notset';

			}elseif (!empty($material) && empty($to) && empty($from)) {
				//query material
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And material_type = '$material' ");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And material_type = '$material' ")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = 'notset';
				$data['material'] = $material;
				$data['location'] = 'notset';
			}else {
				//query all
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0' ")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = 'notset';
				$data['material'] = 'notset';
				$data['location'] = 'notset';
			}
		}elseif (!empty($location) && empty($customer)) {
			if (!empty($material) && !empty($to) && !empty($from)) {
				//query location, material, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_date Between '$from' And '$to' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_date Between '$from' And '$to'  And material_type = '$material'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = 'notset';
				$data['material'] = $material;
				$data['location'] = $location;
			}elseif (empty($material) && !empty($to) && !empty($from)) {
				//query location, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location' And sales_date Between '$from' And '$to'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And sales_date Between '$from' And '$to'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = 'notset';
				$data['material'] = 'notset';
				$data['location'] = $location;
			}elseif (!empty($material) && empty($to) && empty($from)) {
				//query material, location
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location'  And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location' And material_type = '$material'" )->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = 'notset';
				$data['material'] = $material;
				$data['location'] = $location;
			}else {
				//query location
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_location = '$location'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_location = '$location'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = 'notset';
				$data['material'] = 'notset';
				$data['location'] = $location;
			}
		}elseif (empty($location) && !empty($customer)) {
			if (!empty($material) && !empty($to) && !empty($from)) {
				//query customer, material, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to' And material_type = '$material'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = 'notset';
			}elseif (empty($material) && !empty($to) && !empty($from)) {
				//query customer, date
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_name_costumer = '$customer' And sales_date Between '$from' And '$to'")->row_array();
				
				$data['from'] = $from;
				$data['to'] = $to;
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = 'notset';
			}elseif (!empty($material) && empty($to) && empty($from)) {
				//query material, customer
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_name_costumer = '$customer' And material_type = '$material'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_name_costumer = '$customer' And material_type = '$material'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = $customer;
				$data['material'] = $material;
				$data['location'] = 'notset';
			}else {
				//query customer
				$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0' And sales_name_costumer = '$customer'");
				$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0'  And sales_name_costumer = '$customer'")->row_array();
				
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['customer'] = $customer;
				$data['material'] = 'notset';
				$data['location'] = 'notset';
			}
		}
		else {
			$data['data'] = $this->get_data->get_data('tb_sales', "WHERE is_delete_sales = '0'");
			$data['total'] = $this->db->query(" SELECT SUM(sales_amount) AS count FROM tb_sales Where is_delete_sales = '0' ")->row_array();
			
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['customer'] = 'notset';
			$data['material'] = 'notset';
			$data['location'] = 'notset';	
		}	

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('material_sales/list', $data); //load view content
		$this->load->view('header_footer/footer');
	}

	public function ms_add()
	{

		/* Get Last sales_id */
		$last_row = $this->get_data->get_data('tb_sales', 'GROUP BY sales_id ORDER BY sales_id DESC'); //Select Fuel ID Descending //Dari terbaru or terbesar
		if ($last_row == null) { // Jika data kosong return IN-0001
			$result['sales_id'] = 'SLS-0001';
		} else {
			$last_sales_id = $last_row[0]->sales_id; //Ambil fuel_in_id
			$last_sales_id_num = preg_split('#(?<=[a-z])-(?=\d)#i', $last_sales_id); //Split String between 'PO' dan '000n'
			$last_sales_id_num_temp = str_pad($last_sales_id_num[1] + 1, 4, 0, STR_PAD_LEFT); // Nilai 000n ditambah 1 = 000n+1, 
			$result['sales_id'] = 'SLS-' . $last_sales_id_num_temp; //Merge dan return string 'PO-000n+1';		
		}
		/* End of Get Last sales_id */
		/* Get Last do_id */
		$last_row_do = $this->get_data->get_data('tb_sales', 'GROUP BY do_id ORDER BY do_id DESC'); //Select Fuel ID Descending //Dari terbaru or terbesar
		if ($last_row_do == null) { // Jika data kosong return IN-0001
			$result['do_id'] = '0001/' . date("d") . '/' . 'DO' . '/' . date("Y");
		} else {
			$last_do_id = $last_row[0]->do_id; //Ambil fuel_in_id
			$last_do_id_num = preg_split("#/#", $last_do_id); //Split String between 'PO' dan '000n'
			$last_do_id_num_temp = str_pad($last_do_id_num[0] + 1, 4, 0, STR_PAD_LEFT); // Nilai 000n ditambah 1 = 000n+1, 
			$result['do_id'] = $last_sales_id_num_temp . "/" . date("m") . "/" . "DO" . "/" . date("Y"); //Merge dan return string 'PO-000n+1';		
		}
		/* End of Get Last do_id */
		/* Get Customer Type */
		$result["costumer"] = $this->get_data->get_data('tb_costumer', "WHERE is_delete_costumer = '0'");
		$result["material"] = $this->get_data->get_data('tb_material', "WHERE is_delete_material = '0'");

		$user_id = $this->session->userdata("user_id");
		$result['customer'] = $this->get_data->get_data('tb_costumer', "WHERE is_delete_costumer = '0'"); // Load Profile
		$result['material'] = $this->get_data->get_data('tb_material', "WHERE is_delete_material = '0'"); // Load Profile

		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		$this->form_validation->set_rules('material_amount', 'Material Amount', 'required|trim');
		$this->form_validation->set_rules('customer_name', 'Customer Name', 'required|trim');
		$this->form_validation->set_rules('customer_type', 'Customer type', 'required|trim');
		$this->form_validation->set_rules('veh_police_number', 'Vehicle Police Number', 'required|trim');
		$this->form_validation->set_rules('driver_name', 'Driver Name', 'required|trim');
		$this->form_validation->set_rules('material_type', 'Material Type', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Material Sales";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('material_sales/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {

			/* Customer */
			$customer = $this->input->post("customer_name");
			$customer_arr = explode("|", $customer);
			$customer_id = $customer_arr[0];
			$customer_name = $customer_arr[1];
			$customer_type = $customer_arr[2];

			/* Material */
			$material = $this->input->post("material_type");
			$material_arr = explode("|", $material);
			$material_id = $material_arr[0];
			$material_name = $material_arr[1];

			$data = [
				'do_id' => $this->input->post("do_id"),
				'sales_id' => $this->input->post("sales_id"),
				'sales_costumer_id' => $customer_id,
				'sales_date' =>  date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'sales_name_costumer' => $customer_name,
				'sales_amount' => $this->input->post('amount'),
				'sales_type_costumer' => $customer_type,
				'sales_vehicle_number' => $this->input->post("veh_police_number"),
				'sales_driver_name' => $this->input->post("driver_name"),
				'sales_payment' => $this->input->post("payment"),
				'sales_term' => $this->input->post("term_payment"),
				'sales_material_amount' => $this->input->post("material_amount"),
				'sales_material_id' => $material_id,
				'material_type' => $material_name,
				'sales_act_status' => 'DO',
				'sales_status' => '1',
				'sales_insert_date' => date("Y-m-d h:i:s"),
				'is_delete_sales' => '0'

			];
			$this->db->insert('tb_sales', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Sales dengan Nomor : " . $this->input->post("sales_id");
			$history_year = date("Y");
			$history_refrence = "tb_sales";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/ms_list/SuccessAdd/' . $this->input->post("do_id"));
		}
	}

	public function ms_edit($sales_id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_sales', "WHERE sales_id = '$sales_id'");
		$result['costumer'] = $this->get_data->get_data('tb_costumer', "WHERE is_delete_costumer = '0'"); // Load Profile
		$result['location'] = $this->get_data->get_data('tb_location', "WHERE is_delete_pit = '0'"); // Load Profile
		$result['material'] = $this->get_data->get_data('tb_material', "WHERE is_delete_material = '0'"); // Load Profile
		$result['page'] = "Material Sales";

		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		// $this->form_validation->set_rules('due_date', 'Due Date', 'required|trim');
		$this->form_validation->set_rules('customer_name', 'Customer Name', 'required|trim');
		$this->form_validation->set_rules('customer_type', 'Customer type', 'required|trim');
		$this->form_validation->set_rules('veh_police_number', 'Vehicle Police Number', 'required|trim');
		$this->form_validation->set_rules('driver_name', 'Driver Name', 'required|trim');
		$this->form_validation->set_rules('material_type', 'Material Type', 'required|trim');
		$this->form_validation->set_rules('location', 'Sales location', 'required|trim');		
		// $this->form_validation->set_rules('sales_amount', 'Sales Amount', 'required|trim');


		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Material Sales";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('material_sales/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {

			/* Customer */
			$customer = $this->input->post("customer_name");
			$customer_arr = explode("|", $customer);
			$customer_id = $customer_arr[0];
			$customer_name = $customer_arr[1];
			$customer_type = $customer_arr[2];

			/* Material */
			$material = $this->input->post("material_type");
			$material_arr = explode("|", $material);
			$material_id = $material_arr[0];
			$material_name = $material_arr[1];

			$date = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date'))));
			$due_date = date('Y-m-d', strtotime($date . ' + ' . $this->input->post("term_of_payment") . ' days'));

			$data = [
				'sales_costumer_id' => $customer_id,
				'sales_date' =>  date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'sales_due_date' =>  $due_date,
				'sales_name_costumer' => $customer_name,
				'sales_type_costumer' => $customer_type,
				'sales_vehicle_number' => $this->input->post("veh_police_number"),
				'sales_driver_name' => $this->input->post("driver_name"),
				'sales_material_id' => $material_id,
				'material_type' => $material_name,
				'sales_location' => $this->input->post("location"),
				'sales_payment' => $this->input->post("payment"),
				'sales_term' => $this->input->post("term_of_payment"),
				'sales_material_amount' => $this->input->post("material_amount"),
				// 'sales_amount' => $this->input->post("sales_amount")
			];
			$this->db->where('sales_id', $sales_id);
			$this->db->update('tb_sales', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Sales dengan Nomor : " . $sales_id;
			$history_year = date("Y");
			$history_refrence = "tb_sales";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/ms_list/SuccessEdit/' . $sales_id);
		}
	}

	public function ms_release($id = NULL)
	{
		$user_id = $this->session->userdata("user_id");
		$result['data'] = $this->get_data->get_data('tb_sales', "INNER JOIN tb_costumer ON tb_costumer.costumer_id = tb_sales.sales_costumer_id INNER JOIN tb_material ON tb_material.material_id = tb_sales.sales_material_id WHERE tb_sales.sales_id = '$id' LIMIT 1");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['page'] = "Material Sales";

		/* get lats invoice_id */
		$last_row_inv_id = $this->get_data->get_data('tb_invoice', 'GROUP BY invoice_id ORDER BY invoice_id DESC');
		if ($last_row_inv_id == null) {
			$result['inv_id'] = '0001/' . date("m") . '/' . 'INV' . '/' . date("Y");
		} else {
			$last_inv_id = $last_row_inv_id[0]->invoice_id;
			$last_inv_id_num = preg_split("#/#", $last_inv_id);
			$last_inv_id_num_temp = str_pad($last_inv_id_num[0] + 1, 4, 0, STR_PAD_LEFT);
			$result['inv_id'] = $last_inv_id_num_temp . "/" . date("m") . "/" . "INV" . "/" . date("Y");
		}

		/* get last spb_number */
		$last_row_spb = $this->get_data->get_data('tb_invoice', 'GROUP BY spb_number ORDER BY spb_number DESC');
		if ($last_row_spb == null) {
			$result['spb_id'] = '0001/' . date("m") . '/' . 'SPB' . '/' . date("Y");
		} else {
			$last_spb = $last_row_spb[0]->spb_number;
			$last_spb_num = preg_split("#/#", $last_inv_id);
			$last_spb_num_temp = str_pad($last_spb_num[0] + 1, 4, 0, STR_PAD_LEFT);
			$result['spb_id'] = $last_spb_num_temp . "/" . date("m") . "/" . "SPB" . "/" . date("Y");
		}

		if ($result['data'][0]->sales_invoice == null && $result['data'][0]->spb_id == null) {
			$data = array(
				"invoice_id" => $result['inv_id'],
				"do_number" => $result['data'][0]->do_id,
				"spb_number" => $result['spb_id'],
				"invoice_costumer_id" => $result['data'][0]->sales_costumer_id,
				"invoice_sales_id" => $result['data'][0]->sales_id,
				"invoice_date" => date("Y-m-d"),
				"invoice_costumer_name" => $result['data'][0]->sales_name_costumer,
				"invoice_costumer_type" => $result['data'][0]->sales_type_costumer,
				"invoice_address" => $result['data'][0]->address,
				"invoice_city" => $result['data'][0]->city,
				"invoice_telephone" => $result['data'][0]->telephone,
				"invoice_fax" => $result['data'][0]->fax,
				"invoice_police_number" => $result['data'][0]->sales_vehicle_number,
				"invoice_driver_name" => $result['data'][0]->sales_driver_name,
				"invoice_material_id" => $result['data'][0]->sales_material_id,
				"invoice_material_type" => $result['data'][0]->material_type,
				"invoice_location" => $result['data'][0]->sales_location,
				"invoice_payment" => $result['data'][0]->sales_payment,
				"invoice_amount" => $result['data'][0]->sales_amount,
				"invoice_insert_date" => date("Y-m-d h:i:s"),
				"is_delete_invoice" => "0"
			);
			$this->db->insert('tb_invoice', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Invoice dengan Nomor : " . $result['inv_id'];
			$history_year = date("Y");
			$history_refrence = "tb_incoive";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");

			$data = array(
				'sales_invoice' => $result['inv_id'],
				'sales_act_status' => 'SPB, INV',
				'spb_id' => $result['spb_id']
			);
			$this->db->where('sales_id', $result['data'][0]->sales_id);
			$this->db->update('tb_sales', $data);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('material_sales/release', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function fo_in($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0'");
		$result['page'] = "Cash In";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['cust'] = 'notset';

		$result['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And is_date_cash_in = '0'")->row_array();
		$result['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And is_date_cash_in = '0'")->row_array();
		$result['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where is_date_cash_in = '0'")->row_array();

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Cash In : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Cash In : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Cash In : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('finance_operational/cash_in/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function fo_in_add()
	{
		// $this->session->keep_flashdata('invoice');
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['customer'] = $this->get_data->get_data('tb_costumer', "WHERE is_delete_costumer = '0'");
		$result['material'] = $this->get_data->get_data('tb_material', "WHERE is_delete_material = '0'");
		$result['page'] = "Cash In";

		$this->form_validation->set_rules('invoice', 'Invoice', 'required|trim');
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('payment', 'Payment', 'required|trim');
		$this->form_validation->set_rules('material', 'Material', 'required|trim');
		$this->form_validation->set_rules('amount', 'Amount', 'required|trim|numeric');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_cash_in', 'ORDER BY cash_in_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->cash_in_id;
			}
			$user_id = $this->session->userdata("user_id");
			$result['data'] = $data_id;
			$result['data1'] = $this->get_data->get_data('tb_sales', "WHERE sales_invoice IS NOT NULL AND TRIM(sales_invoice) <> '' AND sales_status = '1' and spb_status = '0' AND is_delete_sales = '0'");
			$result['data_invoice'] = $this->get_data->get_data('tb_sales', 'WHERE is_delete_sales = \'0\'');
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Cash In";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('finance_operational/cash_in/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$post = $this->input->post();
			$data = [
				'cash_in_id' => $post['id'],
				'invoice_number' => $post['invoice'],
				'cash_in_date' => date("Y-m-d"),
				'costumer_name' => $post['name'],
				'costumer_type' => $post['type'],
				'material_type' => $post['material'],
				'cash_in_payment' => $post['payment'],
				'cash_in_info' => $post['info'],
				'cash_in_source' => 'invoice',
				'cash_in_amount' => $post['amount']
			];
			$this->db->insert('tb_cash_in', $data);
			$data_update_sales = [
				'sales_status' => '0'
			];
			$this->db->where('sales_invoice', $post['invoice']);
			$this->db->update('tb_sales', $data_update_sales);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Cash In dengan Nomor : " . $this->input->post('id');
			$history_year = date("Y");
			$history_refrence = "tb_cash_in";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/fo_in/SuccessAdd/' . $post['id']);
		}
	}

	public function fo_in_add1()
	{
		// $this->session->keep_flashdata('invoice');
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['customer'] = $this->get_data->get_data('tb_costumer', "WHERE is_delete_costumer = '0'");
		$result['material'] = $this->get_data->get_data('tb_material', "WHERE is_delete_material = '0'");
		$result['page'] = "Cash In";

		$this->form_validation->set_rules('id', 'ID', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_cash_in', 'ORDER BY cash_in_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->cash_in_id;
			}
			$user_id = $this->session->userdata("user_id");
			$result['data'] = $data_id;
			$result['data1'] = $this->get_data->get_data('tb_sales', "WHERE sales_invoice IS NOT NULL AND TRIM(sales_invoice) <> '' AND sales_status = '1' AND is_delete_sales = '0'");
			$result['data2'] = $this->get_data->get_data('tb_balance', "WHERE balance_id IS NOT NULL AND balance_operation = 'cash in' AND is_delete_balance = '0'");
			$result['data_invoice'] = $this->get_data->get_data('tb_sales', 'WHERE is_delete_sales = \'0\'');
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Cash In";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('finance_operational/cash_in/add1', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$post = $this->input->post();
			$date = $post['date'];
			$ready_date = substr($date,6)."-".substr($date,3,2)."-".substr($date,0,2);

			$data = [
				'cash_in_id' => $post['id'],
				'invoice_number' => $post['balance'],
				'cash_in_date' => $ready_date,
				'cash_in_payment' => $post['payment'],
				'cash_in_info' => $post['info'],
				'cash_in_source' => 'balance',
				'cash_in_amount' => $post['amount']
			];
			$this->db->insert('tb_cash_in', $data);
			// $data_update_sales = [
			// 	'sales_status' => '0'
			// ];
			// $this->db->where('sales_invoice', $post['invoice']);
			// $this->db->update('tb_sales', $data_update_sales);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Cash In dengan Nomor : " . $this->input->post('id');
			$history_year = date("Y");
			$history_refrence = "tb_cash_in";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/fo_in/SuccessAdd/' . $post['id']);
		}
	}

	public function fo_in_edit1($id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['cash_in'] = $this->get_data->get_data('tb_cash_in', "WHERE cash_in_id = '$id'");
		$result['data2'] = $this->get_data->get_data('tb_balance', "WHERE balance_id IS NOT NULL AND balance_operation = 'cash in' AND is_delete_balance = '0'");
		$result['page'] = "Cash In";

		$this->form_validation->set_rules('invoice', 'Balance is empty', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('finance_operational/cash_in/edit1', $result); //load view content
			$this->load->view('header_footer/footer');
		}else {
			$post = $this->input->post();
			$data = array(
				'invoice_number' => $post['invoice'],
				'cash_in_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'cash_in_payment' => $post['payment'],
				'cash_in_info' => $post['info'],
				'cash_in_amount' => $post['amount']
			);
			$this->db->where('cash_in_id', $id);
			$this->db->update('tb_cash_in', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Cash In dengan Nomor : " . $id;
			$history_year = date("Y");
			$history_refrence = "tb_cash_in";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/fo_in/SuccessEdit/' . $id);
		}

	}

	public function fo_in_edit($cash_in_id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['cash_in'] = $this->get_data->get_data('tb_cash_in', "WHERE cash_in_id = '$cash_in_id'");
		$result['costumer'] = $this->get_data->get_data('tb_costumer', "WHERE is_delete_costumer = '0'");
		$result['material'] = $this->get_data->get_data('tb_material', "WHERE is_delete_material = '0'");
		$result['data1'] = $this->get_data->get_data('tb_sales', '');
		$result['data_invoice'] = $this->get_data->get_data('tb_sales', '');
		$result['page'] = "Cash In";

		$this->form_validation->set_rules('customer_name', 'Customer Name', 'required|trim');
		$this->form_validation->set_rules('material_type', 'Material Type', 'required|trim');
		$this->form_validation->set_rules('payment_type', 'Payment Type', 'required|trim');
		$this->form_validation->set_rules('amount', 'Amount', 'required|trim|numeric');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('finance_operational/cash_in/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {

			/* Customer */
			$customer = $this->input->post("customer_name");
			$customer_arr = explode("|", $customer);
			$customer_id = $customer_arr[0];
			$customer_name = $customer_arr[1];
			$customer_type = $customer_arr[2];

			/* Material */
			$material = $this->input->post("material_type");
			$material_arr = explode("|", $material);
			$material_id = $material_arr[0];
			$material_name = $material_arr[1];

			$data = array(
				'cash_in_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'cash_in_costumer_id' => $customer_id,
				'costumer_name' => $customer_name,
				'costumer_type' => $customer_type,
				'cash_in_material_id' => $material_id,
				'material_type' => $material_name,
				'cash_in_payment' => $this->input->post("payment_type"),
				'cash_in_info' => $this->input->post("info"),
				'cash_in_amount' => $this->input->post("amount")
			);
			$this->db->where('cash_in_id', $cash_in_id);
			$this->db->update('tb_cash_in', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Cash In dengan Nomor : " . $cash_in_id;
			$history_year = date("Y");
			$history_refrence = "tb_cash_in";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/fo_in/SuccessEdit/' . $cash_in_id);
		}
	}

	public function fo_in_delete($fo_in_id)
	{
		if ($this->get_data->update_data('tb_cash_in', "is_date_cash_in = '1' WHERE cash_in_id = '$fo_in_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Delete Cash In dengan Nomor : " . $fo_in_id;
			$history_year = date("Y");
			$history_refrence = "tb_cash_in";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/fo_in/SuccessDelete/' . $fo_in_id);
		} else {
			//error
		}
	}

	public function fo_in_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$cust = $this->input->post('customer');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Cash In";

		if (!empty($cust)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0' And costumer_name = '$cust' And cash_in_date Between '$from' and '$to'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And costumer_name = '$cust' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And costumer_name = '$cust' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where costumer_name = '$cust' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = $cust;

			}else {
				$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0' And costumer_name = '$cust'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And costumer_name = '$cust' And is_date_cash_in = '0'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And costumer_name = '$cust' And is_date_cash_in = '0'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where costumer_name = '$cust' And is_date_cash_in = '0'")->row_array();
				
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = $cust;

			}

		}else if (empty($cust)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where is_date_cash_in = '0' And cash_in_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = 'notset';
				

			}else {
				$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And is_date_cash_in = '0'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And is_date_cash_in = '0'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where is_date_cash_in = '0'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = 'notset';
				
			}

		}else {
			$data['data'] = $this->get_data->get_data('tb_cash_in', "WHERE is_date_cash_in = '0'");
			$data['kas'] = $this->db->query(" SELECT SUM(cash_in_amount) AS cash FROM tb_cash_in WHERE cash_in_payment = 'Kas' And is_date_cash_in = '0'")->row_array();
			$data['bank'] = $this->db->query(" SELECT SUM(cash_in_amount) AS bank FROM tb_cash_in WHERE cash_in_payment = 'Bank' And is_date_cash_in = '0'")->row_array();
			$data['total']= $this->db->query(" SELECT SUM(cash_in_amount) AS cashout FROM tb_cash_in Where is_date_cash_in = '0'")->row_array();

			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['cust'] = 'notset';

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('finance_operational/cash_in/list', $data); //load view content
		$this->load->view('header_footer/footer');

	}

	public function fo_out($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0'");
		$result['page'] = "Cash Out";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['cust'] = 'notset';

		$result['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And is_delete_cash_out = '0'")->row_array();
		$result['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And is_delete_cash_out = '0'")->row_array();
		$result['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0'")->row_array();

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Cash Out : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Cash In : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Cash In : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('finance_operational/cash_out/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function fo_out_add()
	{
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('amount', 'Amount', 'required|trim|numeric');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_cash_out', 'ORDER BY cash_out_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->cash_out_id;
			}
			$result['data'] = $data_id;
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Cash Out";

			$result['ops_type'] = $this->get_data->get_data('tb_operation_cost', "Where is_delete_cost = '0' ");
			$result['ops_exp'] = $this->get_data->get_data('tb_operation_exp', "Where is_delete_exp = '0' ");
			$result['balance'] = $this->get_data->get_data('tb_balance', "Where balance_operation = 'cash out' && is_delete_balance = '0' ");

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('finance_operational/cash_out/add'); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$post = $this->input->post();
			$com = $post['operation_type'];
			$bom = explode("|", $com);
			$id = $bom[0];
			$ops = $bom[1];
			$data = [
				'cash_out_id' => $post['id'],
				'cash_out_date' => $post['date'],
				'cash_out_operation_id' => $id,
				'operation_type' => $ops,
				'cash_out_payment' => $post['payment'],
				'cash_out_amount' => $post['amount'],
				'cash_out_info' => $post['info']
			];
			$this->db->insert('tb_cash_out', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Cash Out dengan Nomor : " . $this->input->post('id');
			$history_year = date("Y");
			$history_refrence = "tb_cash_out";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/fo_out/SuccessAdd/' . $post['id']);
		}
	}

	public function fo_out_edit($cash_out_id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['cash_out'] = $this->get_data->get_data('tb_cash_out', "WHERE cash_out_id = '$cash_out_id'");
		$result['ops_type'] = $this->get_data->get_data('tb_operation_cost', "Where is_delete_cost = '0' ");
		$result['ops_exp'] = $this->get_data->get_data('tb_operation_exp', "Where is_delete_exp = '0' ");
		$result['balance'] = $this->get_data->get_data('tb_balance', "Where balance_operation = 'cash out' && is_delete_balance = '0' ");

		$result['page'] = "Cash Out";

		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		$this->form_validation->set_rules('operation_type', 'Operation Type', 'required|trim');
		$this->form_validation->set_rules('payment_type', 'Payment Type', 'required|trim');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|numeric');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('finance_operational/cash_out/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {

			/* Operation Type */
			$operation = $this->input->post("operation_type");
			$operation_arr = explode("|", $operation);
			$operation_id = $operation_arr[0];
			$operation_name = $operation_arr[1];

			$data = [
				'cash_out_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'cash_out_operation_id' => $operation_id,
				'operation_type' => $operation_name,
				'cash_out_payment' => $this->input->post('payment_type'),
				'cash_out_amount' => $this->input->post('amount'),
				'cash_out_info' => $this->input->post('additional_info')
			];
			$this->db->where('cash_out_id', $cash_out_id);
			$this->db->update('tb_cash_out', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Edit Cash Out dengan Nomor : " . $cash_out_id;
			$history_year = date("Y");
			$history_refrence = "tb_cash_out";
			$history_action = "Edit";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/fo_out/SuccessEdit/' . $cash_out_id);
		}
	}

	public function fo_out_delete($fo_out_id)
	{
		if ($this->get_data->update_data('tb_cash_out', "is_delete_cash_out = '1' WHERE cash_out_id = '$fo_out_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Delete Cash Out dengan Nomor : " . $fo_out_id;
			$history_year = date("Y");
			$history_refrence = "tb_cash_out";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/fo_out/SuccessAdd/' . $fo_out_id);
		} else {
			//error
		}
	}

	public function fo_out_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$user_id = $this->session->userdata("user_id");
		$cust = $this->input->post('operation');

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Cash Out";

		if (!empty($cust)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0' And operation_type = '$cust' And cash_out_date Between '$from' and '$to'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And operation_type = '$cust' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And operation_type = '$cust' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where operation_type = '$cust' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = $cust;

			}else {
				$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0' And operation_type = '$cust'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And operation_type = '$cust' And is_delete_cash_out = '0'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And operation_type= '$cust' And is_delete_cash_out = '0'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where operation_type = '$cust' And is_delete_cash_out = '0'")->row_array();
				
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = $cust;

			}

		}else if (empty($cust)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0' And cash_out_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = 'notset';
				

			}else {
				$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0'");
				$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And is_delete_cash_out = '0'")->row_array();
				$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And is_delete_cash_out = '0'")->row_array();
				$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = 'notset';
				
			}

		}else {
			$data['data'] = $this->get_data->get_data('tb_cash_out', "WHERE is_delete_cash_out = '0'");
			$data['kas'] = $this->db->query(" SELECT SUM(cash_out_amount) AS cash FROM tb_cash_out WHERE cash_out_payment = 'Kas' And is_delete_cash_out = '0'")->row_array();
			$data['bank'] = $this->db->query(" SELECT SUM(cash_out_amount) AS bank FROM tb_cash_out WHERE cash_out_payment = 'Bank' And is_delete_cash_out = '0'")->row_array();
			$data['total']= $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0'")->row_array();

			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['cust'] = 'notset';

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('finance_operational/cash_out/list', $data); //load view content
		$this->load->view('header_footer/footer');
	}

	public function out_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$unit = $this->input->post('Unit');
		$name = $this->input->post('Name');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Outgoing";

		if (empty($unit) && empty($name)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' ");
				$data['from'] = $from;
				$data['to'] = $to;
				$data['name'] = 'notset';
				$data['unit'] = 'notset';
				$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' ")->row_array();
			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' ");
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['name'] = 'notset';
				$data['unit'] = 'notset';
				$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' ")->row_array();
			}

		}elseif (!empty($unit) && !empty($name)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' And fuel_out_name = '$name' And fuel_out_unit = '$unit' ");
				$data['from'] = $from;
				$data['to'] = $to;
				$data['name'] = $name;
				$data['unit'] = $unit;
				$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' And fuel_out_name = '$name' And fuel_out_unit = '$unit' ")->row_array();
			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' And fuel_out_name = '$name' And fuel_out_unit = '$unit'");
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['name'] = 'notset';
				$data['unit'] = 'notset';
				$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_name = '$name' And fuel_out_unit = '$unit'")->row_array();
			}
							
		}elseif (!empty($unit) && empty($name)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' And fuel_out_unit  = '$unit' ");
				$data['from'] = $from;
				$data['to'] = $to;
				$data['name'] = 'notset';
				$data['unit'] = $unit;
				$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_date Between '$from' And '$to' And fuel_out_unit = '$unit' ")->row_array();

			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' And fuel_out_unit = '$unit' ");
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['name'] = 'notset';
				$data['unit'] = $unit;
				$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_unit = '$unit' And fuel_out_date Between '$from' And '$to' ")->row_array();

			}
		}elseif (empty($unit) && !empty($name)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' And fuel_out_name = '$name' And fuel_out_date Between '$from' And '$to' ");
				$data['from'] = $from;
				$data['to'] = $to;
				$data['name'] = $name;
				$data['unit'] = 'notset';
				$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_name = '$name' And fuel_out_date Between '$from' And '$to' ")->row_array();
			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' And fuel_out_name = '$name' ");
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['name'] = $name;
				$data['unit'] = 'notset';
				$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' And fuel_out_name = '$name' ")->row_array();
			}
							
		}else {
			$data['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' ");
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['name'] = 'notset';
			$data['unit'] = 'notset';
			$data['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' ")->row_array();

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('stocking_fuel/outgoing/list', $data); //load view content
		$this->load->view('header_footer/footer');

	}

	public function cm_add()
	{
		$this->form_validation->set_rules('name', 'Customer Name', 'required|trim');
		$this->form_validation->set_rules('city', 'City', 'required|trim');
		$this->form_validation->set_rules('phone', 'Telephone', 'required|trim|numeric');
		$this->form_validation->set_rules('fax', 'Fax', 'trim|numeric');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_costumer', 'ORDER BY costumer_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->costumer_id;
			}
			$result['data'] = $data_id;
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Customer Master";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/customer_master/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'costumer_id' => $this->input->post('id'),
				'costumer_name' => $this->input->post('name'),
				'costumer_type' => $this->input->post('type'),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'telephone' => $this->input->post('phone'),
				'fax' => $this->input->post('fax'),
				'costumer_info' => $this->input->post('info'),
				'costumer_insert_date' => date("Y-m-d H:i:s"),
				'is_delete_costumer' => '0'
			];
			$this->db->insert('tb_costumer', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Customer dengan Nomor : " . $this->input->post('id');
			$history_year = date("Y");
			$history_refrence = "tb_costumer";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/cm_list/SuccessAdd/' . $this->input->post('id'));
		}
	}

	public function cm_edit($id = null)
	{
		$this->form_validation->set_rules('name', 'Customer Name', 'required|trim');
		$this->form_validation->set_rules('city', 'City', 'required|trim');
		$this->form_validation->set_rules('phone', 'Telephone', 'required|trim|numeric');
		$this->form_validation->set_rules('fax', 'Fax', 'trim|numeric');

		if ($this->form_validation->run() == FALSE) {
			// $data = $this->get_data->get_data('tb_costumer', 'ORDER BY costumer_id DESC LIMIT 1');

			// foreach ($data as $row) {
			// 	$data_id = $row->costumer_id;
			// }
			// $result['data'] = $data_id;
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Customer Master";
			$result['cust'] = $this->db->get_where('tb_costumer', ['costumer_id' => $id])->row_array();

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/customer_master/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'costumer_id' => $this->input->post('id'),
				'costumer_name' => $this->input->post('name'),
				'costumer_type' => $this->input->post('type'),
				'address' => $this->input->post('address'),
				'city' => $this->input->post('city'),
				'telephone' => $this->input->post('phone'),
				'fax' => $this->input->post('fax'),
				'costumer_info' => $this->input->post('info'),
				'costumer_insert_date' => date("Y-m-d H:i:s"),
				'is_delete_costumer' => '0'
			];
			$this->db->set('costumer_name', $this->input->post('name'));
			$this->db->set('costumer_type', $this->input->post('type'));
			$this->db->set('address', $this->input->post('address'));
			$this->db->set('city', $this->input->post('city'));
			$this->db->set('telephone', $this->input->post('phone'));
			$this->db->set('fax', $this->input->post('fax'));
			$this->db->set('costumer_info', $this->input->post('info'));
			$this->db->where('costumer_id', $id);
			$this->db->update('tb_costumer');

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Customer dengan Nomor : " . $id;
			$history_year = date("Y");
			$history_refrence = "tb_costumer";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/cm_list/');
		}
	}

	//Delete customer master
	public function cm_delete($costumer_id = null)
	{
		if ($hasil = $this->get_data->update_data('tb_costumer', "is_delete_costumer = '1' WHERE costumer_id = '$costumer_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Delete Customer dengan Nomor : " . $costumer_id;
			$history_year = date("Y");
			$history_refrence = "tb_costumer";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/cm_list/SuccessDelete/' . $costumer_id);
		} else {
			//error
		}
	}

	public function cm_list($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_costumer', '');
		$result['page'] = "Customer Master";

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Customer Master : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Customer Master : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Customer Master : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('master_data/customer_master/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function mt_add()
	{
		$this->form_validation->set_rules('name', 'Material Name', 'required|trim');
		$this->form_validation->set_rules('price', 'Retail Price', 'required|trim|numeric');
		$this->form_validation->set_rules('spec-price', 'Special Retail Place', 'required|trim|numeric');
		$this->form_validation->set_rules('dist-price', 'Distributor Price', 'required|trim|numeric');
		$this->form_validation->set_rules('stock', 'Stock', 'required|trim|numeric');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_material', 'ORDER BY material_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->material_id;
			}
			$result['data'] = $data_id;
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Material Type";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/materials_type/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'material_id' => $this->input->post('code'),
				'material_name' => $this->input->post('name'),
				'retail_price' => $this->input->post('price'),
				'special_price' => $this->input->post('spec-price'),
				'distributor_Price' => $this->input->post('dist-price'),
				'material_stock' => $this->input->post('stock'),
				'material_info' => $this->input->post('info'),
				'material_status' => 1,
				'material_insert_date' => date("Y-m-d H:i:s"),
				'is_delete_material' => '0'
			];
			$this->db->insert('tb_material', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Material dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_material";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/mt_list/SuccessAdd/' . $this->input->post('code'));
		}
	}

	public function mt_edit($material_id = null)
	{
		$this->form_validation->set_rules('name', 'Material Name', 'required|trim');
		$this->form_validation->set_rules('price', 'Retail Price', 'required|trim|numeric');
		$this->form_validation->set_rules('spec-price', 'Special Retail Place', 'required|trim|numeric');
		$this->form_validation->set_rules('dist-price', 'Distributor Price', 'required|trim|numeric');
		$this->form_validation->set_rules('stock', 'Stock', 'required|trim|numeric');

		$data['material'] = $this->db->get_where('tb_material', ['material_id' => $material_id])->row_array();

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Material Type";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/materials_type/edit', $data); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$this->db->set('material_name', $this->input->post('name'));
			$this->db->set('retail_price', $this->input->post('price'));
			$this->db->set('special_price', $this->input->post('spec-price'));
			$this->db->set('distributor_price', $this->input->post('dist-price'));
			$this->db->set('material_stock', $this->input->post('stock'));
			$this->db->set('material_info', $this->input->post('info'));
			$this->db->where('material_id', $material_id);
			$this->db->update('tb_material');

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Material dengan Nomor : " . $material_id;
			$history_year = date("Y");
			$history_refrence = "tb_material";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/mt_list/SuccessEdit/' . $material_id);
		}
	}

	//Delete Material
	public function mt_delete($material_id = null)
	{
		if ($hasil = $this->get_data->update_data('tb_material', "is_delete_material = '1' WHERE material_id = '$material_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Delete Material dengan Nomor : " . $material_id;
			$history_year = date("Y");
			$history_refrence = "tb_material";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/mt_list/SuccessDelete/' . $material_id);
		} else {
			//error
		}
	}

	public function mt_list($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_material', 'WHERE is_delete_material = "0"');
		$result['page'] = "Material Type";

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Material Type : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Material Type : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Material Type : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('master_data/materials_type/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function oc_add()
	{
		$this->form_validation->set_rules('name', 'Operational Cost Name', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_operation_cost', 'ORDER BY ops_cost_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->ops_cost_id;
			}
			$result['data'] = $data_id;
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Operation Cost";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/operation_cost/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'ops_cost_id' => $this->input->post('code'),
				'ops_cost_name' => $this->input->post('name'),
				'ops_cost_info' => $this->input->post('info'),
				'cost_insert_date' => date("Y-m-d H:i:s"),
				'is_delete_cost' => '0'
			];
			$this->db->insert('tb_operation_cost', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Operation Cost dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_operation_cost";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/oc_list/SuccessAdd/' . $this->input->post('code'));
		}
	}

	public function oc_edit($id = null)
	{
		$this->form_validation->set_rules('name', 'Operational Cost Name', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			// $data = $this->get_data->get_data('tb_operation_cost', 'ORDER BY ops_cost_id DESC LIMIT 1');

			// foreach ($data as $row) {
			// 	$data_id = $row->ops_cost_id;
			// }
			// $result['data'] = $data_id;
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Operation Cost";
			$result['cost'] = $this->db->get_where('tb_operation_cost', ['ops_cost_id' => $id])->row_array();

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/operation_cost/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$this->db->set('ops_cost_name', $this->input->post('name'));
			$this->db->set('ops_cost_info', $this->input->post('info'));
			$this->db->where('ops_cost_id', $id);
			$this->db->update('tb_operation_cost');

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Edit Operation Cost dengan Nomor : " . $id;
			$history_year = date("Y");
			$history_refrence = "tb_operation_cost";
			$history_action = "Edit";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/oc_list');
		}
	}

	//Delete operation cost
	public function oc_delete($ops_cost_id = null)
	{
		if ($hasil = $this->get_data->update_data('tb_operation_cost', "is_delete_cost = '1' WHERE ops_cost_id = '$ops_cost_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $ops_cost_id;
			$history_desc = "Create Operation Cost dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_operation_cost";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/oc_list/SuccessDelete/' . $$ops_cost_id);
		} else {
			//error
		}
	}

	public function oc_list($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_operation_cost', 'WHERE is_delete_cost = "0"');
		$result['page'] = "Operation Cost";

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Operation Cost : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Operation Cost : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Operation Cost : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('master_data/operation_cost/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function oe_add()
	{
		$this->form_validation->set_rules('name', 'Operational Cost Name', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_operation_exp', 'ORDER BY ops_exp_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->ops_exp_id;
			}
			$result['data'] = $data_id;

			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Operation Expenses";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/operation_expenses/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'ops_exp_id' => $this->input->post('code'),
				'ops_exp_name' => $this->input->post('name'),
				'ops_exp_info' => $this->input->post('info'),
				'exp_insert_date' => date("Y-m-d H:i:s"),
				'is_delete_exp' => '0'
			];
			$this->db->insert('tb_operation_exp', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Operation Expenses dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_operation_expenses";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/oe_list/SuccessAdd/' . $this->input->post('code'));
		}
	}

	public function oe_edit($id = null)
	{
		$this->form_validation->set_rules('name', 'Operational Exp. Name', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			// $data = $this->get_data->get_data('tb_operation_exp', 'ORDER BY ops_exp_id DESC LIMIT 1');

			// foreach ($data as $row) {
			// 	$data_id = $row->ops_exp_id;
			// }
			// $result['data'] = $data_id;

			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Operation Expenses";
			$result['exp'] = $this->db->get_where('tb_operation_exp', ['ops_exp_id' => $id])->row_array();

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/operation_expenses/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'ops_exp_id' => $this->input->post('code'),
				'ops_exp_name' => $this->input->post('name'),
				'ops_exp_info' => $this->input->post('info'),
				'exp_insert_date' => date("Y-m-d H:i:s"),
				'is_delete_exp' => '0'
			];
			$this->db->set('ops_exp_name', $this->input->post('name'));
			$this->db->set('ops_exp_info', $this->input->post('info'));
			$this->db->where('ops_exp_id', $id);
			$this->db->update('tb_operation_exp');

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Operation Expenses dengan Nomor : " . $id;
			$history_year = date("Y");
			$history_refrence = "tb_operation_expenses";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/oe_list');
		}
	}

	//Delete operation expenses
	public function oe_delete($ops_exp_id = null)
	{
		if ($hasil = $this->get_data->update_data('tb_operation_exp', "is_delete_exp = '1' WHERE ops_exp_id = '$ops_exp_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Delete Operation Expenses dengan Nomor : " . $ops_exp_id;
			$history_year = date("Y");
			$history_refrence = "tb_operation_expenses";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/oe_list/SuccessDelete/' . $ops_exp_id);
		} else {
			//error
		}
	}

	public function oe_list($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_operation_exp', 'WHERE is_delete_exp = "0"');
		$result['page'] = "Operation Expenses";

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Operation Expenses : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Operation Expenses : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Operation Expenses : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('master_data/operation_expenses/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function inc_add()
	{
		//Should i put these on a method?
		/* Get Last Fuel ID */
		$last_row = $this->get_data->get_data('tb_fuel_in', 'GROUP BY fuel_in_id ORDER BY fuel_in_id DESC'); //Select Fuel ID Descending //Dari terbaru or terbesar
		if ($last_row == null) { // Jika data kosong return IN-0001
			$result['fuel_in_id'] = 'IN-0001';
		} else {
			$last_fuel_in_id = $last_row[0]->fuel_in_id; //Ambil fuel_in_id
			$last_fuel_in_id_num = preg_split('#(?<=[a-z])-(?=\d)#i', $last_fuel_in_id); //Split String between 'PO' dan '000n'
			$last_fuel_in_id_num_temp = str_pad($last_fuel_in_id_num[1] + 1, 4, 0, STR_PAD_LEFT); // Nilai 000n ditambah 1 = 000n+1, 
			$result['fuel_in_id'] = 'IN-' . $last_fuel_in_id_num_temp; //Merge dan return string 'PO-000n+1';		
		}
		/* End of Get Last Fuel ID */
		/* Get Last PO Number */
		$last_row_po = $this->get_data->get_data('tb_fuel_in', 'GROUP BY po_number ORDER BY po_number DESC'); //Podo koyok nduwur :voss, but with po_number
		if ($last_row_po == null) {
			$result['po_number'] = 'PO-0001';
		} else {
			$last_po_number = $last_row_po[0]->po_number;
			$last_po_number_num = preg_split('#(?<=[a-z])-(?=\d)#i', $last_po_number);
			if (sizeof($last_po_number_num) > 1) {
				$last_po_number_num_temp = str_pad($last_po_number_num[1] + 1, 4, 0, STR_PAD_LEFT);
				$result['po_number'] = 'PO-' . $last_po_number_num_temp;
			} else {
				$result['po_number'] = 'PO-0001';
			}
		}
		/* End of Get Last PO Number */

		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		$this->form_validation->set_rules('due_date', 'Due Date', 'required|trim');
		$this->form_validation->set_rules('product_desc', 'Product Description', 'required|trim');
		$this->form_validation->set_rules('supplier', 'Supplier', 'required|trim');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required|trim|numeric');
		$this->form_validation->set_rules('unit_price', 'Unit Price', 'required|trim|numeric');


		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Incoming";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('stocking_fuel/incoming/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'fuel_in_id' => $this->input->post("fuel_in_id"),
				'po_number' => $this->input->post('po_number'),
				'fuel_in_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'due_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('due_date')))),
				'fuel_in_desc' => $this->input->post('product_desc'),
				'fuel_in_supplier' => $this->input->post('supplier'),
				'fuel_in_supplier_address' => $this->input->post('address'),
				'fuel_in_supplier_phone' => $this->input->post('phone'),
				'fuel_in_supplier_fax' => $this->input->post('fax'),
				'fuel_in_quantity' => $this->input->post('quantity'),
				'unit_price' => $this->input->post('unit_price'),
				'fuel_in_total' => $this->input->post("total"),
				'fuel_in_info' => $this->input->post("additional_info"),
				'fuel_in_status' => 'in',
				'fuel_in_insert_date' => date("Y-m-d H:i:s"),
				'is_delete_fuel_in' => '0'
			];
			$this->db->insert('tb_fuel_in', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Incoming dengan Nomor : " . $this->input->post('fuel_in_id');
			$history_year = date("Y");
			$history_refrence = "tb_fuel_in";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/inc_list/SuccessAdd/' . $this->input->post("fuel_in_id"));
		}
	}

	public function inc_edit($fuel_in_id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile

		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		$this->form_validation->set_rules('due_date', 'Due Date', 'required|trim');
		$this->form_validation->set_rules('product_desc', 'Product Description', 'required|trim');
		$this->form_validation->set_rules('supplier', 'Supplier', 'required|trim');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required|trim|numeric');
		$this->form_validation->set_rules('unit_price', 'Unit Price', 'required|trim|numeric');

		$result['fuel_in'] = $this->get_data->get_data('tb_fuel_in', "WHERE fuel_in_id = '$fuel_in_id' ");

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Incoming";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('stocking_fuel/incoming/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'fuel_in_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'due_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('due_date')))),
				'fuel_in_desc' => $this->input->post('product_desc'),
				'fuel_in_supplier' => $this->input->post('supplier'),
				'fuel_in_supplier_address' => $this->input->post('address'),
				'fuel_in_supplier_phone' => $this->input->post('phone'),
				'fuel_in_supplier_fax' => $this->input->post('fax'),
				'fuel_in_quantity' => $this->input->post('quantity'),
				'unit_price' => $this->input->post('unit_price'),
				'fuel_in_total' => $this->input->post("total"),
				'fuel_in_info' => $this->input->post("additional_info")
			];
			$this->db->where('fuel_in_id', $fuel_in_id);
			$this->db->update('tb_fuel_in', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Incoming dengan Nomor : " . $fuel_in_id;
			$history_year = date("Y");
			$history_refrence = "tb_fuel_in";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/inc_list/SuccessEdit/' . $fuel_in_id);
		}
	}

	public function inc_received($fuel_in_id = null)
	{
		if ($this->get_data->update_data('tb_fuel_in', "gr_status = '1' WHERE fuel_in_id = '$fuel_in_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Status Goods Receive Incoming dengan Nomor : " . $fuel_in_id;
			$history_year = date("Y");
			$history_refrence = "tb_fuel_in";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/inc_list');
		}
	}

	public function goods_rec()
	{
		$fuel_in_id = $this->input->post("goods_id");

		$data = array(
			'fuel_in_real' => $this->input->post("goods"),
			'gr_status' => '1'
		);
		$this->db->where('fuel_in_id', $fuel_in_id);
		if($this->db->update('tb_fuel_in', $data)){
			redirect('Page_control/inc_list/SuccessEdit/'.$fuel_in_id);
		}
	}

	public function inc_list($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_fuel_in', 'WHERE is_delete_fuel_in = "0"'); // Load data
		$result['page'] = "Incoming";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['cust'] = 'notset';

		$result['total'] = $this->db->query(" SELECT SUM(fuel_in_real) AS quant FROM tb_fuel_in WHERE gr_status = '1' And is_delete_fuel_in = '0'")->row_array();

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Incoming : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Incoming : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Incoming : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('stocking_fuel/incoming/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function inc_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$cust = $this->input->post('supply');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Incoming";

		if (!empty($cust)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE gr_status = '1' And is_delete_fuel_in = '0' And fuel_in_supplier = '$cust' And fuel_in_date Between '$from' and '$to'");			
				$data['total']= $this->db->query(" SELECT SUM(fuel_in_real) AS quant FROM tb_fuel_in Where gr_status = '1' And fuel_in_supplier = '$cust' And is_delete_fuel_in = '0' And fuel_in_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = $cust;

			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE is_delete_fuel_in = '0' And fuel_in_supplier = '$cust'");
				$data['total']= $this->db->query(" SELECT SUM(fuel_in_real) AS quant FROM tb_fuel_in Where fuel_in_supplier = '$cust' And is_delete_fuel_in = '0'")->row_array();
				
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = $cust;

			}

		}else if (empty($cust)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE is_delete_fuel_in = '0' And fuel_in_date Between '$from' and '$to'");
				$data['total']= $this->db->query(" SELECT SUM(fuel_in_real) AS quant FROM tb_fuel_in Where is_delete_fuel_in = '0' And fuel_in_date Between '$from' and '$to'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = 'notset';
				

			}else {
				$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE is_delete_fuel_in = '0'");
				$data['total']= $this->db->query(" SELECT SUM(fuel_in_real) AS quant FROM tb_fuel_in Where is_delete_fuel_in = '0'")->row_array();

				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = 'notset';
				
			}

		}else {
			$data['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE is_delete_fuel_in = '0'");
			$data['total']= $this->db->query(" SELECT SUM(fuel_in_real) AS quant FROM tb_fuel_in Where is_delete_fuel_in = '0'")->row_array();

			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['cust'] = 'notset';

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('stocking_fuel/incoming/list', $data); //load view content
		$this->load->view('header_footer/footer');
	}

	public function out_add()
	{
		/* Get Last Fuel Out ID */
		$last_row = $this->get_data->get_data('tb_fuel_out', 'GROUP BY fuel_out_id ORDER BY fuel_out_id DESC'); //Select Fuel Out ID Descending //Dari terbaru or terbesar
		if ($last_row == null) { // Jika data kosong return OUT-0001
			$result['fuel_out_id'] = 'OUT-0001';
		} else {
			$last_fuel_out_id = $last_row[0]->fuel_out_id; //Ambil fuel_out_id
			$last_fuel_out_id_num = preg_split('#(?<=[a-z])-(?=\d)#i', $last_fuel_out_id); //Split String between 'OUT' dan '000n'
			$last_fuel_out_id_num_temp = str_pad($last_fuel_out_id_num[1] + 1, 4, 0, STR_PAD_LEFT); // Nilai 000n ditambah 1 = 000n+1, 
			$result['fuel_out_id'] = 'OUT-' . $last_fuel_out_id_num_temp; //Merge dan return string 'OUT-000n+1';		
		}

		$result['unit'] = $this->get_data->get_data('tb_unit', "WHERE is_delete_unit = '0'"); // Load Profile

		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('unit_code', 'Unit Code', 'required|trim');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required|trim|numeric');

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Outgoing";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('stocking_fuel/outgoing/add'); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'fuel_out_id' => $this->input->post("fuel_out_id"),
				'fuel_out_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'fuel_out_name' => $this->input->post('name'),
				'fuel_out_quantity' => $this->input->post('quantity'),
				'fuel_out_unit' => $this->input->post('unit_code'),
				'fuel_out_info' => $this->input->post('additional_info'),
				'fuel_out_status' => 'out',
				'is_delete_fuel_out' => '0'
			];
			$this->db->insert('tb_fuel_out', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Outgoing dengan Nomor : " . $this->input->post('fuel_out_id');
			$history_year = date("Y");
			$history_refrence = "tb_fuel_out";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/out_list/SuccessAdd/' . $this->input->post("fuel_out_id"));
		}
	}

	public function out_edit($fuel_out_id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['unit'] = $this->get_data->get_data('tb_unit', "WHERE is_delete_unit = '0'"); // Load Profile
		$result['page'] = "Outgoing";
		$result['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE fuel_out_id = '$fuel_out_id'");

		$this->form_validation->set_rules('date', 'Date', 'required|trim');
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('unit_code', 'Unit Code', 'required|trim');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required|trim|numeric');

		$result['fuel_out'] = $this->get_data->get_data('tb_fuel_out', "WHERE fuel_out_id = '$fuel_out_id' ");

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Outgoing";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('stocking_fuel/outgoing/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'fuel_out_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'fuel_out_name' => $this->input->post('name'),
				'fuel_out_unit' => $this->input->post('unit_code'),
				'fuel_out_quantity' => $this->input->post('quantity'),
				'fuel_out_info' => $this->input->post('additional_info')
			];
			$this->db->where('fuel_out_id', $fuel_out_id);
			$this->db->update('tb_fuel_out', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Update Outgoing dengan Nomor : " . $fuel_out_id;
			$history_year = date("Y");
			$history_refrence = "tb_fuel_out";
			$history_action = "Update";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/out_list/SuccessEdit/' . $fuel_out_id);
		}
	}

	public function out_list($alert = null, $alertContent = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_fuel_out', "WHERE is_delete_fuel_out = '0' ");
		$result['page'] = "Outgoing";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['name'] = 'notset';
		$result['unit'] = 'notset';
		$result['total'] = $this->db->query(" SELECT SUM(fuel_out_quantity) AS count FROM tb_fuel_out Where is_delete_fuel_out = '0' ")->row_array();

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Outgoing : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Outgoing : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Outgoing : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('stocking_fuel/outgoing/list', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function fuel_out_delete($fuel_out_id = null)
	{
		if ($hasil = $this->get_data->update_data('tb_fuel_out', "is_delete_fuel_out = '1' WHERE fuel_out_id = '$fuel_out_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Delete Outgoing dengan Nomor : " . $fuel_out_id;
			$history_year = date("Y");
			$history_refrence = "tb_fuel_out";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/out_list');
		} else {
			//error
		}
	}

	public function history()
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_history', "INNER JOIN tb_admin ON tb_admin.user_id = tb_history.user_input");


		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('user/history', $result); //load view content
		$this->load->view('header_footer/footer');
	}

	public function location( $alert = null, $alertContent = null){
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "Where user_id = '$user_id'");
		$result['data'] = $this->get_data->get_data('tb_location', "Where is_delete_pit = '0' ");
		$result['page'] = "PIT Location";

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add PIT : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit PIT : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete PIT : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('master_data/pit_location/list', $result); //load view content
		$this->load->view('header_footer/footer');

	}
	
	public function location_add(){
		$this->form_validation->set_rules('name', 'PIT Location', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_location', 'ORDER BY pit_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->pit_id;
			}
			$result['data'] = $data_id;

			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "PIT Location";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/pit_location/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'pit_id' => $this->input->post('code'),
				'pit_name' => $this->input->post('name'),
				'pit_info' => $this->input->post('info'),
				'pit_created_date' => date("Y-m-d H:i:s"),
				'is_delete_pit' => '0'
			];
			$this->db->insert('tb_location', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create PIT location dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_location";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/location/SuccessAdd/' . $this->input->post('code'));
		}
	}

	public function location_edit($pit = null){
		$this->form_validation->set_rules('name', 'PIT Name', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "PIT Location";
			$result['pit'] = $this->db->get_where('tb_location', ['pit_id' => $pit])->row_array();

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/pit_location/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$this->db->set('pit_name', $this->input->post('name'));
			$this->db->set('pit_info', $this->input->post('info'));
			$this->db->where('pit_id', $pit);
			$this->db->update('tb_location');

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Edit PIT Location dengan Nomor : " . $id;
			$history_year = date("Y");
			$history_refrence = "tb_location";
			$history_action = "Edit";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/location');
		}
	}

	public function location_delete($pit_id = null){
		if ($hasil = $this->get_data->update_data('tb_location', "is_delete_pit = '1' WHERE pit_id = '$pit_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $pit_id;
			$history_desc = "Delete PIT location dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_location";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/location/SuccessDelete/' . $$pit_id);
		} else {
			//error
		}
	}

	public function balance( $alert = null, $alertContent = null){
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "Where user_id = '$user_id'");
		$result['data'] = $this->get_data->get_data('tb_balance', "Where is_delete_balance = '0' ");
		$result['page'] = "Balance Sheet";

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add PIT : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit PIT : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete PIT : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('master_data/balance_sheet/list', $result); //load view content
		$this->load->view('header_footer/footer');

	}
	
	public function balance_add(){
		$this->form_validation->set_rules('name', 'Balance name', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_balance', 'ORDER BY balance_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->balance_id;
			}
			$result['data'] = $data_id;

			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Balance Sheet";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/balance_sheet/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = [
				'balance_id' => $this->input->post('code'),
				'balance_operation' => $this->input->post('operation'),
				'balance_name' => $this->input->post('name'),
				'balance_info' => $this->input->post('info'),
				'balance_created_date' => date("Y-m-d H:i:s"),
				'is_delete_balance' => '0'
			];
			$this->db->insert('tb_balance', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Balance Sheet dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_balance";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/balance/SuccessAdd/' . $this->input->post('code'));
		}
	}

	public function balance_edit($balance_id){
		$this->form_validation->set_rules('name', 'Balance Name', 'required|trim');
		$this->form_validation->set_rules('operation', 'Balance Operation', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Balance Sheet";
			$result['balance'] = $this->db->get_where('tb_balance', ['balance_id' => $balance_id])->row_array();

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/balance_sheet/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$this->db->set('balance_name', $this->input->post('name'));
			$this->db->set('balance_info', $this->input->post('info'));
			$this->db->set('balance_operation', $this->input->post('operation'));
			$this->db->where('balance_id', $balance_id);
			$this->db->update('tb_balance');

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Edit Balance Sheet dengan Nomor : " . $balance_id;
			$history_year = date("Y");
			$history_refrence = "tb_balance";
			$history_action = "Edit";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/unit/SuccessEdit/' . $this->input->post('code'));
		}
	}

	public function balance_delete($pit_id = null){
		if ($hasil = $this->get_data->update_data('tb_balance', "is_delete_balance = '1' WHERE balance_id = '$pit_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $pit_id;
			$history_desc = "Delete Balance Sheet dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_balance";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/balance/SuccessDelete/' . $$pit_id);
		} else {
			//error
		}
	}

	public function unit( $alert = null, $alertContent = null){
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "Where user_id = '$user_id'");
		$result['data'] = $this->get_data->get_data('tb_unit', "Where is_delete_unit = '0' ");
		$result['page'] = "Machine Type";

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add Machine Type : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit Machine Type : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete Machine Type : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('master_data/unit_type/list', $result); //load view content
		$this->load->view('header_footer/footer');

	}
	
	public function unit_add(){
		$this->form_validation->set_rules('code', 'Unit machine code', 'required|trim');
		$this->form_validation->set_rules('unit_code', 'Unit code', 'required|trim');
		$this->form_validation->set_rules('owner', 'Unit owner', 'required|trim');
		$this->form_validation->set_rules('year', 'Unit year', 'required|trim');
		$this->form_validation->set_rules('model', 'Unit model', 'required|trim');
		$this->form_validation->set_rules('rate', 'Unit rate', 'required|trim');
		$this->form_validation->set_rules('rate_time', 'Unit time', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_unit', 'ORDER BY unit_machine_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->unit_machine_id;
			}
			$result['data'] = $data_id;

			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Machine Type";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/unit_type/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$unitR = $this->input->post('rate') .' '. $this->input->post('rate_time');
			$data = [
				'unit_machine_id' => $this->input->post('code'),
				'unit_id' => $this->input->post('unit_code'),
				'unit_model' => $this->input->post('model'),
				'unit_year' => $this->input->post('year'),
				'unit_owner' => $this->input->post('owner'),
				'unit_rate' => $unitR,
				'unit_info' => $this->input->post('info'),
				'unit_rental' => $this->input->post('rate'),
				'unit_time' => $this->input->post('rate_time'),
				'unit_created_date' => date("Y-m-d H:i:s"),
				'is_delete_unit' => '0'
			];
			$this->db->insert('tb_unit', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Machine type dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_unit";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/unit/SuccessAdd/' . $this->input->post('code'));
		}
	}

	public function unit_edit($id = null){
		$this->form_validation->set_rules('unit_code', 'Unit code', 'required|trim');
		$this->form_validation->set_rules('model', 'Unit model', 'required|trim');
		$this->form_validation->set_rules('year', 'Unit year', 'required|trim');
		$this->form_validation->set_rules('owner', 'Unit owner', 'required|trim');
		$this->form_validation->set_rules('rate', 'Unit rate', 'required|trim');
		$this->form_validation->set_rules('time', 'Unit time', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Machine Type";
			$result['unit'] = $this->db->get_where('tb_unit', ['unit_machine_id' => $id])->row_array();

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('master_data/unit_type/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$data = array(
				'unit_id' => $this->input->post('unit_code'),
				'unit_model' => $this->input->post('model'),
				'unit_year' => $this->input->post('year'),
				'unit_rate' => $this->input->post('rate') .' '. $this->input->post('time'),
				'unit_owner' => $this->input->post('owner'),
				'unit_rental' => $this->input->post('rate'),
				'unit_time' => $this->input->post('time'),
				'unit_info' => $this->input->post('info')
			);

			$this->db->where('unit_machine_id', $id);
			$this->db->update('tb_unit', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Edit Machine Type dengan Machine Code : " . $id;
			$history_year = date("Y");
			$history_refrence = "tb_unit";
			$history_action = "Edit";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/unit/SuccessEdit/' . $this->input->post('unit_code'));
		}
	}

	public function unit_delete($unit_id = null){
		if ($hasil = $this->get_data->update_data('tb_unit', "is_delete_unit = '1' WHERE unit_machine_id = '$unit_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $unit_id;
			$history_desc = "Delete Machine Type dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_unit";
			$history_action = "Delete";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/unit/SuccessDelete/' . $$unit_id);
		} else {
			//error
		}
	}

	public function machine( $alert = null, $alertContent = null){
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "Where user_id = '$user_id'");
		$result['data'] = $this->get_data->get_data('tb_machine', "Where is_delete_machine = '0' ");
		$result['page'] = "Machine Utilization";
		$result['from'] = 'notset';
		$result['to'] = 'notset';
		$result['cust'] = 'notset';
		$result['start'] = $this->db->query(" SELECT SUM(machine_start) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();
		$result['finish'] = $this->db->query(" SELECT SUM(machine_finish) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();
		$result['usage'] = $this->db->query(" SELECT SUM(machine_range) AS count FROM tb_machine Where is_delete_machine = '0' ")->row_array();

		if ($alert == "SuccessAdd") {
			$this->session->set_flashdata("user_alert", "Success To Add PIT : " . $alertContent);
		} else if ($alert == "SuccessEdit") {
			$this->session->set_flashdata("user_alert", "Success To Edit PIT : " . $alertContent);
		} else if ($alert == "SuccessDelete") {
			$this->session->set_flashdata("user_alert", "Success To Delete PIT : " . $alertContent);
		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $result);
		$this->load->view('machine_util/list', $result); //load view content
		$this->load->view('header_footer/footer');

	}

	public function mac_filter()
	{
		$from = $this->input->post('dateFrom');
		$to = $this->input->post('dateTo');
		$cust = $this->input->post('unit');
		$user_id = $this->session->userdata("user_id");

		$data['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$data['page'] = "Machine Utilization";

		if (!empty($cust)) {
			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0' And machine_unit = '$cust' And machine_date Between '$from' and '$to'");			
				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = $cust;

			}else {
				$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0' And machine_unit = '$cust'");
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = $cust;

			}

		}else if (empty($cust)) {

			if (!empty($from) && !empty($to)) {
				$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0' And machine_date Between '$from' and '$to'");
				$data['status1'] = 'All';
				$data['from'] = $from;
				$data['to'] = $to;
				$data['cust'] = 'notset';
				

			}else {
				$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0'");
				$data['status1'] = 'All';
				$data['from'] = 'notset';
				$data['to'] = 'notset';
				$data['cust'] = 'notset';
				
			}

		}else {
			$data['data'] = $this->get_data->get_data('tb_machine', "WHERE is_delete_machine = '0'");
			$data['status1'] = 'All';
			$data['from'] = 'notset';
			$data['to'] = 'notset';
			$data['cust'] = 'notset';

		}

		$this->load->view('header_footer/header');
		$this->load->view('sidebar', $data);
		$this->load->view('machine_util/list', $data); //load view content
		$this->load->view('header_footer/footer');
	}
	
	public function machine_add(){
		$result['unit'] = $this->get_data->get_data('tb_unit', "WHERE is_delete_unit = '0'"); // Load Profile

		$this->form_validation->set_rules('code', 'Unit machine code', 'required|trim');
		$this->form_validation->set_rules('unit_code', 'Unit code', 'required|trim');
		$this->form_validation->set_rules('date', 'Unit date', 'required|trim');
		$this->form_validation->set_rules('unit_code', 'Unit code', 'required|trim');
		$this->form_validation->set_rules('operator', 'Unit operator', 'required|trim');
		$this->form_validation->set_rules('start', 'Unit start', 'required|trim');
		$this->form_validation->set_rules('finish', 'Unit finish', 'required|trim');
		$this->form_validation->set_rules('usage', 'Unit usage', 'required|trim');
		//$this->form_validation->set_rules('model', 'Unit model', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->get_data->get_data('tb_machine', 'ORDER BY machine_id DESC LIMIT 1');

			foreach ($data as $row) {
				$data_id = $row->machine_id;
			}
			$result['data'] = $data_id;

			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['page'] = "Machine Utilization";

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('machine_util/add', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$unit = $this->input->post('unit_code');
			$bom = explode('|', $unit);
			$model = $bom[1];
			$code = $bom[0];
			$data = [
				'machine_id' => $this->input->post('code'),
				'machine_unit' => $code,
				'machine_model' => $model,
				'machine_date' => date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))),
				'machine_operator' => $this->input->post('operator'),
				'machine_start' => $this->input->post('start'),
				'machine_range' => $this->input->post('usage'),
				'machine_finish' => $this->input->post('finish'),
				'machine_info' => $this->input->post('info'),
				'machine_created_date' => date("Y-m-d H:i:s"),
				'is_delete_machine' => '0'
			];
			$this->db->insert('tb_machine', $data);

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Create Machine utilization dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_machine";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/machine/SuccessAdd/' . $this->input->post('code'));
		}
	}

	public function machine_edit($id = null){
		$result['unit'] = $this->get_data->get_data('tb_unit', "WHERE is_delete_unit = '0'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_machine', "WHERE machine_id = '$id'");
		$result['page'] = "Machine Utilization";

		$this->form_validation->set_rules('unit_code', 'Machine unit code', 'required|trim');
		$this->form_validation->set_rules('date', 'Machine date', 'required|trim');
		//$this->form_validation->set_rules('model', 'Machine model', 'required|trim');
		$this->form_validation->set_rules('operator', 'Machine operator', 'required|trim');
		$this->form_validation->set_rules('finish', 'Machine finish', 'required|trim');
		$this->form_validation->set_rules('start', 'Machine start', 'required|trim');

		if ($this->form_validation->run() == FALSE) {
			$user_id = $this->session->userdata("user_id");
			$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
			$result['machine'] = $this->db->get_where('tb_machine', ['machine_id' => $id])->row_array();

			$this->load->view('header_footer/header');
			$this->load->view('sidebar', $result);
			$this->load->view('machine_util/edit', $result); //load view content
			$this->load->view('header_footer/footer');
		} else {
			$range = $this->input->post('finish') - $this->input->post('start');
			$combine = $this->input->post('unit_code');
			$bom = explode('|', $combine);
			$code = $bom[0];
			$model = $bom[1];
			$this->db->set('machine_date', date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('date')))));
			$this->db->set('machine_unit', $code);
			$this->db->set('machine_model', $model);
			$this->db->set('machine_start', $this->input->post('start'));
			$this->db->set('machine_finish', $this->input->post('finish'));
			$this->db->set('machine_info', $this->input->post('info'));
			$this->db->set('machine_range', $range);
			$this->db->set('machine_operator', $this->input->post('operator'));
			$this->db->where('machine_id', $id);
			$this->db->update('tb_machine');

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $this->session->userdata("user_id");
			$history_desc = "Edit Machine utilization dengan Nomor : " . $id;
			$history_year = date("Y");
			$history_refrence = "tb_machine";
			$history_action = "Edit";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/machine/SuccessEdit/' . $id);
		}
	}

	public function machine_delete($machine_id = null){
		if ($hasil = $this->get_data->update_data('tb_machine', "is_delete_machine = '1' WHERE machine_id = '$machine_id'")) {

			$history_id = "HSTR/" . date("Y-m-d H:i:s");
			$history_user = $ops_cost_id;
			$history_desc = "Delete Machine dengan Nomor : " . $this->input->post('code');
			$history_year = date("Y");
			$history_refrence = "tb_machine";
			$history_action = "Create";
			$history_date = date("Y-m-d H:i:s");

			$this->get_data->save_data("tb_history", "'$history_id','$history_user','$history_desc','$history_year','$history_refrence','$history_action','$history_date'");
			redirect('Page_control/machine/SuccessDelete/' . $$machine_id);
		} else {
			//error
		}
	}

	public function print_do($do_id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$user_data =  $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'");
		$result['officer_name'] = $user_data[0]->username;

		$do_id_n = base64_decode(str_replace('-', '=', str_replace('_', '/', $do_id)));
		$result['data'] = $this->get_data->query_uni("SELECT
														tb_sales.do_id,
														tb_sales.sales_payment,
														tb_costumer.address,
														tb_sales.sales_name_costumer,
														tb_sales.sales_date,
														tb_sales.sales_vehicle_number,
														tb_sales.sales_driver_name,
														tb_sales.material_type,
														tb_sales.sales_location,
														tb_costumer.telephone,
														tb_costumer.fax
													FROM
														tb_sales
														INNER JOIN tb_costumer ON tb_sales.sales_costumer_id = tb_costumer.costumer_id
													WHERE
														tb_sales.do_id = '" . $do_id_n . "'");

		$this->load->view('printout/delivery_order', $result);
	}

	public function print_invoice($id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$user_data =  $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'");
		$result['officer_name'] = $user_data[0]->username;

		$result['data'] = $this->get_data->get_data('tb_sales', "INNER JOIN tb_costumer ON tb_costumer.costumer_id = tb_sales.sales_costumer_id INNER JOIN tb_material ON tb_material.material_id = tb_sales.sales_material_id WHERE tb_sales.sales_id = '$id' LIMIT 1");
		$result['invoice'] = $this->get_data->get_data('tb_invoice', "WHERE invoice_sales_id = '$id'");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile

		$this->load->view('printout/invoice', $result);
	}

	public function print_po($fuel_in_id = null)
	{
		$user_id = $this->session->userdata("user_id");
		$result['user'] = $this->get_data->get_data('tb_admin', "WHERE user_id = '$user_id'"); // Load Profile
		$result['data'] = $this->get_data->get_data('tb_fuel_in', "WHERE fuel_in_id = '$fuel_in_id'");
		$this->load->view('printout/purchase_order', $result);
	}

	public function backupdata()
	{
		$this->load->dbutil();

		$date_now = date('Y-m-d H-i-s');

		$file_prefs = array(
			'format' => 'zip',
			'filename' => 'db_material_backup_'.$date_now.'.sql'
		);

		$backup = $this->dbutil->backup($file_prefs);
		$this->load->helper('file');

		write_file('Database/backup/data_backup_' . $date_now . '.zip', $backup);
		$this->load->helper('download');
		force_download('Database/backup/data_backup_' . $date_now . '.zip', null);
		redirect('Page_control');
	}
}
