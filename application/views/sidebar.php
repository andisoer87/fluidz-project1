<div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url('template/') ?>assets/img/Hanging-Garage-Shelves.jpg">
    <div class="logo">
        <a href="" class="simple-text logo-normal">
            Dev elo /
        </a>
    </div>
    <div class="sidebar-wrapper">
        <?php if($this->session->userdata("user_level") == 'manager'): ?>
        <ul class="nav">
            <li class="nav-item <?php if (@$page == "Dashboard") {
                                    echo "active";
                                } ?>">
                <a class="nav-link" href="<?php echo site_url('Page_control'); ?>">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
        <ul>
        <?php else: ?>
        <ul class="nav">
            <li class="nav-item <?php if (@$page == "Dashboard") {
                                    echo "active";
                                } ?>">
                <a class="nav-link" href="<?php echo site_url('Page_control'); ?>">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item <?php if (@$page == "Material Sales") {
                                    echo "active";
                                } ?>">
                <a class="nav-link" href="<?php echo site_url('Page_control/ms_list'); ?>">
                    <i class="material-icons">receipt</i>
                    <p>Material Sales</p>
                </a>
            </li>
            <li class="nav-item <?php if (@$page == "Finance Operational" || @$page == "Cash In" || @$page == "Cash Out") {
                                    echo "active";
                                } ?>">
                <a class="nav-link" href="#">
                    <i class="material-icons">monetization_on</i>
                    <p>Finance Operational</p>
                </a>

                <ul class="nav sub-nav pl-4">
                    <li class="nav-item <?php if (@$page == "Cash In") {
                                            echo "active";
                                        } ?>">
                        <a class="nav-link" href="<?php echo site_url('Page_control/fo_in'); ?>">
                            <p>Cash In</p>
                        </a>
                    </li>
                    <li class="nav-item <?php if (@$page == "Cash Out") {
                                            echo "active";
                                        } ?>">
                        <a class="nav-link" href="<?php echo site_url('Page_control/fo_out'); ?>">
                            <p>Cash Out</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?php if (@$page == "Stocking Fuel" || @$page == "Incoming" || @$page == "Outgoing") {
                                    echo "active";
                                } ?>">
                <a class="nav-link" href="#">
                    <i class="material-icons">category</i>
                    <p>Stocking Fuel</p>
                </a>

                <ul class="nav sub-nav pl-4">
                    <li class="nav-item <?php if (@$page == "Incoming") {
                                            echo "active";
                                        } ?>">
                        <a class="nav-link" href="<?php echo site_url('Page_control/inc_list'); ?>">
                            <p>Incoming</p>
                        </a>
                    </li>
                    <li class="nav-item <?php if (@$page == "Outgoing") {
                                            echo "active";
                                        } ?>">
                        <a class="nav-link" href="<?php echo site_url('Page_control/out_list'); ?>">
                            <p>Outgoing</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item <?php if (@$page == "Machine Utilization") {
                                    echo "active";
                                } ?>">
                <a class="nav-link" href="<?php echo site_url('Page_control/machine'); ?>">
                    <i class="material-icons">local_shipping</i>
                    <p>Machine Utilization</p>
                </a>
            </li>
            <?php if($this->session->userdata("user_level") == 'admin'): ?>
                <li class="nav-item  <?php if (@$page == "Master Data" || @$page == "Material Type" || @$page == "Operation Cost" || @$page == "Operation Expenses" || @$page == "Customer Master" || @$page =="Machine Type" || @$page == "Balance Sheet" || @$page == "PIT Location") {
                                        echo "active";
                                    } ?>">
                    <a class="nav-link" href="#">
                        <i class="material-icons">folder</i>
                        <p>Master Data</p>
                    </a>

                    <ul class="nav sub-nav pl-4">
                        <li class="nav-item <?php if (@$page == "Material Type") {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?php echo site_url('Page_control/mt_list'); ?>">
                                <p>Material Type</p>
                            </a>
                        </li>
                        <li class="nav-item <?php if (@$page == "Machine Type") {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?php echo site_url('Page_control/unit'); ?>">
                                <p>Machine Type</p>
                            </a>
                        </li>
                        <li class="nav-item <?php if (@$page == "Operation Cost") {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?php echo site_url('Page_control/oc_list'); ?>">
                                <p>Operation Cost</p>
                            </a>
                        </li>
                        <li class="nav-item <?php if (@$page == "Operation Expenses") {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?php echo site_url('Page_control/oe_list'); ?>">
                                <p>Operation Expenses</p>
                            </a>
                        </li>
                        <li class="nav-item <?php if (@$page == "Balance Sheet") {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?php echo site_url('Page_control/balance'); ?>">
                                <p>Balance Sheet</p>
                            </a>
                        </li>                    
                        <li class="nav-item <?php if (@$page == "Customer Master") {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?php echo site_url('Page_control/cm_list'); ?>">
                                <p>Customer Master</p>
                            </a>
                        </li>
                        <li class="nav-item <?php if (@$page == "PIT Location") {
                                                echo "active";
                                            } ?>">
                            <a class="nav-link" href="<?php echo site_url('Page_control/location'); ?>">
                                <p>PIT Location</p>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php else: ?>
            <?php endif; ?>
        </ul>
        <?php endif; ?>
    </div>
</div>

<!-- my account modal -->
<div class="modal fade" id="myAccountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="formMyAccountModal" method="post" action="<?php echo base_url() ?>Welcome/editProfile" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">My Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-profile">
                        <div class="custom-file">
                            <input type="file" style="display: none;" id="customFile" name="customFile" accept="image/*" onchange="previewImage();">
                            <input type="hidden" style="display: none;" id="oldCustomFile" name="oldCustomFile" accept="image/*" value="<?php echo ($user[0]->photo != null ? $user[0]->photo : null); ?>">
                            <label for="customFile">
                                <div class="card-avatar mt-4" style="cursor: pointer">
                                    <img title="Click to change photo" id="imageProfileModal" name="imageProfileModal" class="img" src="<?php echo ($user[0]->photo != null ? base_url() . $user[0]->photo : base_url("template/assets/img/faces/avatar.jpg")); ?>" />
                                </div>
                            </label>
                        </div>
                        <div class="card-body">
                            <table class="table mt-4">
                                <tbody>
                                    <tr>
                                        <td>Username</td>
                                        <td>:</td>
                                        <td><input type="text" class="form-control pl-4 pr-4" name="usernameProfileModal" value="<?php echo $user[0]->username ?>"></td>
                                    </tr>
                                    <tr>
                                        <td>E-mail</td>
                                        <td>:</td>
                                        <td><input type="text" class="form-control pl-4 pr-4" name="emailProfileModal" value="<?php echo $user[0]->email ?>  "></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- changepass modal -->
<div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form" id="formChangePassword" method="post" action="<?php echo base_url() ?>Welcome/changePassword">
                    <p class="description text-center">Input Your Password</p>
                    <?php

                    if ($this->session->flashdata('error_message')) {
                    ?>
                        <div class="alert alert-danger alert-dismissible fade show pr-4" role="alert">
                            <p class="m-0 p-0"><strong>Error!</strong> <?php echo $this->session->flashdata('error_message') ?></p>
                            <button type="button" class="close" style="margin-top: 12px" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php
                    }

                    ?>
                    <div class="card-body">

                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">lock_outline</i></div>
                                </div>
                                <input type="password" name="old_password" placeholder="Old Password..." class="form-control">
                            </div>
                        </div>

                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">lock_outline</i></div>
                                </div>
                                <input type="password" name="new_password" placeholder="New Password..." class="form-control">
                            </div>
                        </div>

                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">lock_outline</i></div>
                                </div>
                                <input type="password" name="new_passwordconf" placeholder="Confirm Password..." class="form-control">
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" form="formChangePassword" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <a class="navbar-brand" href="javascript:;"></a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
                <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="mr-2" style="width: 24px; height: 24px; border-radius: 50%;" src="<?php echo ($user[0]->photo != null ? base_url() . $user[0]->photo : base_url("template/assets/img/faces/avatar.jpg")); ?>" />
                            <?php echo $user[0]->username ?>
                            <i class="material-icons ml-2">keyboard_arrow_down</i>
                            <p class="d-lg-none d-md-block">
                                Account
                            </p>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                            <a class="dropdown-item" data-toggle="modal" href="#myAccountModal">My Account</a>
                            <a class="dropdown-item" data-toggle="modal" href="#changePassword">Change Password</a>
                            <?php if ($this->session->userdata("user_level") == 'admin') : ?>
                                <a class="dropdown-item" href="<?php echo site_url('page_control/usr_manage'); ?>">User Management</a>
                            <?php else : ?>

                            <?php endif; ?>
                            <a class="dropdown-item" href="<?php echo site_url('page_control/history'); ?>">History Activity</a>
                            <a class="dropdown-item" id="backup_data" onclick="return backup_data()" href="<?php echo base_url('page_control/backupdata'); ?>">Backup All Data</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?php echo base_url("welcome/logout") ?>">Log out</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <script type="application/javascript">
        function previewImage() {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("customFile").files[0]);

            oFReader.onload = function(oFREvent) {
                document.getElementById("imageProfileModal").src = oFREvent.target.result;
            };
        }

        function backup_data() {
            if(confirm("Backup data ?")){
                return true;
            }else{
                return false;
            }
        }
    </script>

    <div class="content pt-1">
        <div class="container-fluid">