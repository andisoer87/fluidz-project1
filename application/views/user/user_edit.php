<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add User</h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?php echo base_url()?>Page_control/usr_edit">
                    <table class="table mt-4">
                        <tbody>
                            <?php foreach($data_user as $user){ ?>
                            <tr>
                                <td>ID User</td>
                                <td>:</td>
                                <td><input type="text" name="user_id" value="<?php echo $user->user_id; ?>" class="form-control pl-4 pr-4" placeholder="SLS-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Email </td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="email" name="email" value="<?php echo $user->email; ?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('email') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Username </td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="username" value="<?php echo $user->username; ?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('username') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Password </td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="password" value="" class="form-control pl-4 pr-4" placeholder="Kosongkan field jika tidak ingin mengubah password">
                                    <label class="text text-danger"><?= form_error('password') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>User level</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="user_level" value="<?php echo $user->admin_level; ?>" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="">User Level</option>
                                            <option <?php echo ($user->admin_level == "admin" ? "selected" : "");?> value="admin">Administrator</option>
                                            <option <?php echo ($user->admin_level == "officer" ? "selected" : "");?> value="officer">Officer</option>
                                            <!-- <option <?php echo ($user->admin_level == "guest" ? "selected" : "");?> value="guest">Guest</option> -->
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('user_level') ?></label>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/usr_manage'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <a><button type="submit" class="btn btn-success">Save</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })

    function costumer_change() {
        var costumer_type = $("#costumer_name").val();
        var costumer_typearr = costumer_type.split("|");
        
        $("#customer_type").val(costumer_typearr[2]);
    }
</script>