<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add Balance Sheet</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?= base_url('Page_control/balance_add') ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Balance Sheet Code</td>
                                <td>:</td>
                                <td><input name="code" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="PIT-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Finance Operation</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="operation" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="cash out">Cash Out</option>
                                            <option value="cash in">Cash In</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Balance Sheet Name</td>
                                <td>:</td>
                                <td>
                                    <input name="name" type="text" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3"></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/location'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        whole_string = '<?php echo $data; ?>';
        split_string = whole_string.split(/(\d+)/);
        length_normalisation = parseInt(split_string[1]);
        length_split = ""+length_normalisation+"";
        id_number = parseInt(split_string[1]) + 1;

        if(length_split.length == 1){
            document.getElementById("id-generate").value = 'BS-000'+id_number;
        }else if(length_split.length == 2){
            document.getElementById("id-generate").value = 'BS-00'+id_number;
        }else if(length_split.length == 3){
            document.getElementById("id-generate").value = 'BS-0'+id_number;
        }else{
            document.getElementById("id-generate").value = 'BS-'+id_number;
        }
    });
</script>