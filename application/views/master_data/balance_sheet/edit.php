<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit Balance Sheet</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="POST" action="<?= base_url('Page_control/balance_edit/') . $balance['balance_id'] ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Balance code</td>
                                <td>:</td>
                                <td><input name="code" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="<?= $balance['balance_id'] ?>" readonly></td>
                            </tr>
                            <tr>
                                <td>Finance Operation</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="operation" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option <?php echo $balance['balance_operation'] == 'cash out' ? "selected": ""?> value="cash out">Cash Out</option>
                                            <option <?php echo $balance['balance_operation'] == 'cash in' ? "selected": "";?> value="cash in">Cash In</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Balance name</td>
                                <td>:</td>
                                <td>
                                    <input name="name" type="text" class="form-control pl-4 pr-4" value="<?= $balance['balance_name'] ?>">
                                    <label class="text text-danger"><?= form_error('name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3">
                                            <?= $balance['balance_info'] ?>
                                        </textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/balance'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>