<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit Machine Type</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="POST" action="<?= base_url().'Page_control/unit_edit/'.$unit['unit_machine_id']?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Machine Code</td>
                                <td>:</td>
                                <td><input name="code" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="<?= $unit['unit_machine_id'] ?>" readonly></td>
                            </tr>
                            <tr>
                                <td>Unit Code</td>
                                <td>:</td>
                                <td>
                                    <input name="unit_code" id="id-generate" type="text" class="form-control pl-4 pr-4" value="<?= $unit['unit_id'] ?>">
                                    <label class="text text-danger"><?= form_error('unit_code') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Model</td>
                                <td>:</td>
                                <td>
                                    <input name="model" type="text" class="form-control pl-4 pr-4" value="<?= $unit['unit_model'] ?>">
                                    <label class="text text-danger"><?= form_error('model') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Year</td>
                                <td>:</td>
                                <td>
                                    <input name="year" type="number" class="form-control pl-4 pr-4" value="<?= $unit['unit_year'] ?>">
                                    <label class="text text-danger"><?= form_error('year') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Owner</td>
                                <td>:</td>
                                <td>
                                    <input name="owner" type="text" class="form-control pl-4 pr-4" value="<?= $unit['unit_owner'] ?>">
                                    <label class="text text-danger"><?= form_error('owner') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Rental</td>
                                <td>:</td>
                                <td class="d-inline-flex col-12">
                                    <div class="col5 my-auto">
                                        <input name="rate" type="number" class="form-control pl-4" value="<?= $unit['unit_rental'] ?>">
                                        <label class="text text-danger"><?= form_error('rate') ?></label>
                                    </div>
                                    <div class="col-4 ml-3">
                                        <div class="form-group">
                                            <select name="time" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                                <option <?php echo $unit['unit_time'] == 'hours' ? "selected": ""?> value="hours">Hours</option>
                                                <option <?php echo $unit['unit_time'] == 'days' ? "selected": "";?> value="days">Days</option>
                                                <option <?php echo $unit['unit_time'] == 'weeks' ? "selected": ""?> value="weeks">Weeks</option>
                                                <option <?php echo $unit['unit_time'] == 'months' ? "selected": "";?> value="months">Months</option>
                                            </select>
                                            <label class="text text-danger"><?= form_error('time') ?></label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <textarea name="info" class="form-control pl-4 pr-4" type="text" rows="3">
                                        <?= $unit['unit_info'] ?>
                                    </textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/unit'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>