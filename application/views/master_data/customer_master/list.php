<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">List of customer</h4>
        </div>
        <div class="card-body">
            <!-- header -->
            <div class="col-md-12">

                <?php if ($this->session->flashdata('user_alert')) { ?>
                    <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                        <strong>Success!</strong> <?php echo $this->session->flashdata('user_alert'); ?>
                        <button type="button" class="close" style="margin-top: 12px" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <?php } ?>

                <?php if($this->session->userdata("user_level") == 'admin' || $this->session->userdata("user_level") == 'petugas'): ?>
                <a href="<?php echo site_url('Page_control/cm_add'); ?>"><button type="button" class="btn btn-success">Add Customer</button></a>
                <?php else: ?>
                <a href="#"><button type="button" class="btn btn-default">Add data un-available</button></a>
                <?php endif; ?>
            </div>

            <!-- tabel -->
            <div class="col-md-12">
                <table class="table table-hover mt-4" id="table-cs">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Cust. code</th>
                            <th>Customer name</th>
                            <th>Cust. type</th>
                            <th>Address</th>
                            <th>Information</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        foreach ($data as $row) {
                        ?>
                            <tr height="50">
                                <td><?php echo $i; ?>.</td>
                                <td><?php echo $row->costumer_id; ?></td>
                                <td><?php echo $row->costumer_name; ?></td>
                                <td><?php echo $row->costumer_type; ?></td>
                                <td><?php echo $row->address; ?>, <?php echo $row->city; ?></td>
                                <td><?php echo $row->costumer_info; ?></td>
                                <td class="td-actions text-right">
                                    <?php if($this->session->userdata("user_level") == 'admin' || $this->session->userdata("user_level") == 'petugas'): ?>
                                    <a type="button" rel="tooltip" title="edit" class="btn btn-info text-white" href="<?php echo site_url('Page_control/cm_edit/') . $row->costumer_id; ?>">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a type="button" rel="tooltip" title="delete" class="btn btn-danger text-white" onclick="delete_cm('<?php echo base_url() ?>Page_control/cm_delete/<?php echo $row->costumer_id ?>', '<?php echo $row->costumer_name ?>')">
                                        <i class="material-icons">delete</i>
                                    </a>
                                    <?php else: ?>
                                    <a class="text-center">No action available</a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php
                            $i++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var table = $('#table-cs').DataTable({
            "searching": false,
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
            "order": [
                [1, 'asc']
            ]
        });

        table.on('order.dt search.dt', function() {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1 + '. ';
            });
        }).draw();
    });

    function delete_cm(url, customer_name) {
        if (confirm("Hapus Costumer " + customer_name + "?")) {
            window.location.replace(url);
        }
    }

    $(document).ready(function() {
        var date_input = $('#datefrom'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })

    $(document).ready(function() {
        var date_input = $('#dateto'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>