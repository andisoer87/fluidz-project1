<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit customer master</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="POST" action="<?= base_url('Page_control/cm_edit/') . $cust['costumer_id'] ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>ID</td>
                                <td>:</td>
                                <td><input name="id" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="<?= $cust['costumer_id'] ?>" readonly></td>
                            </tr>
                            <tr>
                                <td>Customer name</td>
                                <td>:</td>
                                <td>
                                    <input name="name" type="text" class="form-control pl-4 pr-4" value="<?= $cust['costumer_name'] ?>">
                                    <label class="text text-danger"><?= form_error('name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Customer type</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="type" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="">--CHOOSE TYPE--</option>
                                            <option <?php echo $cust['costumer_type'] = 'Retail' ? "selected" : ""?> value="Retail">Retail</option>
                                            <option <?php echo $cust['costumer_type'] = 'Special retail' ? "selected" : ""?> value="Special retail">Special Retail</option>
                                            <option <?php echo $cust['costumer_type'] = 'Distributor' ? "selected" : ""?> value="Distributor">Distributor</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="address" class="form-control pl-4 pr-4" rows="3"><?= $cust['address'] ?></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>:</td>
                                <td>
                                    <input name="city" type="text" class="form-control pl-4 pr-4" value="<?= $cust['city'] ?>">
                                    <label class="text text-danger"><?= form_error('city') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Telephone</td>
                                <td>:</td>
                                <td>
                                    <input name="phone" type="text" class="form-control pl-4 pr-4" value="<?= $cust['telephone'] ?>">
                                    <label class="text text-danger"><?= form_error('phone') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Fax (optional)</td>
                                <td>:</td>
                                <td>
                                    <input name="fax" type="text" class="form-control pl-4 pr-4" value="<?= $cust['fax'] ?>">
                                    <label class="text text-danger"><?= form_error('fax') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3"><?= $cust['costumer_info'] ?></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/cm_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>