<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add operation expenses</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?= base_url('Page_control/oe_add') ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Operation exp. code</td>
                                <td>:</td>
                                <td><input name="code" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="OEx-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Operation exp. name</td>
                                <td>:</td>
                                <td>
                                    <input name="name" type="text" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3"></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/oe_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        whole_string = '<?php echo $data; ?>';
        split_string = whole_string.split(/(\d+)/);
        length_normalisation = parseInt(split_string[1]);
        length_split = ""+length_normalisation+"";
        id_number = parseInt(split_string[1]) + 1;

        if(length_split.length == 1){
            document.getElementById("id-generate").value = 'OEX-000'+id_number;
        }else if(length_split.length == 2){
            document.getElementById("id-generate").value = 'OEX-00'+id_number;
        }else if(length_split.length == 3){
            document.getElementById("id-generate").value = 'OEX-0'+id_number;
        }else{
            document.getElementById("id-generate").value = 'OEX-'+id_number;
        }
    });
</script>