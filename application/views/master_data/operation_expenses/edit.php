<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit operation expenses</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="POST" action="<?= base_url('Page_control/oe_edit/') . $exp['ops_exp_id'] ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Operation exp. code</td>
                                <td>:</td>
                                <td><input name="code" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="<?= $exp['ops_exp_id'] ?>" readonly></td>
                            </tr>
                            <tr>
                                <td>Operation exp. name</td>
                                <td>:</td>
                                <td>
                                    <input name="name" type="text" class="form-control pl-4 pr-4" value="<?= $exp['ops_exp_name'] ?>">
                                    <label class="text text-danger"><?= form_error('name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3">
                                            <?= $exp['ops_exp_info'] ?>
                                        </textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/oe_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>