<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add materials type</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?= base_url('Page_control/mt_add') ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Material code</td>
                                <td>:</td>
                                <td><input type="text" name="code" id="id-generate" class="form-control pl-4 pr-4" placeholder="MTR-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Material name</td>
                                <td>:</td>
                                <td>
                                    <input name="name" type="text" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('name'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Retail price</td>
                                <td>:</td>
                                <td>
                                    <input name="price" type="number" min="0" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('price'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Special Retail price</td>
                                <td>:</td>
                                <td>
                                    <input name="spec-price" type="number" min="0" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('spec-price'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Distributor price</td>
                                <td>:</td>
                                <td>
                                    <input name="dist-price" type="number" min="0" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('dist-price'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Stock</td>
                                <td>:</td>
                                <td>
                                    <input name="stock" type="number" min="0" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('stock'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3"></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/mt_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        whole_string = '<?php echo $data; ?>';
        split_string = whole_string.split(/(\d+)/);
        length_normalisation = parseInt(split_string[1]);
        length_split = ""+length_normalisation+"";
        id_number = parseInt(split_string[1]) + 1;

        if(length_split.length == 1){
            document.getElementById("id-generate").value = 'MTR-000'+id_number;
        }else if(length_split.length == 2){
            document.getElementById("id-generate").value = 'MTR-00'+id_number;
        }else if(length_split.length == 3){
            document.getElementById("id-generate").value = 'MTR-0'+id_number;
        }else{
            document.getElementById("id-generate").value = 'MTR-'+id_number;
        }
    });
</script>