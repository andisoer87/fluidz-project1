<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit materials type</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?= base_url('Page_control/mt_edit/') . $material['material_id'] ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Material code</td>
                                <td>:</td>
                                <td><input type="text" class="form-control pl-4 pr-4" placeholder="<?= $material['material_id'] ?>" readonly></td>
                            </tr>
                            <tr>
                                <td>Material name</td>
                                <td>:</td>
                                <td>
                                    <input name="name" type="text" class="form-control pl-4 pr-4" value="<?= $material['material_name'] ?>">
                                    <label class="text text-danger"><?= form_error('name'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Retail price</td>
                                <td>:</td>
                                <td>
                                    <input name="price" type="text" class="form-control pl-4 pr-4" value="<?= $material['retail_price'] ?>">
                                    <label class="text text-danger"><?= form_error('price'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Special Retail price</td>
                                <td>:</td>
                                <td>
                                    <input name="spec-price" type="text" class="form-control pl-4 pr-4" value="<?= $material['special_price'] ?>">
                                    <label class="text text-danger"><?= form_error('spec-price'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Distributor price</td>
                                <td>:</td>
                                <td>
                                    <input name="dist-price" type="text" class="form-control pl-4 pr-4" value="<?= $material['distributor_price'] ?>">
                                    <label class="text text-danger"><?= form_error('dist-price'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Stock</td>
                                <td>:</td>
                                <td>
                                    <input name="stock" type="number" min="0" class="form-control pl-4 pr-4" value="<?= $material['material_stock'] ?>">
                                    <label class="text text-danger"><?= form_error('stock'); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3"><?= $material['material_info'] ?></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/mt_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>