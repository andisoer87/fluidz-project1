<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit stock fuel <strong>(incoming)</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?php echo site_url('Page_control/inc_edit/').$fuel_in[0]->fuel_in_id; ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Incoming number</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $fuel_in[0]->fuel_in_id?>" class="form-control pl-4 pr-4" placeholder="In-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>PO number</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $fuel_in[0]->po_number?>" class="form-control pl-4 pr-4" placeholder="PO-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="date" name="date" value="<?php echo date("d/m/Y", strtotime($fuel_in[0]->fuel_in_date));?>" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY">
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Due Date</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="duedate" name="due_date" value="<?php echo date("d/m/Y", strtotime($fuel_in[0]->due_date));?>" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY">
                                    <label class="text text-danger"><?= form_error('due_date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Product Description</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="product_desc" value="<?php echo $fuel_in[0]->fuel_in_desc?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('product_desc') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Supplier</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="supplier"  value="<?php echo $fuel_in[0]->fuel_in_supplier?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('supplier') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="address" name="address" value="<?php echo $fuel_in[0]->fuel_in_supplier_address?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('address') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="phone" name="phone" value="<?php echo $fuel_in[0]->fuel_in_supplier_phone?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('phone') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Fax</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="fax" name="fax" value="<?php echo $fuel_in[0]->fuel_in_supplier_fax?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('fax') ?></label>
                                </td>
                            </tr>
                            <tr>
                            <tr>
                                <td>Quantity</td>
                                <td>:</td>
                                <td>
                                    <input type="number" id="quantity" name="quantity" value="<?php echo $fuel_in[0]->fuel_in_quantity?>"  value="<?php echo $fuel_in[0]->fuel_in_quantity?>" min="0" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('quantity') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit price</td>
                                <td>:</td>
                                <td>
                                    <input type="text"  id="unit_price" name="unit_price" value="<?php echo $fuel_in[0]->unit_price?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('unit_price') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="total" name="total" value="<?php echo $fuel_in[0]->fuel_in_total?>" class="form-control pl-4 pr-4" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea class="form-control pl-4 pr-4" name="additional_info" rows="3"><?php echo $fuel_in[0]->fuel_in_info?></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/inc_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <a><button type="submit" class="btn btn-success">Save</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })

    /* Count Total */
    var quantity;
    var unit_price;

    document.getElementById('quantity').addEventListener('input', function () {
        quantity = $(this).val();
        countTotal();
    })

    document.getElementById('unit_price').addEventListener('input', function () {
        unit_price = $(this).val();
        countTotal();
    })

    function countTotal() {
        $("#total").val(unit_price * quantity);    
    }
    /* End of count total */
    $(document).ready(function() {
        var date_input = $('#duedate'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>