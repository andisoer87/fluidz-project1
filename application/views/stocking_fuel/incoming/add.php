<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add stock fuel <strong>(incoming)</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?php echo site_url('Page_control/inc_add'); ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Incoming number</td>
                                <td>:</td>
                                <td><input type="text" class="form-control pl-4 pr-4" name="fuel_in_id" value="<?php echo $fuel_in_id?>" placeholder="<?php echo $fuel_in_id?>" readonly></td>
                            </tr>
                            <tr>
                                <td>PO number</td>
                                <td>:</td>
                                <td><input type="text" class="form-control pl-4 pr-4" name="po_number" value="<?php echo $po_number?>" placeholder="<?php echo $po_number?>" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="date" id="date" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY">
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Due Date</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="due_date" id="duedate" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY">
                                    <label class="text text-danger"><?= form_error('due_date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Product Description</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="product_desc" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('product_desc') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Supplier</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="supplier" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('supplier') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Quantity</td>
                                <td>:</td>
                                <td>
                                    <input type="number" id="quantity" name="quantity" min="0" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('quantity') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit price</td>
                                <td>:</td>
                                <td>
                                    <input type="number" id="unit_price" name="unit_price" min="0" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('unit_price') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Total</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="total" name="total" class="form-control pl-4 pr-4" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="address" name="address" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('address') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="phone" name="phone" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('phone') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Fax</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="fax" name="fax" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('fax') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea class="form-control pl-4 pr-4" placeholder="" name="additional_info" rows="3"></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/inc_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <a><button type="submit" class="btn btn-success">Save</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })

    /* Count Total */
    var quantity;
    var unit_price;

    document.getElementById('quantity').addEventListener('input', function () {
        quantity = $(this).val();
        countTotal();
    })

    document.getElementById('unit_price').addEventListener('input', function () {
        unit_price = $(this).val();
        countTotal();
    })

    function countTotal() {
        $("#total").val(unit_price * quantity);    
    }
    /* End of count total */

    $(document).ready(function() {
        var date_input = $('#duedate'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>