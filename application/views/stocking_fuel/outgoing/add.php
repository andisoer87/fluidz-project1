<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add stock fuel <strong>(outgoing)</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Outgoing number</td>
                                <td>:</td>
                                <td><input type="text" name="fuel_out_id" value="<?php echo $fuel_out_id?>" class="form-control pl-4 pr-4" placeholder="In-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="date" name="date" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY">
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="name" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Code</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="unit_code" onchange="costumer_change()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="costumer_name">
                                            <option value="">--CHOOSE UNIT--</option>
                                            <?php

                                                foreach ($unit as $row) {
                                                    ?>
                                                        <option value="<?php echo $row->unit_id?>"><?php echo $row->unit_id?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('customer_name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Quantity</td>
                                <td>:</td>
                                <td>
                                    <input type="number" min="0" name="quantity" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('quantity') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea class="form-control pl-4 pr-4" name="additional_info" rows="3"></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/out_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })

    $(document).ready(function() {
        var date_input = $('#duedate'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>