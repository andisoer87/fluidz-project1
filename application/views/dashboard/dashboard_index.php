<div class="card">
  <div class="card-header card-header-primary">
    <h4 class="card-title">Dashboard</h4>
  </div>
  <div class="card-body">
    <form method="post" action="<?= base_url('Page_control/dash_datefilter') ?>">
      <?php if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) : ?>
        <div class="form-group d-inline-block">
          <input class="form-control" id="datefrom" name="date1" placeholder="Date from" value="<?php echo $this->session->userdata('tgl-1'); ?>" type="text" />
        </div>

        <div class="form-group d-inline-block ml-4">
          <input class="form-control" id="dateto" name="date2" placeholder="Date to" value="<?php echo $this->session->userdata('tgl-2'); ?>" type="text" />
        </div>
      <?php else : ?>
        <div class="form-group d-inline-block">
          <input class="form-control" id="datefrom" name="date1" autocomplete="off" placeholder="Date from" type="text" />
        </div>

        <div class="form-group d-inline-block ml-4">
          <input class="form-control" id="dateto" name="date2" autocomplete="off" placeholder="Date to" type="text" />
        </div>
      <?php endif; ?>

      <div class="form-group d-inline-block">
        &emsp;<button type="submit" class="btn btn-success btn-sm">Submit</button>
      </div>
      <div class="form-group d-inline-block">
        <?php if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) : ?>
          &emsp;<a href="<?= base_url('Page_control/dash_datereset') ?>" class="btn btn-warning btn-sm">Clear Filter</a>
        <?php endif; ?>
      </div>
    </form>
  </div>
</div>

<div class="row d-flex justify-content-center">
  <div class="col-md-4">
    <div class="card card-chart">
      <div class="card-header card-header-primary pb-0">
        <div class="card-icon card-header-warning pb-2 ml-0">
          <i class="material-icons">assignment</i>
        </div>
        <h4 class="card-title">Total DO</h4>
        <p class="category">Report</p>
      </div>
      <div class="card-body">
        <h2 class="text-center">
          <?php
          $tgl1 = $this->session->userdata('tgl-1');
          $tgl2 = $this->session->userdata('tgl-2');
          if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) {
            $query = $this->db->query(" SELECT COUNT(*) AS do FROM tb_sales WHERE is_delete_sales = '0' AND DATE(sales_date) BETWEEN '$tgl1' AND '$tgl2' ")->row_array();
          } else {
            $query = $this->db->query(" SELECT COUNT(*) AS do FROM tb_sales WHERE is_delete_sales = '0'")->row_array();
          }
          echo $query['do'];
          ?>
        </h2>
      </div>
      <div class="card-footer">
        <a href="<?php echo site_url('Page_control/dash_do'); ?>" class="btn btn-success col-12"><i class="material-icons mr-2">info</i>more info...</a>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="card card-chart">
      <div class="card-header card-header-primary pb-0">
        <div class="card-icon card-header-warning pb-2 ml-0">
          <i class="material-icons">bubble_chart</i>
        </div>
        <h4 class="card-title">Stock Fuel</h4>
        <p class="category">Report</p>
      </div>
      <div class="card-body">
        <h2 class="text-center">
          <?php
          $tgl1 = $this->session->userdata('tgl-1');
          $tgl2 = $this->session->userdata('tgl-2');
          if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) {
            $query = $this->db->query(" SELECT SUM(fuel_in_real) AS fuelin FROM tb_fuel_in WHERE is_delete_fuel_in = '0' AND DATE(fuel_in_date) BETWEEN '$tgl1' AND '$tgl2' ")->row_array();
          } else {
            $query = $this->db->query(" SELECT SUM(fuel_in_real) AS fuelin FROM tb_fuel_in WHERE is_delete_fuel_in = '0'")->row_array();
          }
          if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) {
            $query2 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS fuelout FROM tb_fuel_out WHERE is_delete_fuel_out = '0' AND DATE(fuel_out_date) BETWEEN '$tgl1' AND '$tgl2' ")->row_array();
          } else {
            $query2 = $this->db->query(" SELECT SUM(fuel_out_quantity) AS fuelout FROM tb_fuel_out WHERE is_delete_fuel_out = '0'")->row_array();
          }
          $fuel_ready = $query['fuelin'] - $query2['fuelout'];
          echo idr_format($fuel_ready) . "<strong> L</strong>";
          ?>
        </h2>
      </div>
      <div class="card-footer">
        <a href="<?php echo site_url('Page_control/dash_fu'); ?>" class="btn btn-success col-12"><i class="material-icons mr-2">info</i>more info...</a>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="card card-chart">
      <div class="card-header card-header-primary pb-0">
        <div class="card-icon card-header-warning pb-2 ml-0">
          <i class="material-icons">local_shipping</i>
        </div>
        <h4 class="card-title">Machine Utilization</h4>
        <p class="category">Report </p>
      </div>
      <div class="card-body">
        <h2 class="text-center">
        <?php
          $tgl1 = $this->session->userdata('tgl-1');
          $tgl2 = $this->session->userdata('tgl-2');
          if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) {
            $query = $this->db->query(" SELECT SUM(machine_range) AS range_unit FROM tb_machine WHERE is_delete_machine = '0' AND DATE(machine_date) BETWEEN '$tgl1' AND '$tgl2'")->row_array();
          } else {
            $query = $this->db->query(" SELECT SUM(machine_range) AS range_unit FROM tb_machine WHERE is_delete_machine = '0'")->row_array();
          }
          echo idr_format($query['range_unit']) . "<strong> Km</strong>";
          ?>
        </h2>
      </div>
      <div class="card-footer">
        <a href="<?php echo site_url('Page_control/dash_ma'); ?>" class="btn btn-success col-12"><i class="material-icons mr-2">info</i>more info...</a>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="card card-chart">
      <div class="card-header card-header-primary pb-0">
        <div class="card-icon card-header-warning pb-2 ml-0">
          <i class="material-icons">attach_money</i>
        </div>
        <h4 class="card-title">Cash In</h4>
        <p class="category">Report (IDR)</p>
      </div>
      <div class="card-body">
        <h2 class="text-center">
          <?php
          $tgl1 = $this->session->userdata('tgl-1');
          $tgl2 = $this->session->userdata('tgl-2');
          if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) {
            $query = $this->db->query(" SELECT SUM(cash_in_amount) AS cashin FROM tb_cash_in WHERE is_date_cash_in = '0' AND DATE(cash_in_date) BETWEEN '$tgl1' AND '$tgl2' ")->row_array();
          } else {
            $query = $this->db->query(" SELECT SUM(cash_in_amount) AS cashin FROM tb_cash_in WHERE is_date_cash_in = '0'")->row_array();
          }
          echo "<strong>Rp. </strong>" . idr_format($query['cashin']);
          ?>
        </h2>
      </div>
      <div class="card-footer">
        <a href="<?php echo site_url('Page_control/dash_ci'); ?>" class="btn btn-success col-12"><i class="material-icons mr-2">info</i>more info...</a>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="card card-chart">
      <div class="card-header card-header-primary pb-0">
        <div class="card-icon card-header-warning pb-2 ml-0">
          <i class="material-icons">attach_money</i>
        </div>
        <h4 class="card-title">Cash Out</h4>
        <p class="category">Report (IDR)</p>
      </div>
      <div class="card-body">
        <h2 class="text-center">
        <?php
          $tgl1 = $this->session->userdata('tgl-1');
          $tgl2 = $this->session->userdata('tgl-2');
          if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) {
            $query = $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out WHERE is_delete_cash_out = '0' AND DATE(cash_out_date) BETWEEN '$tgl1' AND '$tgl2' ")->row_array();
          } else {
            $query = $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out WHERE is_delete_cash_out = '0'")->row_array();
          }
          echo "<strong>Rp. </strong>" . idr_format($query['cashout']);
          ?>
        </h2>
      </div>
      <div class="card-footer">
        <a href="<?php echo site_url('Page_control/dash_co'); ?>" class="btn btn-success col-12"><i class="material-icons mr-2">info</i>more info...</a>
      </div>
    </div>
  </div>

  <div class="col-md-4">
    <div class="card card-chart">
      <div class="card-header card-header-primary pb-0">
        <div class="card-icon card-header-warning pb-2 ml-0">
          <i class="material-icons">attach_money</i>
        </div>
        <h4 class="card-title">Saldo</h4>
        <p class="category">Report </p>
      </div>
      <div class="card-body">
        <h2 class="text-center">
        <?php
          $tgl1 = $this->session->userdata('tgl-1');
          $tgl2 = $this->session->userdata('tgl-2');
          if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) {
            $query = $this->db->query(" SELECT SUM(cash_in_amount) AS cashin FROM tb_cash_in WHERE is_date_cash_in = '0' AND DATE(cash_in_date) BETWEEN '$tgl1' AND '$tgl2'")->row_array();
          } else {
            $query = $this->db->query(" SELECT SUM(cash_in_amount) AS cashin FROM tb_cash_in WHERE is_date_cash_in = '0'")->row_array();
          }
          if ($this->session->userdata('tgl-1') != NULL && $this->session->userdata('tgl-2') != NULL) {
            $query2 = $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out WHERE is_delete_cash_out = '0' AND DATE(cash_out_date) BETWEEN '$tgl1' AND '$tgl2' ")->row_array();
          } else {
            $query2 = $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out WHERE is_delete_cash_out = '0'")->row_array();
          }
          echo "<strong>Rp. </strong>" . idr_format($query['cashin'] - $query2['cashout']);
          ?>
        </h2>
      </div>
      <div class="card-footer">
        <a href="<?php echo site_url('Page_control/dash_sa'); ?>" class="btn btn-success col-12"><i class="material-icons mr-2">info</i>more info...</a>
      </div>
    </div>
  </div>

</div>

<script>
  $(document).ready(function() {
    var date_input = $('#datefrom'); //our date input has the name "date"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

    var options = {
      format: 'yyyy-mm-dd',
      container: container,
      todayHighlight: true,
      autoclose: true,
      orientation: "top auto",
    };
    date_input.datepicker(options);
  })

  $(document).ready(function() {
    var date_input = $('#dateto'); //our date input has the name "date"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

    var options = {
      format: 'yyyy-mm-dd',
      container: container,
      todayHighlight: true,
      autoclose: true,
      orientation: "top auto",
    };
    date_input.datepicker(options);
  })
</script>