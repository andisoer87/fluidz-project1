<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Saldo Report</h4>
        </div>
        <div class="card-body">
            <!-- tabel -->
            <div class="col-md-12">
                <table class="table table-hover mt-4" id="table-saldo-report">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>No. Cash In/Out</th>
                            <th>Cash In</th>
                            <th>Cash Out</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data as $row){ ?>
                        <tr height="50">
                            <td></td>
                            <td><?php echo $row->cash_in_date; ?></td>
                            <td>Cash In</td>
                            <th>IDR <span class="pull-right"><?php echo idr_format($row->cash_in_amount); ?></span></th>
                            <th></th>
                        </tr height="50">
                        <?php } ?>
                        <?php foreach($data1 as $row1){ ?>
                        <tr height="50">
                            <td></td>
                            <td><?php echo $row1->cash_out_date; ?></td>
                            <td>Cash Out</td>
                            <th></th>
                            <th>IDR <span class="pull-right"><?php echo idr_format($row1->cash_out_amount); ?></span></th>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <?php
                            
                        ?>
                        <?php 
                            if ($from === 'notset' && $to === 'notset') {
                                $query = $this->db->query(" SELECT SUM(cash_in_amount) AS cashin FROM tb_cash_in Where is_date_cash_in = '0' ")->row_array();
                                $query1 = $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0' ")->row_array();
                            }else if ($from != null && $to != null){
                                $query = $this->db->query(" SELECT SUM(cash_in_amount) AS cashin FROM tb_cash_in Where is_date_cash_in = '0' AND cash_in_date Between '$from' and '$to' ")->row_array();
                                $query1 = $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0' AND cash_out_date Between '$from' and '$to' ")->row_array();
                            }else {
                                $query = $this->db->query(" SELECT SUM(cash_in_amount) AS cashin FROM tb_cash_in Where is_date_cash_in = '0' ")->row_array();
                                $query1 = $this->db->query(" SELECT SUM(cash_out_amount) AS cashout FROM tb_cash_out Where is_delete_cash_out = '0' ")->row_array();
                            }
                        ?>
                        <tr height="50">
                            <td bgcolor="#F4F4F4"></td>
                            <td bgcolor="#F4F4F4"></td>
                            <td bgcolor="#F4F4F4"></td>
                            <td class=" font-weight-bold text-danger">IDR <span class="pull-right"><?php echo "Rp. " . idr_format($query['cashin']); ?></td>
                            <td class=" font-weight-bold text-danger">IDR <span class="pull-right"><?php echo "Rp. " . idr_format($query1['cashout']); ?></td>
                        </tr>
                        <tr height="50">
                            <td class="border-top-0 font-weight-bold text-danger" bgcolor="#F4F4F4"></td>
                            <td class="border-top-0 font-weight-bold text-danger" bgcolor="#F4F4F4"></td>
                            <td class="border-top-0 font-weight-bold text-danger" bgcolor="#F4F4F4">TOTAL</td>
                            <td class=" font-weight-bold text-danger">Saldo Awal</td>
                            <td class=" font-weight-bold text-danger">IDR <span class="pull-right"><?php echo "Rp. " . idr_format($query['cashin']); ?></td>
                        </tr>
                        <tr height="50">
                            <td class="border-top-0 font-weight-bold text-danger" bgcolor="#F4F4F4"></td>
                            <td class="border-top-0 font-weight-bold text-danger" bgcolor="#F4F4F4"></td>
                            <td class="border-top-0 font-weight-bold text-danger" bgcolor="#F4F4F4"></td>
                            <td class="border-top-0 font-weight-bold text-danger">Saldo Akhir</td>
                            <td class="border-top-0 font-weight-bold text-danger">IDR <span class="pull-right"><?php echo "Rp. " . idr_format($query['cashin'] - $query1['cashout']); ?></span></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $.fn.dataTable.ext.search.push(

            function (settings, data, dataIndex) {
                var date_input_from = $('#datefrom'); //our date input has the name "date"
                var date_input_to = $('#dateto'); //our date input has the name "date"
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true,
                    updateViewDate: false,
                    orientation: "top auto"
                };
                date_input_from.datepicker(options);
                date_input_to.datepicker(options);
                
                var min = $('#datefrom').datepicker("getDate");
                var max = $('#dateto').datepicker("getDate");

                var startDate = new Date(data[1]);
                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }

                return false;
            }
        );
        var table = $('#table-saldo-report').DataTable({
			filterDropDown: {									
				columns: [
                    {
					    idx: 2
                    }
                ]
            },
            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/\I|\D|\R/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            var pageTotal = api
                    .column( 3, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    
            var pageTotal1 = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                    
                // Update footer by showing the total with the reference of the column index 
                //$( api.column( 3 ).footer() ).html('IDR <span class="pull-right">' + pageTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + '</span>');
                //$( api.column( 4 ).footer() ).html('IDR <span class="pull-right">' + pageTotal1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + '</span>');
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            } ],
            "order": [[ 1, 'asc' ]]
		});

        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1+'. ';
            } );
        } ).draw();
    
        // Event listener to the two range filtering inputs to redraw on input
        $('#datefrom, #dateto').change(function () {
            //table.draw();
        });
    });
        
    (function($){
        
        // parse initialization array and returns filterDef array to faster and easy use
        // also sets defaults for properties that are not set
        function parseInitArray(initArray)
        {
            // initialization and setting defaults
            var filterDef = {
                "columns": [],
                "columnsIdxList": [],
                "bootstrap": false,
                "autoSize": true,
                "label": "Filter : "
            };
            
            // set filter properties if they have been defined, otherwise the defaults will be used
            if ( ("bootstrap" in initArray) && (typeof initArray.bootstrap === 'boolean') )
            {
                filterDef.bootstrap = initArray.bootstrap;
            }

            if ( ("autoSize" in initArray) && (typeof initArray.autoSize === 'boolean') )
            {
                filterDef.autoSize = initArray.autoSize;
            }

            if ( ("label" in initArray) && (typeof initArray.label === 'string') )
            {
                filterDef.label = initArray.label;
            }
                                        
            // add definition for each column
            if ("columns" in initArray)
            {								
                for(var i = 0; i < initArray.columns.length; i++)
                {
                    var initColumn = initArray.columns[i];
                    
                    if ( ("idx" in initColumn) && (typeof initColumn.idx === 'number') )
                    {
                        // initialize column					
                        var idx = initColumn.idx;					
                        filterDef['columns'][idx] = {						
                            "title": null,
                            "maxWidth": null,
                            "autoSize": true
                        };
                        
                        // add to list of indeces in same order they appear in the init array
                        filterDef['columnsIdxList'].push(idx);
                        
                        // set column properties if they have been defined, otherwise the defaults will be used
                        if ( ('title' in initColumn) 
                            && (typeof initColumn.title === 'string') 
                        ){
                            filterDef['columns'][idx].title = initColumn.title;
                        }
                        
                        if ( ('maxWidth' in initColumn) 
                            && (typeof initColumn.maxWidth === 'string') 						
                        ){
                            filterDef['columns'][idx].maxWidth = initColumn.maxWidth;
                        }
                        
                        if ( ('autoSize' in initColumn) 
                            && (typeof initColumn.autoSize === 'boolean')
                        ){
                            filterDef['columns'][idx].autoSize = initColumn.autoSize;
                        }	
                    }
                }			
            }
            
            return filterDef;		
        }
        
        // Add filterDropDown container div, draw select elements with default options
        // use preInit so that elements are created and correctly shown before data is loaded
        $(document).on( 'preInit.dt', function ( e, settings ) 
        {
            if ( e.namespace !== 'dt' ) {
                return;
            }
            
            // get api object for current dt table
            var api = new $.fn.dataTable.Api( settings );
            
            // get id of current table
            var id = api.table().node().id;
            
            // get initialisation object for current table to retrieve custom settings
            var initObj = api.init();
            
            // only proceed if filter has been defined in current table, otherwise don't do anything.
            if (!("filterDropDown" in initObj)) return;
                    
            // get current filter definition from init array
            var filterDef =  parseInitArray(initObj.filterDropDown);
            
            // only proceed if there are any columns defined
            if (filterDef.columns.length == 0) return;		
            
            // get container div for current data table to add new elements to
            var container = api.table().container();
            
            // add filter elements to DOM			
            var filterWrapperId = id + "_filterWrapper";
            var divCssClass = filterWrapperId + " " + (
                (filterDef.bootstrap) 
                    ? "form-inline" 
                    : ""
            );
            $(container).prepend(
                '<br><div class="col-md-12">'+
                    '<div id="' + filterWrapperId + '" class="' + divCssClass + '" style="display: none">' + filterDef.label + '</div>'+
                    '&emsp;'+
                    '<form method="post" action="<?php echo base_url('Page_control/sa_filter'); ?>" >'+
                        '&emsp;&emsp;&emsp;<div class="col-md-3 form-group d-inline-block" style="margin-right: 35px">'+
                            <?php if($from === 'notset' && $to === 'notset'): ?>
                                '<input class="form-control" id="datefrom" name="dateFrom" placeholder="Date from" type="text" />'+
                            <?php elseif($from != null && $to != null): ?>
                                '<input class="form-control" value="<?php echo $from ?>" id="datefrom" name="dateFrom" placeholder="Date from" type="text" />'+
                            <?php else: ?>
                                '<input class="form-control" id="datefrom" name="dateFrom" placeholder="Date from" type="text" />'+
                            <?php endif; ?>
                        '</div>'+
                        '<div class="col-md-3 form-group d-inline-block">'+
                            <?php if($from === 'notset' && $to === 'notset'): ?>
                                '<input class="form-control" id="dateto" name="dateTo" placeholder="Date to" type="text" />'+
                            <?php elseif($from != null && $to != null): ?>
                                '<input class="form-control" value="<?php echo $to ?>" id="dateto" name="dateTo" placeholder="Date to" type="text" />'+
                            <?php else: ?>
                                '<input class="form-control" id="dateto" name="dateTo" placeholder="Date to" type="text" />'+
                            <?php endif; ?>
                        '</div>'+
                        <?php if($from === 'notset' && $to === 'notset'): ?>
                        '<div class="form-group d-inline-block">'+
                            '&emsp;<button type="submit" class="btn btn-success btn-sm">Submit</button>'+
                        '</div>'+
                        <?php else: ?>
                        '<div class="form-group d-inline-block">'+
                            '&emsp;<button type="submit" class="btn btn-success btn-sm">Submit</button>'+
                        '</div>'+
                        '<div class="form-group d-inline-block">'+
                            '&emsp;<a href="<?= base_url()?>Page_control/dash_sa" role="button" class="btn btn-warning btn-sm">Reset</a>'+
                        '</div>'+
                        <?php endif; ?>
                    '</form>'+
                '</div><br>'
            );
            
            api.columns(filterDef.columnsIdxList).every( function () 
            {
                var column = this;
                var idx = column.index();
                
                // set title of current column
                var colName = ( filterDef.columns[idx].title !== null )
                    ? filterDef.columns[idx].title 
                    : $(this.header()).html();
                
                if (colName == "") colName = 'column ' + (idx + 1);
                            
                // adding select element for current column to container
                var selectClass = "form-control " + id + "_filterSelect";			
                var selectId = id + "_filterSelect" + idx;		
                $('#' + filterWrapperId).append('<select id="' + selectId 
                    + '" class="col-md-3 form-control' + selectClass + '"></select>');
                
                // initalising select for current column and appling event to react to changes
                var select = $("#" + selectId).empty()
                    .append( '<option value="">' + colName + '</option>' );
                
                // set max width of select elements to current width (which is defined by the size of the title)
                // turn off on for very small screens for responsive design or if autoSize has been set to false
                if ( filterDef.autoSize && filterDef.columns[idx].autoSize && (screen.width > 768) )
                {
                    select.css('max-width', select.outerWidth());
                }
                            
                // apply optional css tyle if defined in init array
                // will override automatic max width setting
                if (filterDef.columns[idx].maxWidth !== null)
                {
                    select.css('max-width', filterDef.columns[idx].maxWidth);
                }	
            } );
        
        } );
        
        // filter table and add available options to dropDowns
        $(document).on( 'init.dt', function ( e, settings ) 
        {
            if ( e.namespace !== 'dt' ) {
                return;
            }
            
            // get api object for current dt table
            var api = new $.fn.dataTable.Api( settings );
            
            // get id of current table
            var id = api.table().node().id;
            
            // get initialisation object for current table to retrieve custom settings
            var initObj = api.init();
            
            // only proceed if filter has been defined in current table, otherwise don't do anything.
            if (!("filterDropDown" in initObj)) return;
            
            // get current filter definition
            var filterDef =  parseInitArray(initObj.filterDropDown);
            
            // get container div for current data table to to add new elements to
            var container = api.table().container();
                            
            api.columns(filterDef.columnsIdxList).every( function () 
            {
                var column = this;
                var idx = column.index();
                            
                // adding select element for current column to container
                var selectId = id + "_filterSelect" + idx;							
                            
                // initalising select for current column and appling event to react to changes
                var select = $("#" + selectId);				
                
                select.on( 'change', function () 
                {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^' + val + '$' : '', true, false )
                        .draw();
                } );

                column.data().unique().sort().each( function ( d, j ) 
                {
                    if (d != "") select.append( '<option value="' + d + '">' + d + '</option>' );
                } );
            
            } );
        
        } );

    }(jQuery));
</script>