<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add With Balance Sheet</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="POST" action="<?= base_url()?>Page_control/fo_in_edit1/<?php echo $cash_in[0]->cash_in_id;?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Cash in number</td>
                                <td>:</td>
                                <td><input value="<?php echo $cash_in[0]->cash_in_id?>" type="text" id="id-generate" class="form-control pl-4 pr-4" placeholder="Cin-0001" readonly name="id"></td>
                            </tr>
                            <tr>
                                <form action="" method="post">
                                    <td>Balance Sheet Number</td>
                                    <td>:</td>
                                    <td>
                                        <input value="<?php echo $cash_in[0]->invoice_number?>" id="invoice" data-toggle="modal" data-target="#invoiceList" name="invoice" type="text" class="form-control pl-4 pr-4">
                                        <label class="text text-danger"><?= form_error('balance') ?></label>
                                    </td>
                                </form>
                            </tr> 
                            <tr>
                                <td>Date </td>
                                <td>:</td>
                                <td><input value="<?php echo date("d/m/Y", strtotime($cash_in[0]->cash_in_date));?>" class="form-control" autocomplete="off" placeholder="DD/MM/YYYY" id="date" name="date" type="text" /></td>
                            </tr>                       
                            <tr>
                                <td>Payment</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select id="payment" name="payment" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option <?php echo ($cash_in[0]->cash_in_payment == "Kas" ? "selected" : "") ?> value="Kas">Kas</option>
                                            <option <?php echo ($cash_in[0]->cash_in_payment == "Bank" ? "selected" : "") ?> value="Bank">Bank</option>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td>:</td>
                                <td><input value="<?php echo $cash_in[0]->cash_in_amount?>" id="amount" name="amount" type="number" class="form-control pl-4 pr-4"></td>
                            </tr>
                            <tr>
                                <td>Additional Information</td>
                                <td>:</td>
                                <td><textarea name="info" class="form-control pl-4 pr-4" type="text" rows="3"><?php echo $cash_in[0]->cash_in_info?></textarea></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/fo_in'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<!-- my account modal -->
<div class="modal fade bd-example-modal-lg" id="invoiceList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="formMyAccountModal" method="post" action="<?php echo base_url() ?>Welcome/editProfile" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Balance Sheet Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-profile">
                        <div class="card-body">
                            <table class="table table-hover mt-4" id="table-invoice">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Balance Id</th>
                                        <th>Name</th>
                                        <th>Information</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($data2 as $row1) {
                                    ?>
                                        <tr height="50">
                                            <td><?php echo $i++; ?></td>
                                            <th><?php echo $row1->balance_id; ?></th>
                                            <th><?php echo $row1->balance_name; ?></th>
                                            <td><?php echo $row1->balance_info; ?></td>
                                            <td><button type="button" data-dismiss="modal" onclick="selected_balance('<?php echo $row1->balance_id; ?>','<?php echo $row1->balance_name; ?>','<?php echo $row1->balance_info?>')" class="btn btn-primary btn-sm">Choose</button></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(".js-select").select2({
            width: '100%'
        });

        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
        orientation: "top auto",
        };
        date_input.datepicker(options);
    });

    $(document).ready(function() {

        var table = $('#table-invoice').DataTable({
            "searching": true,
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
            "order": [
                [1, 'asc']
            ]
        });

        table.on('order.dt search.dt', function() {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1 + '. ';
            });
        }).draw();

    });

    function selected_invoice(id, data, data1, data2, data3, data4, data5) {
        document.getElementById("invoice").value = id;
        document.getElementById("date").value = data;
        document.getElementById("name").value = data1;
        document.getElementById("type").value = data2;
        document.getElementById("material").value = data3;
        document.getElementById("payment").value = data5;
        document.getElementById("amount").value = data4;
    }

    function selected_balance(id, name, info) {
        document.getElementById("invoice").value = id;
    }

</script>