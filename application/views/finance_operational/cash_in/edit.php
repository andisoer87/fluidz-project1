<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit payment (cash in)</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?php echo base_url()?>Page_control/fo_in_edit/<?php echo $cash_in[0]->cash_in_id;?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Cash in number</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $cash_in[0]->cash_in_id?>" class="form-control pl-4 pr-4" placeholder="Cin-0001" readonly></td>
                            </tr>
                            <tr>
                                <form action="" method="post">
                                    <td>No. Invoice / Balance Sheet</td>
                                    <td>:</td>
                                    <td>
                                        <input id="invoice" readonly value="<?php echo $cash_in[0]->invoice_number?>" data-toggle="modal" data-target="#invoiceList" name="invoice" type="text" class="form-control pl-4 pr-4">
                                    </td>
                                </form>
                            </tr>
                            <tr>
                                <td>Customer name</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="customer_name" onchange="costumer_change()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="costumer_name">
                                            <option value="">--CHOOSE CUSTOMER NAME--</option>
                                            <?php

                                                foreach ($costumer as $row) {
                                                    ?>
                                                        <option <?php echo ($cash_in[0]->costumer_name == $row->costumer_name ? "selected" : "");?> value="<?php echo $row->costumer_id?>|<?php echo $row->costumer_name?>|<?php echo $row->costumer_type?>"><?php echo $row->costumer_name?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('customer_name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Customer type</td>
                                <td>:</td>
                                <td>
                                    <input type="text" readonly name="customer_type" id="customer_type" value="<?php echo $cash_in[0]->costumer_type?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('customer_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Material type</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="material_type" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="">--CHOOSE MATERIAL TYPE--</option>
                                            <?php

                                                foreach ($material as $row) {
                                                    ?>
                                                    <option <?php echo ($cash_in[0]->material_type == $row->material_name ? "selected" : ""); ?> value="<?php echo $row->material_id?>|<?php echo $row->material_name?>"><?php echo $row->material_name?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('material_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Date </td>
                                <td>:</td>
                                <td><input value="<?php echo date("d/m/Y", strtotime($cash_in[0]->cash_in_date));?>" class="form-control" autocomplete="off" placeholder="DD/MM/YYYY" id="date" name="date" type="text" /></td>
                            </tr>
                            <tr>
                                <td>Payment</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="payment_type" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="">--CHOOSE PAYMENT TYPE--</option>
                                            <option <?php echo ($cash_in[0]->cash_in_payment == "Kas" ? "selected" : "");?> value="Kas">Kas</option>
                                            <option <?php echo ($cash_in[0]->cash_in_payment == "Bank" ? "selected" : "");?> value="Bank">Bank</option>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('payment_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td>:</td>
                                <td><input name="amount" value="<?php echo $cash_in[0]->cash_in_amount?>" type="text" class="form-control pl-4 pr-4"></td>
                            </tr>
                            <tr>
                                <td>Additional Information</td>
                                <td>:</td>
                                <td>
                                    <textarea name="info" class="form-control pl-4 pr-4" type="text" rows="3"><?php echo $cash_in[0]->cash_in_info?></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/fo_in'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <a><button type="submit" class="btn btn-success">Save</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
        orientation: "top auto",
        };
        date_input.datepicker(options);
    });

    function costumer_change() {
        var costumer_type = $("#costumer_name").val();
        var costumer_typearr = costumer_type.split("|");
        
        $("#customer_type").val(costumer_typearr[2]);
    };

</script>