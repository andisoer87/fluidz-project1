<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add payment (cash out)</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?= base_url('Page_control/fo_out_add') ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Cash out number</td>
                                <td>:</td>
                                <td><input name="id" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="COu-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                    <input class="form-control" id="datefrom" name="date" placeholder="Date from" type="text" />
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Operation type</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select class="form-control selectpicker pl-4 pr-4" name="operation_type" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="" class="text-center"><strong>--CHOOSE FROM OPERATION COST--</strong></option>
                                            <?php
                                            
                                                foreach ($ops_type as $row) {
                                                    ?>
                                                    
                                                    <option value="<?php echo $row->ops_cost_id?>|<?php echo $row->ops_cost_name?>"><?php echo $row->ops_cost_name?></option>
                                                    
                                                    <?php
                                                }

                                            ?>
                                            <option value="" class="text-center"><strong>--CHOOSE FROM OPERATION EXPENSES--</strong></option>
                                            <?php
                                            
                                                foreach ($ops_exp as $row) {
                                                    ?>
                                                    
                                                    <option value="<?php echo $row->ops_exp_id?>|<?php echo $row->ops_exp_name?>"><?php echo $row->ops_exp_name?></option>
                                                    
                                                    <?php
                                                }

                                            ?>
                                            <option value="" class="text-center"><strong>--CHOOSE FROM BALANCE SHEET--</strong></option>
                                            <?php
                                            
                                                foreach ($balance as $row) {
                                                    ?>
                                                    
                                                    <option value="<?php echo $row->balance_id?>|<?php echo $row->balance_name?>"><?php echo $row->balance_name?></option>
                                                    
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('operation_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Payment</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="payment" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="Kas">Kas</option>
                                            <option value="Bank">Bank</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td>:</td>
                                <td>
                                    <input name="amount" type="text" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('amount') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3"></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/fo_out'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#datefrom'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
    
    $(document).ready(function(){
        whole_string = '<?php echo $data; ?>';
        split_string = whole_string.split(/(\d+)/);
        length_normalisation = parseInt(split_string[1]);
        length_split = ""+length_normalisation+"";
        id_number = parseInt(split_string[1]) + 1;

        if(length_split.length == 1){
            document.getElementById("id-generate").value = 'COU-000'+id_number;
        }else if(length_split.length == 2){
            document.getElementById("id-generate").value = 'COU-00'+id_number;
        }else if(length_split.length == 3){
            document.getElementById("id-generate").value = 'COU-0'+id_number;
        }else{
            document.getElementById("id-generate").value = 'COU-'+id_number;
        }
    });
</script>