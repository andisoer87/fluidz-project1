<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit payment (cash out)</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?php echo base_url()?>Page_control/fo_out_edit/<?php echo $cash_out[0]->cash_out_id?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Cash out number</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $cash_out[0]->cash_out_id?>" class="form-control pl-4 pr-4" placeholder="COu-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                    <input class="form-control" value="<?php echo date("d/m/Y", strtotime($cash_out[0]->cash_out_date));?>" id="datefrom" name="date" placeholder="Date from" type="text" />
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Operation type</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select class="form-control selectpicker pl-4 pr-4" name="operation_type" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="" class="text-center"><strong>--CHOOSE FROM OPERATION COST--</strong></option>
                                            <?php
                                            
                                                foreach ($ops_type as $row) {
                                                    ?>
                                                    
                                                    <option <?php echo ($cash_out[0]->operation_type == $row->ops_cost_name ? "selected" : "")?> value="<?php echo $row->ops_cost_id?>|<?php echo $row->ops_cost_name?>"><?php echo $row->ops_cost_name?></option>
                                                    
                                                    <?php
                                                }

                                            ?>
                                            <option  value="" class="text-center"><strong>--CHOOSE FROM OPERATION EXPENSES--</strong></option>
                                            <?php
                                            
                                                foreach ($ops_exp as $row) {
                                                    ?>
                                                    
                                                    <option <?php echo ($cash_out[0]->operation_type == $row->ops_exp_name ? "selected" : "")?> value="<?php echo $row->ops_exp_id?>|<?php echo $row->ops_exp_name?>"><?php echo $row->ops_exp_name?></option>
                                                    
                                                    <?php
                                                }

                                            ?>
                                            <option value="" class="text-center"><strong>--CHOOSE FROM BALANCE SHEET--</strong></option>
                                            <?php
                                            
                                                foreach ($balance as $row) {
                                                    ?>
                                                    
                                                    <option <?php echo ($cash_out[0]->operation_type == $row->balance_name ? "selected" : "")?> value="<?php echo $row->balance_id?>|<?php echo $row->balance_name?>"><?php echo $row->balance_name?></option>
                                                    
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('operation_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Payment</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select class="form-control selectpicker pl-4 pr-4" name="payment_type" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="">--CHOOSE PAYMENT TYPE--</option>
                                            <option <?php echo ($cash_out[0]->cash_out_payment == "Kas" ? "selected" : "")?> value="Kas">Kas</option>
                                            <option <?php echo ($cash_out[0]->cash_out_payment == "Bank" ? "selected" : "")?> value="Bank">Bank</option>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('payment_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="amount" value="<?php echo $cash_out[0]->cash_out_amount?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('amount') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="additional_info" class="form-control pl-4 pr-4" rows="3"><?php echo $cash_out[0]->cash_out_info?></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/fo_out'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <a><button type="submit" class="btn btn-success">Save</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#datefrom'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
</script>