<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit Machine Utilization</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="POST" action="<?= base_url('Page_control/machine_edit/') . $machine['machine_id'] ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Machine Code</td>
                                <td>:</td>
                                <td><input name="code" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="<?= $machine['machine_id'] ?>" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                <input class="form-control" value="<?php echo date("d/m/Y", strtotime($data[0]->machine_date));?>" autocomplete="off" placeholder="DD/MM/YYYY" id="date" name="date" type="text" />
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Code</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="unit_code" onchange="costumer_change()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="unit_code">
                                            <option value="">--CHOOSE UNIT--</option>
                                            <?php

                                                foreach ($unit as $row) {
                                                    ?>
                                                        <option <?php echo ($data[0]->machine_unit == $row->unit_id ? "selected" : "");?> value="<?php echo $row->unit_id?>|<?php echo $row->unit_model ?>"><?php echo $row->unit_id?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('customer_name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Model</td>
                                <td>:</td>
                                <td>
                                    <input id="unit_model" name="model" type="text" class="form-control pl-4 pr-4" value="<?= $machine['machine_model'] ?>" readonly>
                                    <label class="text text-danger"><?= form_error('model') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Operator Name</td>
                                <td>:</td>
                                <td>
                                    <input name="operator" type="text" class="form-control pl-4 pr-4" value="<?= $machine['machine_operator'] ?>">
                                    <label class="text text-danger"><?= form_error('operator') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>HM Start</td>
                                <td>:</td>
                                <td>
                                    <input name="start" id="start" onchange="setStart()" type="number" class="form-control pl-4 pr-4" value="<?= $machine['machine_start'] ?>">
                                    <label class="text text-danger"><?= form_error('start') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>HM finish</td>
                                <td>:</td>
                                <td>
                                    <input name="finish" id="finish" onchange="setFinish()" type="number" class="form-control pl-4 pr-4" value="<?= $machine['machine_finish'] ?>">
                                    <label class="text text-danger"><?= form_error('finish') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>HM Usage</td>
                                <td>:</td>
                                <td>
                                    <input id="usage" name="usage" type="text" class="form-control pl-4 pr-4" value="<?= $machine['machine_range']?>" readonly>
                                    <label class="text text-danger"><?= form_error('usage') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3"><?= $machine['machine_info'] ?></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/machine'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<script>
    var start, finish;

    function setFinish() {
        finish = $("#finish").val();
        start = $("#start").val();
        usage_set();
    }

    function setStart() {
        start = $("#start").val();
        finish = $("#finish").val();
        usage_set();
    }

    function usage_set() {
        var set = finish - start;
        //document.getElementById("usage").value = set;
        $("#usage").val(set);
    }

    function costumer_change() {
        var unit_type = $("#unit_code").val();
        var model = unit_type.split("|");

        document.getElementById("unit_model").value = model[1];
    }  

    $(document).ready(function(){

        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
        orientation: "top auto",
        };
        date_input.datepicker(options);
    });
</script>