<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add Material Utilization</strong></h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?= base_url('Page_control/machine_add') ?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Utilization Number</td>
                                <td>:</td>
                                <td><input name="code" id="id-generate" type="text" class="form-control pl-4 pr-4" placeholder="MU-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                    <input class="form-control" autocomplete="off" placeholder="DD/MM/YYYY" id="date" name="date" type="text" />
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Code</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="unit_code" id="unit_code" onchange="costumer_change()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="costumer_name">
                                            <option value="">--CHOOSE UNIT--</option>
                                            <?php

                                                foreach ($unit as $row) {
                                                    ?>
                                                        <option value="<?php echo $row->unit_id?>|<?php echo $row->unit_model?>"><?php echo $row->unit_id?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('customer_name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Unit Model</td>
                                <td>:</td>
                                <td>
                                    <input name="model" type="text" class="form-control pl-4 pr-4" id="unit_model" readonly>
                                    <label class="text text-danger"><?= form_error('model') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Operator Name</td>
                                <td>:</td>
                                <td>
                                    <input name="operator" type="text" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('operator') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>HM Start</td>
                                <td>:</td>
                                <td>
                                    <input onchange="setStart()" name="start" id="start" type="number" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('start') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>HM Finish</td>
                                <td>:</td>
                                <td>
                                    <input onchange="setFinish()" name="finish" id="finish" type="number" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('finish') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>HM Usage</td>
                                <td>:</td>
                                <td>
                                    <input id="usage" name="usage" type="text" class="form-control pl-4 pr-4" readonly>
                                    <label class="text text-danger"><?= form_error('usage') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Additional information </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea name="info" class="form-control pl-4 pr-4" rows="3"></textarea>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/machine'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<script>
    var start, finish;

    function costumer_change() {
        var unit_type = $("#unit_code").val();
        var model = unit_type.split("|");

        document.getElementById("unit_model").value = model[1];
    }                            

    function setFinish() {
        finish = $("#finish").val();
        
        usage_set();
    }

    function setStart() {
        start = $("#start").val();
        
        usage_set();
    }

    function usage_set() {
        var set = finish - start;
        $("#usage").val(set);
    }

    $(document).ready(function(){

        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
        orientation: "top auto",
        };
        date_input.datepicker(options);
        
        whole_string = '<?php echo $data; ?>';
        split_string = whole_string.split(/(\d+)/);
        length_normalisation = parseInt(split_string[1]);
        length_split = ""+length_normalisation+"";
        id_number = parseInt(split_string[1]) + 1;

        if(length_split.length == 1){
            document.getElementById("id-generate").value = 'MU-000'+id_number;
        }else if(length_split.length == 2){
            document.getElementById("id-generate").value = 'MU-00'+id_number;
        }else if(length_split.length == 3){
            document.getElementById("id-generate").value = 'MU-0'+id_number;
        }else{
            document.getElementById("id-generate").value = 'MU-'+id_number;
        }
    });
</script>