<div class="row  d-flex justify-content-center">
    <div class="col-lg-5 col-md-8 card">
        <div class="card-header card-header-primary text-center">
            <h2 class="card-title">Login</h2>
            <p class="category">Dev elo /</p>
        </div>
        <div class="card-body">
            <div class="modal-body">
                <form class="form" id="form-login" method="post" action="<?php echo base_url()?>Welcome/login">
                    <p class="description text-center">Enter your username and password</p>

                    <?php

                        if($this->session->flashdata('error_message')){
                            ?>
                            <div class="alert alert-danger alert-dismissible fade show pr-4" role="alert">
                                <p class="m-0 p-0"><strong>Error!</strong> <?php echo $this->session->flashdata('error_message') ?></p>
                                <button type="button" class="close" style="margin-top: 12px" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <?php
                        }
                    
                    ?>

                    <div class="card-body">
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">person</i></div>
                                </div>
                                <input type="text" class="form-control" name="username" placeholder="Username...">
                            </div>
                        </div>

                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">lock_outline</i></div>
                                </div>
                                <input type="password" name="password" placeholder="Password..." class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer justify-content-center">
                <button form="form-login" type="submit" class="btn btn-primary btn-link btn-wd btn-lg">Login</button>
            </div>
        </div>
    </div>
</div>