<!doctype html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Material Kit CSS -->
    <link href="<?php echo base_url('template/') ?>assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('template/assets/') ?>datatables/datatables.min.css"/>
</head>
<style type="text/css" media="print">
    @page{
        size : landscape;
    }
</style>
<!-- onload="window.print();" -->
<body onload="window.print()"; style="background:#ffff">
    <div class="container-fluid py-2" style="border:solid black 2px">
        <div class="col">
            <div class="row">
                <div class="w-25">
                    <img src="https://www.google.com/logos/doodles/2020/thank-you-doctors-nurses-and-medical-workers-copy-6753651837108778-law.gif" class="img-fluid img-thumbnail"/>
                </div>
                <div class="w-75" style="text-align:right">
                    <h4 class="mb-1"><strong>TOKO Bangunan</strong></h4>
                    <h5 class="mb-1"><strong>Alamat Jl. Soekarno Hatta No 1</strong></h5>
                    <h5 class="mb-1"><strong>Phone : (xxx - xxxxxxxxx)Fax.   : (xxx - xxxxxxx)</strong></h5>
                </div>
            </div>
        </div>
        <h3 class="text-center font-weight-bold"><b>DELIVERY ORDER</b></h3>
        <div class="col">
            <table>
                <tr>
                    <td><b>Number</b></td>
                    <td><b>:</b></td>
                    <td><b><?php echo $data[0]->do_id?></b></td>
                </tr>
                <tr>
                    <td><b>Date</b></td>
                    <td><b>:</b></td>
                    <td><b><?php echo date("d/m/Y", strtotime($data[0]->sales_date));?></b></td>
                </tr>
                <tr>
                    <td style="vertical-align:top"><b>Customer</b></td>
                    <td style="vertical-align:top"><b>:</b></td>
                    <td><b><?php echo $data[0]->sales_name_costumer?><br><?php echo $data[0]->address?><br>Phone : <?php echo $data[0]->telephone?> Fax. : <?php echo $data[0]->fax?></b></td>
                </tr>
            </table>
        </div>
        <div class="col mt-4">
            <table class="table table-bordered font-weight-bold text-center" >
                <thead>
                    <tr>
                        <th><b>No.</b></th>
                        <th><b>Vehicle Police Number</b></th>
                        <th><b>Driver Name</b></th>
                        <th><b>Materials</b></th>
                        <th><b>PIT Location</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php 
                            $i = 1;
                            foreach ($data as $row) {
                            ?>
                            <th><?php echo $i?>.</th>
                            <th><?php echo $row->sales_vehicle_number?></th>
                            <th><?php echo $row->sales_driver_name?></th>
                            <th><?php echo $row->material_type?></th>
                            <th><?php echo $row->sales_location?></th>
                            <?php
                        };
                        ?>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-6">
                    <b>Remark:</b>
                    <p class="mt-2"><b>Form harap dibawa ke <br> Petugas PIT untuk diisi <i>PIT <br> Location</i> dan dikembalikan ke <br> Petugas DO</b></p>
                </div>
                <div class="col-6">
                    <div class="row h-100">
                        <div class="col-4 p-0 text-center" style="border-bottom:1px solid black;">
                           <b>Petugas PIT,</b> 
                        </div>
                        <div class="col-4">
                        </div>
                        <div class="col-4 p-0 text-center">
                            <p><b>Petugas DO,</b></p>
                            <div class="d-flex flex-column h-50">
                            </div>
                            <!-- <div>
                                <b>Petugas DO,</b><br>
                                <h4><b><?= $officer_name ?></b></h4>
                            </div> -->
                            <p><strong><b><i><u><?= $officer_name ?></u></i></b></strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<style>

</style>