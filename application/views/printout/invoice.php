<!doctype html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Material Kit CSS -->
    <link href="<?php echo base_url('template/') ?>assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('template/assets/') ?>datatables/datatables.min.css"/>
</head>
<style type="text/css" media="print">
    @page{
        size : landscape;
    }

    .page-before{
        page-break-before: always;
    }

    .seconde-page{
        page-break-after:always;
    }
</style>
<body style="background:#ffff" onload="window.print()";>
    <!-- INVOICE -->
    <div class="container-fluid py-2" style="border:solid black 2px">
        <div class="col h-100" style="position:relative">
            <div class="row">
                <div class="w-25">
                    <img src="https://www.google.com/logos/doodles/2020/thank-you-doctors-nurses-and-medical-workers-copy-6753651837108778-law.gif" class="img-fluid img-thumbnail"/>
                </div>
            </div>
            <div class="row w-100 h-100" style="position:absolute;top:0">
                 <h3 class="text-center font-weight-bold m-auto w-100"><b>INVOICE</b></h3>
            </div>
        </div>
        <div class="col mt-2">
            <tr><h4><b>TOKO BANGUNAN</b></h4></tr>
            <div class="row mt-2">
                <div class="col-8">
                    <table>
                        <tr><h4><b>Alamat . . . .</b></h4></tr>
                        <tr><h4><b>Alamat . . . .</b></h4></tr>
                        <tr>
                            <td><b>Phone</b></td>
                            <td><b>:</b></td>
                            <td><b>(xxx - xxxxxxx)</b></td>
                        </tr>
                        <tr>
                            <td><b>Fax</b></td>
                            <td><b>:</b></td>
                            <td><b>(xxx - xxxxxxx)</b></td>
                        </tr>
                    </table>
                </div>
                <div class="col-4">
                    <table>
                        <tr>
                            <td><b>Number</b></td>
                            <td><b>:</b></td>
                            <td><b><?php echo $data[0]->sales_invoice?></b></td>
                        </tr>
                        <tr>
                            <td><b>Invoice Date</b></td>
                            <td><b>:</b></td>
                            <td><b><?php echo date("d/m/Y", strtotime($invoice[0]->invoice_date));?></b></td>
                        </tr>
                        <tr>
                            <td><b>Payment Term</b></td>
                            <td><b>:</b></td>
                            <td><b><?php echo ucfirst($invoice[0]->invoice_payment)?></b></td>
                        </tr>
                        <tr>
                            <td><b>Due Date</b></td>
                            <td><b>:</b></td>
                            <td><b><?php echo ($invoice[0]->invoice_payment == 'credit' ? date("d/m/Y", strtotime($data[0]->sales_due_date)) : '-');?></b></td>
                        </tr>
                        <tr>
                            <td><b>Currency</b></td>
                            <td><b>:</b></td>
                            <td><b>IDR</b></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col">
            <table>
                <tr>
                    <td style="vertical-align:top"><b>Customer</b></td>
                    <td style="vertical-align:top"><b>:</b></td>
                    <td><b><?php echo $invoice[0]->invoice_costumer_name?><br><?php echo $invoice[0]->invoice_address?><br>Phone : <?php echo $invoice[0]->invoice_telephone?> Fax. : <?php echo $invoice[0]->invoice_fax?></b></td>
                </tr>
            </table>
        </div>
        <div class="col mt-2">
            <table class="table font-weight-bold" >
                <thead>
                    <tr>
                        <th><b>No.</b></th>
                        <th><b>Product Descriptions</b></th>
                        <th><b>Quantity</b></th>
                        <th><b>UoM</b></th>
                        <th><b>Unit Price</b></th>
                        <th><b>Amount</b></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 1;
                        foreach ($data as $row) {
                            ?>
                            
                            <tr>
                                <th><b><?php echo $i?>.</b></th>
                                <th><b><?php echo $row->material_type?></b></th>
                                <th><b><?php echo $row->sales_material_amount?></b></th>
                                <th><b>m3</b></th>
                                <?php
                                
                                    if($row->sales_type_costumer == "Retail"){
                                        $price = $row->retail_price;
                                    }else if($row->sales_type_costumer == "Distributor"){
                                        $price = $row->distributor_price;
                                    }else{
                                        $price = $row->special_price;
                                    }

                                ?>
                                <th><b><?php echo number_format($price, 2, ",", ".")?></b></th>
                                <th><b><?php echo number_format($invoice[0]->invoice_amount, 2, ",", ".")?></b></th>
                            </tr>

                            <?php
                        }

                    ?>
                    <tr>
                        <th colspan="4">Inword : <?php echo terbilang($invoice[0]->invoice_amount)?></th>
                        <th>Net Total</th>
                        <th><?php echo number_format($invoice[0]->invoice_amount, 2, ",", ".")?></th>
                    </tr>
                </tbody>
            </table>
            <div class="col text-right">
                <b>
                    <h3>Amount Due : </h3>
                    <h2><b>Rp <?php echo number_format($invoice[0]->invoice_amount, 2, ",", ".")?></b></h2>
                </b>
            </div>
        </div>
    </div>
    <!-- /.INVOICE -->
    <div class="page-before">
    </div>
    <!-- SPB -->
    <div class="container-fluid py-2 second-page" style="border:solid black 2px">
        <div class="col">
            <div class="row">
                <div class="w-25">
                    <img src="https://www.google.com/logos/doodles/2020/thank-you-doctors-nurses-and-medical-workers-copy-6753651837108778-law.gif" class="img-fluid img-thumbnail"/>
                </div>
                <div class="w-75" style="text-align:right">
                    <h4 class="mb-1"><strong>TOKO Bangunan</strong></h4>
                    <h5 class="mb-1"><strong>Alamat Jl. Soekarno Hatta No 1</strong></h5>
                    <h5 class="mb-1"><strong>Phone : (xxx - xxxxxxxxx)Fax.   : (xxx - xxxxxxx)</strong></h5>
                </div>
            </div>
        </div>
        <h3 class="text-center font-weight-bold"><b>SURAT PENGIRIMAN BARANG</b></h3>
        <div class=col>
            <div class="row">
                <div class="col-8">
                    <table>
                        <tr>
                            <td><b>Nomor</b></td>
                            <td><b>:</b></td>
                            <td><b><?php echo $invoice[0]->spb_number?></b></td>
                        </tr>
                    </table>
                </div>
                <div class="col-4 text-right">
                    <h5><b>Jakarta, <?php echo date("d", strtotime($invoice[0]->invoice_date));?> <?php echo date("F", strtotime($invoice[0]->invoice_date));?> <?php echo date("Y", strtotime($invoice[0]->invoice_date));?></b></h5>
                </div>
            </div>
        </div>
        <div class="col">
            <table>
                <tr><td><b>Kepada:</b></td></tr>
                <tr><td><b>Yth. <strong><u><i><?php echo $invoice[0]->invoice_costumer_name?></i></u></strong></b></td></tr>
                <tr><td><b><strong><u><i><?php echo $invoice[0]->invoice_address?></i></u></strong></b></td></tr>
                <tr><td><b><strong><u><i><?php echo $invoice[0]->invoice_telephone?></i></u></strong></b></td></tr>
            </table>    
        </div>
        <div class="col mt-4">
            <p><b>
            Bersama ini kami kirimkan material pesanan bapak/ibu dengan rincian sebagai berikut : 
            </b>
            </p>
        </div>
        <div class="col mt-2">
            <table class="table table-bordered font-weight-bold" >
                <thead>
                    <tr class="text-center">
                        <th><b>No.</b></th>
                        <th><b>Jenis Material</b></th>
                        <th><b>Jumlah</b></th>
                        <th><b>Satuan</b></th>
                        <th><b>Keterangan</b></th>
                    </tr>
                </thead>
                <tbody class="text-center">
                 <?php
                        $i = 1;
                        foreach ($data as $row) {
                            ?>
                            
                            <tr>
                                <th><b><?php echo $i?>.</b></th>
                                <th><b><?php echo $row->material_type?></b></th>
                                <th><b><?php echo $row->sales_material_amount?></b></th>
                                <th><b>m3</b></th>
                                <?php
                                
                                    if($row->sales_type_costumer == "retail"){
                                        $price = $row->retail_price;
                                    }else if($row->sales_type_costumer == "distributor"){
                                        $price = $row->distributor_price;
                                    }else{
                                        $price = $row->special_price;
                                    }

                                ?>
                                <th><b><?php echo $row->sales_location?></b></th>
                            </tr>

                            <?php
                        }

                    ?>
                </tbody>
            </table>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-6">
                    <b>Remark : </b>
                    <p class="mt-2">
                        <b><u><?php echo $row->sales_vehicle_number?></u></b><br>
                        <b><u><?php echo $row->sales_driver_name?></u></b><br>
                    </p>
                </div>
                <div class="col-6">
                    <div class="row h-100">
                        <div class="col-4 p-0 text-center">
                            <p><b>Dikirimkan oleh,</b></p>
                            <div class="d-flex justify-content-center align-items-end h-50 mt-4">
                                <p ><strong><b><i><u><?= $officer_name ?></u></i></b></strong></p>
                            </div>
                            <p><strong><b>TOKO BANGUNAN</b></strong></p>
                        </div>
                        <div class="col-4"></div>
                        <div class="col-4 p-0 text-center">
                            <p><b>Diterima oleh,</b></p>
                            <div class="d-flex flex-column h-50" style="border-bottom:1px solid black;"></div>
                            <p><strong><b><?= $row->sales_name_costumer?></b></strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php

    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }     		
        return $hasil;
    }

    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " Belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " Seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " Seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}

?>