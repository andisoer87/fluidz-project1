<!doctype html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Material Kit CSS -->
    <link href="<?php echo base_url('template/') ?>assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('template/assets/') ?>datatables/datatables.min.css"/>
</head>
<style type="text/css" media="print">
    @page{
        size : landscape;
    }
</style>
<body onload="window.print()" style="background:#ffff">
    <div class="container-fluid py-2" style="border:solid black 2px">
        <div class="col">
            <div class="row">
                <div class="w-25">
                    <img src="https://www.google.com/logos/doodles/2020/thank-you-doctors-nurses-and-medical-workers-copy-6753651837108778-law.gif" class="img-fluid img-thumbnail"/>
                </div>
                <div class="w-75" style="text-align:right">
                    <h4 class="mb-1"><strong>TOKO Bangunan</strong></h4>
                    <h5 class="mb-1"><strong>Alamat Jl. Soekarno Hatta No 1</strong></h5>
                    <h5 class="mb-1"><strong>Phone : (xxx - xxxxxxxxx)Fax.   : (xxx - xxxxxxx)</strong></h5>
                </div>
            </div>
        </div>
        <h3 class="text-center font-weight-bold"><b>PURCHASE ORDER</b></h3>
        <div class="row mt-2">
            <div class="col-8">
                <table>
                    <tr>
                        <td><b>Number</b></td>
                        <td><b>:</b></td>
                        <?php 
                            $number_po_num = preg_split('#(?<=[a-z])-(?=\d)#i', $data[0]->po_number);
                        ?>
                        <td><b><?php echo $number_po_num[1];?>/<?php echo date("m");?>/PO/<?php echo date("y");?></b></td>
                    </tr>
                    <tr>
                        <td><b>Date</b></td>
                        <td><b>:</b></td>
                        <td><b><?php echo date("d/m/Y", strtotime($data[0]->fuel_in_date))?></b></td>
                    </tr>
                    <tr>
                        <td><b>Officer</b></td>
                        <td><b>:</b></td>
                        <td><b><?php echo $user[0]->username?></b></td>
                    </tr>
                </table>
            </div>
            <div class="col-4">
                <table>
                    <tr>
                        <td style="vertical-align:top"><b>Vendor</b></td>
                        <td style="vertical-align:top"><b>:</b></td>
                        <td><b><?php echo $data[0]->fuel_in_supplier?>,<br>
                            Address : <?php echo ($data[0]->fuel_in_supplier_address == null ? "-" : $data[0]->fuel_in_supplier_address )?><br>
                            Phone : <?php echo ($data[0]->fuel_in_supplier_phone == null ? "-" : $data[0]->fuel_in_supplier_phone )?><br>
                            Fax. : <?php echo ($data[0]->fuel_in_supplier_fax == null ? "-" : $data[0]->fuel_in_supplier_fax )?>
                        </td>
                    </tr>
                    <tr>
                        <td><b>Due Date</b></td>
                        <td><b>:</b></td>
                        <td><b><?php echo date("d/m/Y", strtotime($data[0]->due_date))?></b></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col mt-4">
            <table class="table font-weight-bold" >
                <thead>
                    <tr>
                        <th><b>No.</b></th>
                        <th><b>Product Descriptions</b></th>
                        <th><b>Quantity</b></th>
                        <th><b>UoM</b></th>
                        <th><b>Unit Price</b></th>
                        <th><b>Amount</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <?php 
                            $i = 1;
                            $amount_due = 0;
                            foreach ($data as $row) {
                                ?>
                                    <th><b><?php echo $i?>.</b></th>
                                    <th><b><?php echo $row->fuel_in_desc?></b></th>
                                    <th><b><?php echo $row->fuel_in_quantity?></b></th>
                                    <th><b>L</b></th>
                                    <th><b><?php echo number_format($row->unit_price, 2, ",", ".") ?></b></th>
                                    <th><b><?php echo number_format($row->fuel_in_total, 2, ",", ".") ?></b></th>
                                <?php
                                $amount_due += $row->fuel_in_total;
                            }
                        ?>
                    </tr>
                    <tr>
                        <th colspan="4">Inword : <?php echo terbilang($amount_due)?></th>
                        <th>Net Total</th>
                        <th><?php echo number_format($amount_due, 2, ",", ".") ?></th>
                    </tr>
                </tbody>
            </table>
            <div class="col text-right">
                <b>
                    <h3>Amount Due : </h3>
                    <h2><b><?php echo "Rp ".number_format($amount_due, 2, ",", ".") ?></b></h2>
                </b>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <div class="col-6 p-0 text-center">
                            <p><b>Dipesan Oleh,</b></p>
                            <div class="d-flex justify-content-center align-items-end h-50">
                                <p class="mt-md-5"><strong><b><u><i><?php echo $user[0]->username?></i></u></b></strong></p>
                            </div>
                            <p><strong><b>TOKO BANGUNAN</b></strong></p>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <p><b>Remark : </b></p><br>
                    <?php echo $data[0]->fuel_in_info?>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php

    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }     		
        return $hasil;
    }

    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " Belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " Seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " Seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}

?>