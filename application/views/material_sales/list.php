<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">List of material sales</h4>
        </div>
        <div class="card-body">
            <!-- header -->

            <?php if ($this->session->flashdata('user_alert')) { ?>
                <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('user_alert'); ?>
                    <button type="button" class="close" style="margin-top: 12px" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php } ?>

            <div class="col-md-12">
                <?php if($this->session->userdata("user_level") == 'admin' || $this->session->userdata("user_level") == 'officer'): ?>
                <a href="<?php echo site_url('Page_control/ms_add'); ?>"><button type="button" class="btn btn-success">Create Sales</button></a>
                <?php else: ?>
                <a href="#"><button type="button" class="btn btn-default">Add data un-available</button></a>
                <?php endif; ?>
            </div>

            <!-- tabel -->
            <div class="col-md-12">
                <table class="table table-hover mt-4" id="table-mtr-sales">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sale ID</th>
                            <th>No. DO</th>
                            <th>No. Invoice</th>
                            <th>No. SPB</th>
                            <th>Date</th>
                            <th>Customer</th>
                            <th>Location</th>
                            <th>Material</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i=1;
                    foreach($data as $row)
                    {
                    ?>
                        <tr height="50">
                            <td></td>
                            <td><?php echo $row->sales_id; ?></td>
                            <td><?php echo $row->do_id; ?></td>
                            <th><?php echo $row->sales_invoice; ?></th>
                            <td><?php echo $row->spb_id; ?></td>
                            <th><?php echo $row->sales_date; ?></th>
                            <td><?php echo $row->sales_name_costumer; ?></td>
                            <td><?php echo $row->sales_location; ?></td>
                            <td><?php echo $row->material_type; ?></td>
                            <th><?php echo idr_format($row->sales_amount); ?></th>
                            <td><?php echo $row->sales_act_status; ?></td>
                            <td class="td-actions text-right">
                                <?php if($row->sales_act_status == 'DO') :?>
                                <a type="button" rel="tooltip" title="edit" class="btn btn-info" href="<?php echo site_url('Page_control/ms_edit/').$row->sales_id; ?>">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a type="button" rel="tooltip" title="print" class="btn btn-success text-white" onclick="print_do('<?php echo $row->do_id;?>', '<?php echo base_url()?>Page_control/print_do/<?php echo str_replace('=', '-', str_replace('/', '_', base64_encode($row->do_id))); ?>');">
                                    <i class="material-icons">print</i>
                                </a>
                                <a type="button" rel="tooltip" title="Release" class="btn btn-danger text-white" href="<?php echo site_url('Page_control/ms_release/').$row->sales_id; ?>">
                                    <i class="material-icons">confirmation_number</i>
                                </a>
                                <?php else: ?>
                                <a type="button" rel="tooltip" title="edit" class="btn btn-info" href="<?php echo site_url('Page_control/ms_edit/').$row->sales_id; ?>">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a type="button" rel="tooltip" title="print" class="btn btn-success text-white" onclick="print_do('<?php echo $row->do_id;?>', '<?php echo base_url()?>Page_control/print_do/<?php echo str_replace('=', '-', str_replace('/', '_', base64_encode($row->do_id))); ?>');">
                                    <i class="material-icons">print</i>
                                </a>
                                <a type="button" rel="tooltip" title="Release" class="btn btn-warning text-white" href="<?php echo site_url('Page_control/ms_release/').$row->sales_id; ?>">
                                    <i class="material-icons">confirmation_number</i>
                                </a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php
                    $i++;
                    }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr height="50">
                            <td class="border-top-0" bgcolor="#F4F4F4"></td>
                            <td class="border-top-0" colspan="2" bgcolor="#F4F4F4"></td>
                            <td class="border-top-0 font-weight-bold text-danger" colspan = "2" bgcolor="#F4F4F4"><strong>TOTAL</strong></td>
                            <td class="font-weight-bold text-danger " colspan="2" bgcolor="#F4F4F4"></td>
                            <td bgcolor="#F4F4F4" class="font-weight-bold text-danger " colspan="3">IDR<span class="text-danger pull-right font-weight-bold"><?php echo idr_format($total['count']) ?></td>
                            <td bgcolor="#F4F4F4"></td>
                            <td bgcolor="#F4F4F4" class=""></td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $.fn.dataTable.ext.search.push(

            function (settings, data, dataIndex) {
                var date_input_from = $('#datefrom'); //our date input has the name "date"
                var date_input_to = $('#dateto'); //our date input has the name "date"
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                
                $(".js-select").select2({
                    width: '28%',
                });

                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true,
                    updateViewDate: false,
                    orientation: "top auto"
                };
                date_input_from.datepicker(options);
                date_input_to.datepicker(options);
                
                var min = $('#datefrom').datepicker("getDate");
                var max = $('#dateto').datepicker("getDate");

                var startDate = new Date(data[5]);
                if (min == null && max == null) { return true; }
                if (min == null && startDate <= max) { return true;}
                if(max == null && startDate >= min) {return true;}
                if (startDate <= max && startDate >= min) { return true; }

                return false;
            }
        );
        var table = $('#table-mtr-sales').DataTable({
			filterDropDown: {									
				columns: [
                    {
					    idx: 6
                    },{
                        idx: 8
                    },{
                        idx: 7
                    }
                ]
			},
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
    
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/\I|\D|\R/g, '')*1 :
                            typeof i === 'number' ?
                            i : 0;
                };
    
                // Total over all pages
                total = api
                    .column( 8 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 8, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                $( api.column( 8 ).footer() ).html(
                    //pageTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                );
            },
            "columnDefs": [ {
                "searchable": false,
                "orderable": false,
                "targets": 0
            },
            {
                "targets": 7,
                "visible": false
            } ],
            "order": [[ 1, 'sasc' ]]
		});

        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1+'. ';
            } );
        } ).draw();
    
        // Event listener to the two range filtering inputs to redraw on input
        $('#datefrom, #dateto').change(function () {
            //table.draw();
        });
    });

    function print_do(do_id, url) {
        if(confirm("Print Delivery Order "+do_id+" ?")){
            var win = window.open(url, '_blank');
             win.focus();
        }
    }
        
    (function($){
        
        // parse initialization array and returns filterDef array to faster and easy use
        // also sets defaults for properties that are not set
        function parseInitArray(initArray)
        {
            // initialization and setting defaults
            var filterDef = {
                "columns": [],
                "columnsIdxList": [],
                "bootstrap": false,
                "autoSize": true,
                "label": " "
            };
            
            // set filter properties if they have been defined, otherwise the defaults will be used
            if ( ("bootstrap" in initArray) && (typeof initArray.bootstrap === 'boolean') )
            {
                filterDef.bootstrap = initArray.bootstrap;
            }

            if ( ("autoSize" in initArray) && (typeof initArray.autoSize === 'boolean') )
            {
                filterDef.autoSize = initArray.autoSize;
            }

            if ( ("label" in initArray) && (typeof initArray.label === 'string') )
            {
                filterDef.label = initArray.label;
            }
                                        
            // add definition for each column
            if ("columns" in initArray)
            {								
                for(var i = 0; i < initArray.columns.length; i++)
                {
                    var initColumn = initArray.columns[i];
                    
                    if ( ("idx" in initColumn) && (typeof initColumn.idx === 'number') )
                    {
                        // initialize column					
                        var idx = initColumn.idx;					
                        filterDef['columns'][idx] = {						
                            "title": null,
                            "maxWidth": null,
                            "autoSize": true
                        };
                        
                        // add to list of indeces in same order they appear in the init array
                        filterDef['columnsIdxList'].push(idx);
                        
                        // set column properties if they have been defined, otherwise the defaults will be used
                        if ( ('title' in initColumn) 
                            && (typeof initColumn.title === 'string') 
                        ){
                            filterDef['columns'][idx].title = initColumn.title;
                        }
                        
                        if ( ('maxWidth' in initColumn) 
                            && (typeof initColumn.maxWidth === 'string') 						
                        ){
                            filterDef['columns'][idx].maxWidth = initColumn.maxWidth;
                        }
                        
                        if ( ('autoSize' in initColumn) 
                            && (typeof initColumn.autoSize === 'boolean')
                        ){
                            filterDef['columns'][idx].autoSize = initColumn.autoSize;
                        }	
                    }
                }			
            }
            
            return filterDef;		
        }
        
        // Add filterDropDown container div, draw select elements with default options
        // use preInit so that elements are created and correctly shown before data is loaded
        $(document).on( 'preInit.dt', function ( e, settings ) 
        {
            if ( e.namespace !== 'dt' ) {
                return;
            }
            
            // get api object for current dt table
            var api = new $.fn.dataTable.Api( settings );
            
            // get id of current table
            var id = api.table().node().id;
            
            // get initialisation object for current table to retrieve custom settings
            var initObj = api.init();
            
            // only proceed if filter has been defined in current table, otherwise don't do anything.
            if (!("filterDropDown" in initObj)) return;
                    
            // get current filter definition from init array
            var filterDef =  parseInitArray(initObj.filterDropDown);
            
            // only proceed if there are any columns defined
            if (filterDef.columns.length == 0) return;		
            
            // get container div for current data table to add new elements to
            var container = api.table().container();
            
            // add filter elements to DOM			
            var filterWrapperId = id + "_filterWrapper";
            var divCssClass = filterWrapperId + " " + (
                (filterDef.bootstrap) 
                    ? "form-inline" 
                    : ""
            );
            $(container).prepend(
                 '<br>'+
                '<form class="col-md-12" method="post" action="<?php echo base_url('Page_control/ms_filter'); ?>" >'+
                    '<div id="' + filterWrapperId + '" class="col-10 ' + divCssClass + '">' + filterDef.label + '</div>'+
                    '&emsp;'+
                    '<div>'+
                        '<div class="col-md-3 form-group d-inline-block" >'+
                            <?php if($from === 'notset' && $to === 'notset'): ?>
                                '<input class="form-control" id="datefrom" name="dateFrom" placeholder="Date from" type="text" />'+
                            <?php elseif($from != null && $to != null): ?>
                                '<input class="form-control" value="<?php echo $from ?>" id="datefrom" name="dateFrom" placeholder="Date from" type="text" />'+
                            <?php else: ?>
                                '<input class="form-control" id="datefrom" name="dateFrom" placeholder="Date from" type="text" />'+
                            <?php endif; ?>
                        '</div>'+
                        '<div class="col-md-3 form-group d-inline-block">'+
                            <?php if($from === 'notset' && $to === 'notset'): ?>
                                '<input class="form-control" id="dateto" name="dateTo" placeholder="Date to" type="text" />'+
                            <?php elseif($from != null && $to != null): ?>
                                '<input class="form-control" value="<?php echo $to ?>" id="dateto" name="dateTo" placeholder="Date to" type="text" />'+
                            <?php else: ?>
                                '<input class="form-control" id="dateto" name="dateTo" placeholder="Date to" type="text" />'+
                            <?php endif; ?>
                        '</div>'+
                        <?php if($from === 'notset' && $to === 'notset' && $location === 'notset' && $material === 'notset' && $customer === 'notset'): ?>
                        '<div class="form-group d-inline-block">'+
                            '&emsp;<button type="submit" class="btn btn-success btn-sm">Submit</button>'+
                        '</div>'+
                        <?php else: ?>
                        '<div class="form-group d-inline-block">'+
                            '&emsp;<button type="submit" class="btn btn-success btn-sm">Submit</button>'+
                        '</div>'+
                        '<div class="form-group d-inline-block">'+
                            '&emsp;<a href="<?php echo base_url()?>Page_control/ms_list" role="button" class="btn btn-warning btn-sm">Reset</a>'+
                        '</div>'+
                        <?php endif; ?>
                    '</div>'+
                '</form>'+
                '<br>'
            );
            
            api.columns(filterDef.columnsIdxList).every( function () 
            {
                var column = this;
                var idx = column.index();
                
                // set title of current column
                var colName = ( filterDef.columns[idx].title !== null )
                    ? filterDef.columns[idx].title 
                    : $(this.header()).html();
                
                if (colName == "") colName = 'column ' + (idx + 1);
                            
                // adding select element for current column to container
                var selectClass = id + "_filterSelect";			
                var selectId = id + "_filterSelect" + idx;		
                $('#' + filterWrapperId).append('<select id="' + selectId 
                    + '" class="col-md-3 js-select ' + selectClass + '" name="' + colName + '" style="margin-right: 40px"></select>&emsp;');
                
                // initalising select for current column and appling event to react to changes
                var select = $("#" + selectId).empty()
                    .append( '<option value="">' + colName + '</option>' );
                
                // set max width of select elements to current width (which is defined by the size of the title)
                // turn off on for very small screens for responsive design or if autoSize has been set to false
                if ( filterDef.autoSize && filterDef.columns[idx].autoSize && (screen.width > 768) )
                {
                    select.css('max-width', select.outerWidth());
                }
                            
                // apply optional css tyle if defined in init array
                // will override automatic max width setting
                if (filterDef.columns[idx].maxWidth !== null)
                {
                    select.css('max-width', filterDef.columns[idx].maxWidth);
                }	
            } );
        
        } );
        
        // filter table and add available options to dropDowns
        $(document).on( 'init.dt', function ( e, settings ) 
        {
            if ( e.namespace !== 'dt' ) {
                return;
            }
            
            // get api object for current dt table
            var api = new $.fn.dataTable.Api( settings );
            
            // get id of current table
            var id = api.table().node().id;
            
            // get initialisation object for current table to retrieve custom settings
            var initObj = api.init();
            
            // only proceed if filter has been defined in current table, otherwise don't do anything.
            if (!("filterDropDown" in initObj)) return;
            
            // get current filter definition
            var filterDef =  parseInitArray(initObj.filterDropDown);
            
            // get container div for current data table to to add new elements to
            var container = api.table().container();
                            
            api.columns(filterDef.columnsIdxList).every( function () 
            {
                var column = this;
                var idx = column.index();
                            
                // adding select element for current column to container
                var selectId = id + "_filterSelect" + idx;							
                            
                // initalising select for current column and appling event to react to changes
                var select = $("#" + selectId);				
                
                select.on( 'change', function () 
                {
                    // var val = $.fn.dataTable.util.escapeRegex(
                    //     $(this).val()
                    // );

                    // column
                    //     .search( val ? '^' + val + '$' : '', true, false )
                    //     .draw();
                } );

                column.data().unique().sort().each( function ( d, j ) 
                {
                    if (d != "") select.append( '<option value="' + d + '">' + d + '</option>' );
                } );
            
            } );
        
        } );

    }(jQuery));
</script>