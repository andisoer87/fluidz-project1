<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Release Invoice and SPB</h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form>
                    <table class="table mt-4">
                        <tbody>
                            <?php foreach($data as $data_row){ ?>
                            <tr>
                                <td>DO number</td>
                                <td>:</td>
                                <td><input type="text" class="form-control pl-4 pr-4" value="<?php echo $data_row->do_id; ?>" placeholder="001/mm/SPB/yyyy" readonly></td>
                            </tr>
                            <tr>
                                <td>Invoice number</td>
                                <td>:</td>
                                <td><input type="text" class="form-control pl-4 pr-4" value="<?php echo ($data_row->sales_invoice != null ? $data_row->sales_invoice : $inv_id); ?>" placeholder="001/mm/SPB/yyyy" readonly></td>
                            </tr>
                            <tr>
                                <td>SPB number</td>
                                <td>:</td>
                                <td><input type="text" class="form-control pl-4 pr-4" value="<?php echo ($data_row->spb_id != null ? $data_row->spb_id : $spb_id); ?>" placeholder="001/mm/SPB/yyyy" readonly></td>
                            </tr>
                            <tr>
                                <td>Customer Name</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->sales_name_costumer; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Customer Type</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->sales_type_costumer; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <textarea class="form-control pl-4 pr-4" rows="3" readonly><?php echo $data_row->address; ?></textarea>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->city; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Telephone</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->telephone; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Fax</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->fax; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Vehicle Police Number</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->sales_vehicle_number; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Driver Name</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->sales_driver_name; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Material Type</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->material_type; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>PIT Location</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->sales_location; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo date("d/m/Y", strtotime($data_row->sales_date)); ?>" id="date" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY" readonly></td>
                            </tr>
                            <tr>
                                <td>Term Of Payment</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->sales_payment; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <tr>
                                <td>Amount</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data_row->sales_amount; ?>" class="form-control pl-4 pr-4" readonly></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </form>

                <div class="pull-right">
                    <a onclick="print_release('<?php echo base_url()?>Page_control/print_invoice/<?php echo $data_row->sales_id?>')"><button type="button" class="btn btn-success">Print</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })

    $(document).ready(function() {
        var date_input = $('#duedate'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })

    function print_release(url) {
        var win = window.open(url, '_blank');
        win.focus();
    }

</script>