<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Add sales detail</h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?php echo base_url()?>Page_control/ms_add">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Sales ID</td>
                                <td>:</td>
                                <td><input type="text" name="sales_id" value="<?php echo $sales_id?>" class="form-control pl-4 pr-4" placeholder="SLS-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>DO number</td>
                                <td>:</td>
                                <td><input type="text" name="do_id" value="<?php echo $do_id?>" class="form-control pl-4 pr-4" placeholder="0001/mm/DO/yyyy" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                    <input type="text" autocomplete="off" id="date" name="date" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY">
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Customer name</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="customer_name" onchange="costumer_change()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="costumer_name">
                                            <option value="">--CHOOSE CUSTOMER NAME--</option>
                                            <?php

                                                foreach ($costumer as $row) {
                                                    ?>
                                                        <option value="<?php echo $row->costumer_id?>|<?php echo $row->costumer_name?>|<?php echo $row->costumer_type?>"><?php echo $row->costumer_name?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('customer_name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Customer type </td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="customer_type" name="customer_type" class="form-control pl-4 pr-4" readonly>
                                    <label class="text text-danger"><?= form_error('customer_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Vehicle police number </td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="veh_police_number" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('veh_police_number') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Driver name </td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="driver_name" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('driver_name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Material type  </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="material_type" id="material_type" onchange="material_change()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="">--CHOOSE MATERIAL TYPE--</option>
                                            <?php

                                                foreach ($material as $row) {
                                                    ?>
                                                        <option value="<?php echo $row->material_id?>|<?php echo $row->material_name?>|<?php echo $row->retail_price?>|<?php echo $row->special_price?>|<?php echo $row->distributor_price?>"><?php echo $row->material_name?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('material_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Material Amount </td>
                                <td>:</td>
                                <td>
                                    <input type="number" id="material_amount" name="material_amount" onchange="material_amount_change()" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('material_amount') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Payment</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select id="payment" name="payment" onchange="payment_type()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option>--SELECT PAYMENT METHOD--</option>
                                            <option value="cash">Cash</option>
                                            <option value="credit">Kredit</option>
                                    </div>
                                </td>
                            </tr>
                            <tr id="field_term_payment" style="display:none">
                                <td>Term of Payment</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select id="term_payment" name="term_payment" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value=''>--SELECT TERM OF PAYMENT--</option>
                                            <option value="1">N1</option>
                                            <option value="7">N7</option>
                                            <option value="30">N30</option>
                                            <option value="60">N60</option>
                                    </div>
                                </td>
                            </tr>
                            <input type="hidden" name="amount" id="amount" class="form-control pl-4 pr-4">
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/ms_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <a><button type="submit" class="btn btn-success">Save</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

    var harga_material;
    var material_amount;

    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })

    function payment_type() {
        var pay_type = $("#payment").val();

        if(pay_type == 'credit'){
            document.getElementById("field_term_payment").style.display = ''
        }else{
            document.getElementById("field_term_payment").style.display = 'none'
        }
    }

    function costumer_change() {
        var costumer_type = $("#costumer_name").val();
        var costumer_typearr = costumer_type.split("|");
        
        $("#customer_type").val(costumer_typearr[2]);
        material_change();
    }

    function count_amount() {
        console.log("harga : "+harga_material);
        console.log("jumlah : "+material_amount);
        
        var amount = harga_material * material_amount;
        $("#amount").val(amount);

    }

    function material_amount_change(){
        material_amount = $("#material_amount").val();
        count_amount();
    }

    function material_change() {
        var material_type = $("#material_type").val();
        var material_typearr = material_type.split("|");
        
        if($("#customer_type").val() == 'Retail'){
            harga_material = material_typearr[2];
        }else if($("#customer_type").val() == 'Special Retail'){
            harga_material = material_typearr[3];
        }else if($("#customer_type").val() == 'Distributor'){
            harga_material = material_typearr[4];
        }
        
        count_amount()
        // $("#customer_type").val(costumer_typearr[2]);
    }
</script>