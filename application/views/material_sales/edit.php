<div class="col-md-12">
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Edit sales detail</h4>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                <form method="post" action="<?php echo base_url()?>Page_control/ms_edit/<?php echo $data[0]->sales_id;?>">
                    <table class="table mt-4">
                        <tbody>
                            <tr>
                                <td>Sales ID</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data[0]->sales_id?>" class="form-control pl-4 pr-4" placeholder="SLS-0001" readonly></td>
                            </tr>
                            <tr>
                                <td>DO number</td>
                                <td>:</td>
                                <td><input type="text" value="<?php echo $data[0]->do_id?>" class="form-control pl-4 pr-4" placeholder="0001/mm/DO/yyyy" readonly></td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="date" name="date" value="<?php echo date("d/m/Y", strtotime($data[0]->sales_date));?>" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY">
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr>
                            <!-- <tr>
                                <td>Due Date</td>
                                <td>:</td>
                                <td>
                                    <input type="text" id="due_date" name="due_date" value="<?php echo date("d/m/Y", strtotime($data[0]->sales_due_date));?>" class="form-control pl-4 pr-4" placeholder="DD/MM/YYYY">
                                    <label class="text text-danger"><?= form_error('date') ?></label>
                                </td>
                            </tr> -->
                            <tr>
                                <td>Customer name</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="customer_name" onchange="costumer_change()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="costumer_name">
                                            <option value="">--CHOOSE CUSTOMER NAME--</option>
                                            <?php

                                                foreach ($costumer as $row) {
                                                    ?>
                                                        <option <?php echo ($data[0]->sales_name_costumer == $row->costumer_name ? "selected" : "");?> value="<?php echo $row->costumer_id?>|<?php echo $row->costumer_name?>|<?php echo $row->costumer_type?>"><?php echo $row->costumer_name?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('customer_name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Customer type </td>
                                <td>:</td>
                                <td>
                                    <input type="text" value="<?php echo $data[0]->sales_type_costumer?>" id="customer_type" name="customer_type" class="form-control pl-4 pr-4" readonly>
                                    <label class="text text-danger"><?= form_error('customer_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Vehicle police number </td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="veh_police_number" value="<?php echo $data[0]->sales_vehicle_number?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('veh_police_number') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Driver name </td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="driver_name" value="<?php echo $data[0]->sales_driver_name?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('driver_name') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Material type  </td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="material_type" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option value="">--CHOOSE MATERIAL TYPE--</option>
                                            <?php

                                                foreach ($material as $row) {
                                                    ?>
                                                        <option <?php echo ($data[0]->material_type == $row->material_name ? "selected" : ""); ?> value="<?php echo $row->material_id?>|<?php echo $row->material_name?>">
                                                            <?php echo $row->material_name?>
                                                        </option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('material_type') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Material Amount </td>
                                <td>:</td>
                                <td>
                                    <input type="number" name="material_amount" class="form-control pl-4 pr-4" value="<?php echo $data[0]->sales_material_amount?>">
                                    <label class="text text-danger"><?= form_error('material_amount') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Payment</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select id="payment" name="payment" onchange="payment_type()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option <?php echo $data[0]->sales_payment == 'cash' ? "selected": ""?> value="cash">Cash</option>
                                            <option <?php echo $data[0]->sales_payment == 'credit' ? "selected": ""?> value="credit">Credit</option>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>PIT Location</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <select name="location" id="location" onchange="costumer_change()" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="costumer_name">
                                            <option value="">--CHOOSE LOCATION--</option>
                                            <?php

                                                foreach ($location as $row) {
                                                    ?>
                                                        <option <?php echo ($data[0]->sales_location == $row->pit_name ? "selected" : "");?> value="<?php echo $row->pit_name?>"><?php echo $row->pit_name?></option>
                                                    <?php
                                                }

                                            ?>
                                        </select>
                                    </div>
                                    <label class="text text-danger"><?= form_error('location') ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td>Term of Payment</td>
                                <td>:</td>
                                <td>
                                    <div class="form-group">
                                        <?php if($data[0]->sales_payment == 'credit'): ?>
                                            <select id="term_of_payment" name="term_of_payment" class="form-control selectpicker pl-4 pr-4" data-style="btn btn-link" id="exampleFormControlSelect1">
                                                <option value="">--CHOOSE TERM OF PAYMENT--</option>
                                                <option <?php echo ($data[0]->sales_term == "1" ? "selected" : "");?> value="1">N1</option>
                                                <option <?php echo ($data[0]->sales_term == "7" ? "selected" : "");?> value="7">N7</option>
                                                <option <?php echo ($data[0]->sales_term == "30" ? "selected" : "");?> value="30">N30</option>
                                                <option <?php echo ($data[0]->sales_term == "60" ? "selected" : "");?> value="60">N60</option>
                                        <?php else: ?>
                                            <select id="term_of_payment" name="term_of_payment" class="form-control selectpicker pl-4 pr-4" readonly>
                                        <?php endif; ?>
                                                
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <!-- <tr>
                                <td>Amount </td>
                                <td>:</td>
                                <td>
                                    <input type="text" name="sales_amount" value="<?php echo ($data[0]->sales_amount == null ? "0" : $data[0]->sales_amount) ?>" class="form-control pl-4 pr-4">
                                    <label class="text text-danger"><?= form_error('sales_amount') ?></label>
                                </td>
                            </tr> -->
                        </tbody>
                    </table>
                    <div class="pull-right">
                        <a href="<?php echo site_url('Page_control/ms_list'); ?>"><button type="button" class="btn btn-gray">Cancel</button></a>
                        <a><button type="submit" class="btn btn-success">Save</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
    $(document).ready(function() {
        var date_input = $('#due_date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd/mm/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        };
        date_input.datepicker(options);
    })
    function costumer_change() {
        var costumer_type = $("#costumer_name").val();
        var costumer_typearr = costumer_type.split("|");
        
        $("#customer_type").val(costumer_typearr[2]);
    }

    function payment_type() {
        var pay_type = $("#payment").val();

        if(pay_type == 'credit'){
            document.getElementById("term_of_payment").removeAttribute('readonly')
            var html = '<option value="">--CHOOSE TERM OF PAYMENT--</option>'+
                        '<option value="1">N1</option>'+
                        '<option value="7">N7</option>'+
                        '<option value="30">N30</option>'+
                        '<option value="60">N60</option>';
            $("#term_of_payment").html(html);
        }else{
            document.getElementById("term_of_payment").setAttribute('readonly', true)
            $("#term_of_payment").empty();
        }
    }

</script>