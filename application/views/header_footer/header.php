<!doctype html>
<html lang="en">

<head>
    <title>Dev elo /</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Material Kit CSS -->
    <link href="<?php echo base_url('template/') ?>assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('template/assets/') ?>datatables/datatables.min.css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

    <script src="<?php echo base_url('template/') ?>assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('template/') ?>assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('template/') ?>assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url('template/') ?>assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Chartist JS -->
    <script src="<?php echo base_url('template/') ?>assets/js/plugins/chartist.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url('template/') ?>assets/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="<?php echo base_url('template/') ?>assets/js/material-dashboard.js?v=2.1.2" type="text/javascript"></script>

    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    <script src="https://datatables.net/plug-ins/api/sum()"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url('template/assets/') ?>datatables/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
</head>

<style>
.dataTables_filter {
    display: none;
}

.nav .nav-item .sub-nav {
    transition: .2s;
}

.nav .nav-item:not(:hover) .sub-nav,
.nav .nav-item:not(.active) .sub-nav  {
    opacity: 0;
    height: 0;
    width: 0;
    margin: 0;
}

.nav .nav-item:hover .sub-nav,
.nav .nav-item.active .sub-nav {
    opacity: 1;
    width: auto;
    height: auto;
}

.nav .nav-item.active .sub-nav .nav-item.active .nav-link {
    background-color: rgba(200, 200, 200, 0.2);
    color: #3C4858;
    box-shadow: none;
}

</style>

<body>
    <?php $this->load->helper('idr_helper'); ?>