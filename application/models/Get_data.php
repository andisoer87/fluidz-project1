<?php
class Get_data extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}
    
    function query_uni($query_input){
        $hasil=$this->db->query("$query_input");
        return $hasil->result();
    }

    public function get_data($table,$condition){
        $hasil=$this->db->query("SELECT * FROM $table $condition");
        return $hasil->result();
    }

    public function get_data_row($table,$condition){
        $hasil=$this->db->query("SELECT * FROM $table $condition");
        return $hasil;
    }

    public function get_data_rows($table,$condition){
        $hasil=$this->db->query("SELECT * FROM $table $condition");
        return $hasil->num_rows();
    }
    
	function save_data($table,$values){
		$hasil=$this->db->query("INSERT INTO $table VALUES($values)");
		return $hasil;
	}
    
    function update_data($table,$data){
        $hasil=$this->db->query("UPDATE $table SET $data");
        return $hasil;
    }
    
    function delete_data($tabel,$condition){
        $hasil=$this->db->query("DELETE FROM $tabel $condition");
        return $hasil;
    }
  
}