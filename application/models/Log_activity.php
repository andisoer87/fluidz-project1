<?php
class Get_data extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
    }
    
    function create_log(){
        $tb_date = date('Y-m-d');
        $tb_name = 'log_activity_'.$tb_date;
        $query = ("CREATE TABLE IF NOT EXISTS`'.$tb_name.'` (
                    `id_log` int(11) NOT NULL AUTO_INCREMENT,
                    `date` date DEFAULT NULL,
                    `hour` time DEFAULT NULL,
                    `information` varchar(75) DEFAULT NULL,
                    PRIMARY KEY (`id_log`),
                    UNIQUE KEY `idx` (`id_log`) USING BTREE
                ) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=latin1");

        return $this->db->query($query);            
    }
	
	function log_system($info,$data_id){
		$date_now = date('Y-m-d');
		$time_now = date('h:i:s');
		$query=$this->db->query("INSERT INTO log_activity_$date_now VALUES('', '$data_id', '$date_now', '$time_now', '$info')");
		return $query;
	}
  
}