<?php defined('BASEPATH') or exit('No direct script access allowed');


    class tb_admin_m extends CI_Model
    {
        private $_table = "tb_admin";

        public $user_id;
        public $email;
        public $username;
        public $password;
        public $photo;
        public $admin_insert_date;
        public $is_delete_admin;

        public function login($email, $password) //Login By email and password;
        {
            return $this->db->get_where($this->_table, array(
                "username" => $email,
                "is_delete_admin" => '0'
                )
            );
        }

        public function editProfile($user_id, $email, $username, $photo)
        {
            $data = array(
                "username" => $email,
                "username" => $username,
                "photo" => $photo
            );

            $this->db->update($this->_table, $data, array("user_id" => $user_id));

            return $this->db->affected_rows();
        }

        public function checkPassword($user_id)
        {
            return $this->db->get_where($this->_table, array(
                "user_id" => $user_id
            ));
        }

        public function updatePassword($new_password_h, $user_id)
        {
            $data = array(
                "password" => $new_password_h
            );
            
            $this->db->update($this->_table, $data, array("user_id" => $user_id));

            return $this->db->affected_rows();
        }
    }


    

?>
