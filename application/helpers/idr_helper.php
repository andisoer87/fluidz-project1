<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('idr_format')) {
 function idr_format($set_number){
    $idr_result = number_format($set_number,0,',','.');
    return $idr_result;
 }
}
 ?>