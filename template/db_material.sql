-- MariaDB dump 10.17  Distrib 10.4.11-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: db_material
-- ------------------------------------------------------
-- Server version	10.4.11-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_admin`
--

DROP TABLE IF EXISTS `tb_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_admin` (
  `user_id` varchar(15) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `admin_insert_date` datetime DEFAULT NULL,
  `is_delete_admin` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_admin`
--

LOCK TABLES `tb_admin` WRITE;
/*!40000 ALTER TABLE `tb_admin` DISABLE KEYS */;
INSERT INTO `tb_admin` VALUES ('USR-0001','admin@email.com','admin','$2y$10$8WZP3w/NylvXNM8xp6e0SupI4l3oEze6XedLqLDC2BgAKyppJwZra',NULL,'2020-04-09 14:24:35','0'),('USR-0002','user@email.com','user','$2y$10$pKvmW4JC/IWmcZEBOdFcneI45hqhh8X1jJvnVqqq/uCMoyDx6VxHu',NULL,'2020-04-09 14:27:18','0'),('USR-0003','sukma@email.com','sukma','$2y$10$4X7aAAY6/FRhYTeBayok.ehf/C2uvKCLSQRVJOuNHq5HQwB6RfUyK',NULL,'2020-04-08 14:28:04','0'),('USR-0004','janti@gmail.com','janti','$2y$10$GUJplgGwrr7oOdwQpKv1NO2L23ggT5aJhdtUM3up0W1Wg758SZvEi',NULL,'2020-04-08 14:29:00','0'),('USR-0005','bayu@gmail.com','bayu','$2y$10$SoBWv/JwA6vBj4Y9Alf6AuF4edcIVTxNDpzuBlx9SR5YgPVKlTVku',NULL,'2020-04-08 14:29:45','0');
/*!40000 ALTER TABLE `tb_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_cash_in`
--

DROP TABLE IF EXISTS `tb_cash_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cash_in` (
  `cash_in_id` varchar(15) NOT NULL,
  `invoice_number` varchar(20) DEFAULT NULL,
  `cash_in_date` date DEFAULT NULL,
  `cash_in_costumer_id` varchar(20) DEFAULT NULL,
  `costumer_name` varchar(50) DEFAULT NULL,
  `costumer_type` enum('retail','special retail','distributor') DEFAULT NULL,
  `cash_in_material_id` varchar(20) DEFAULT NULL,
  `material_type` varchar(15) DEFAULT NULL,
  `cash_in_payment` enum('cash','credit') DEFAULT NULL,
  `cash_in_amount` int(15) DEFAULT NULL,
  `cash_in_status` enum('in','out') DEFAULT NULL,
  `cash_in_insert_date` datetime NOT NULL,
  `is_date_cash_in` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`cash_in_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cash_in`
--

LOCK TABLES `tb_cash_in` WRITE;
/*!40000 ALTER TABLE `tb_cash_in` DISABLE KEYS */;
INSERT INTO `tb_cash_in` VALUES ('CIN-0001','0001/09/INV/2020','2020-04-09','CUST-0002','nurul','retail','MTR-0001','semen','cash',2600000,'in','2020-04-09 14:40:00','0'),('CIN-0002','0002/07/INV/2020','2020-04-07','CUST-0006','Bowo','distributor','MTR-0010','plastik','credit',120000,'in','2020-04-07 19:05:14','0'),('CIN-0003','0003/06/INV/2020','2020-04-06','CUST-0010','Kanji','special retail','MTR-0005','karet','cash',1320000,'in','2020-04-09 19:05:17','0'),('CIN-0004','0004/09/INV/2020','2020-04-09','CUST-0004','Luhut','distributor','MTR-0004','baja','credit',1170000,'in','2020-04-06 19:05:20','0'),('CIN-0005','0005/07/INV/2020','2020-04-07','CUST-0007','Kariem','special retail','MTR-0003','pasir','credit',450000,'in','2020-04-07 19:05:22','0'),('CIN-0006','0006/06/INV/2020','2020-04-06','CUST-0005','Karisma','special retail','MTR-0002','bata','cash',1000000,'in','2020-04-06 19:05:46','0'),('CIN-0007','0007/04/INV/2020','2020-04-04','CUST-0008','Tito','retail','MTR-0007','batu','credit',1000000,'in','2020-04-04 19:05:49','0'),('CIN-0008','0008/01/INV/2020','2020-04-01','CUST-0003','Joko','distributor','MTR-0006','kaca','cash',2950000,'in','2020-04-01 19:05:53','0'),('CIN-0009','0009/05/INV/2020','2020-04-05','CUST-0004','Luhut','distributor','MTR-0008','aluminium','cash',460000,'in','2020-04-05 19:05:57','0'),('CIN-0010','0010/03/INV/2020','2020-04-03','CUST-0007','Kariem','special retail','MTR-0004','baja','cash',1260000,'in','2020-04-03 19:06:01','0');
/*!40000 ALTER TABLE `tb_cash_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_cash_out`
--

DROP TABLE IF EXISTS `tb_cash_out`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cash_out` (
  `cash_out_id` varchar(15) NOT NULL,
  `cash_out_date` date DEFAULT NULL,
  `cash_out_operation_id` varchar(20) DEFAULT NULL,
  `operation_type` varchar(100) DEFAULT NULL,
  `cash_out_payment` varchar(10) DEFAULT NULL,
  `cash_out_amount` int(100) DEFAULT NULL,
  `cash_out_info` text DEFAULT NULL,
  `cash_out_insert_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_delete_cash_out` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`cash_out_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cash_out`
--

LOCK TABLES `tb_cash_out` WRITE;
/*!40000 ALTER TABLE `tb_cash_out` DISABLE KEYS */;
INSERT INTO `tb_cash_out` VALUES ('COU-0001','2020-04-08','OCO-0001','Pajak','cash',1000000,NULL,'2020-04-12 15:47:30','0'),('COU-0002','2020-04-07','OCO-0002','Maintenance','credit',1400000,NULL,'2020-04-12 15:47:30','0'),('COU-0003','2020-04-09','OCO-0006','Asuransi Pegawai','cash',10000000,NULL,'2020-04-12 15:47:30','0'),('COU-0004','2020-04-09','OCO-0010','Konsumsi','cash',600000,NULL,'2020-04-12 15:47:30','0'),('COU-0005','2020-04-08','OCO-0008','Periklanan','cash',2000000,NULL,'2020-04-12 15:47:30','0'),('COU-0006','2020-04-07','OCO-0003','Gaji Pegawai','cash',3000000,NULL,'2020-04-12 15:47:30','0'),('COU-0007','2020-04-04','OCO-0005','Fasilitas umum','credit',2000000,NULL,'2020-04-12 15:47:30','0'),('COU-0008','2020-04-09','OCO-0011','Lisensi produk','credit',1300000,NULL,'2020-04-12 15:47:30','0'),('COU-0009','2020-04-07','OCO-0015','Bunga bank','cash',200000,NULL,'2020-04-12 15:47:30','0'),('COU-0010','2020-04-09','OCO-0013','Kerusakan perangkat','credit',300000,NULL,'2020-04-12 15:47:30','0');
/*!40000 ALTER TABLE `tb_cash_out` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_costumer`
--

DROP TABLE IF EXISTS `tb_costumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_costumer` (
  `costumer_id` varchar(15) NOT NULL,
  `costumer_name` varchar(50) DEFAULT NULL,
  `costumer_type` enum('retail','special_retail','distributor') DEFAULT NULL,
  `address` text DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `costumer_info` text DEFAULT NULL,
  `costumer_insert_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_delete_costumer` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`costumer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_costumer`
--

LOCK TABLES `tb_costumer` WRITE;
/*!40000 ALTER TABLE `tb_costumer` DISABLE KEYS */;
INSERT INTO `tb_costumer` VALUES ('CUST-0001','Bambang','distributor','jl.gadang gg 6','malang','0812913348585','101212-102312-2312','','2020-04-09 14:18:07','0'),('CUST-0002','Nurul','retail','Jl.Jendral sudirman no 16','Bogor','0822123747479','232132-32121-1232',NULL,'2020-04-07 15:36:07','0'),('CUST-0003','Joko','distributor','Jl.Pejuang no 67','Surabaya','0812332375751','312311-23213-1231',NULL,'2020-04-08 15:37:23','0'),('CUST-0004','Luhut','distributor','Jl.Proklamasi no 10','Jakarta','0851233156483','232131-1232-1235',NULL,'2020-04-07 15:38:19','0'),('CUST-0005','Karisma','special_retail','Jl.Karimun no 77','Jepara','0872312321312','232111-1238-2321',NULL,'2020-04-08 15:44:50','0'),('CUST-0006','Bowo','distributor','Jl.Pahlawan no 96','Yogyakarta','0872338457333','323580-3231-2131',NULL,'2020-04-08 15:46:36','0'),('CUST-0007','Kariem','special_retail','Jl.Madura no 44','Banyuwangi','0852384758199','382320-2323-1231',NULL,'2020-04-06 15:48:54','0'),('CUST-0008','Tito','retail','Jl.Kupang no 66','Bandung','0835757583399','475849-3848-4849',NULL,'2020-04-06 15:51:33','0'),('CUST-0009','Joni','retail','Jl.Jayapura no 46','Manokwari','0863293293932','348590-8483-8484',NULL,'2020-04-07 15:52:43','0'),('CUST-0010','Kanji','special_retail','Jl.Soekarno no 19','Kupang','0893747422185','838382-2384-493',NULL,'2020-04-08 15:55:54','0');
/*!40000 ALTER TABLE `tb_costumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_fuel_in`
--

DROP TABLE IF EXISTS `tb_fuel_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fuel_in` (
  `fuel_in_id` varchar(15) NOT NULL,
  `po_number` varchar(20) DEFAULT NULL,
  `fuel_in_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `fuel_in_supplier` varchar(50) DEFAULT NULL,
  `fuel_in_quantity` int(10) DEFAULT NULL,
  `unit_price` int(10) DEFAULT NULL,
  `fuel_in_total` int(15) DEFAULT NULL,
  `fuel_in_info` text DEFAULT NULL,
  `fuel_in_status` enum('in','out') DEFAULT NULL,
  `fuel_in_insert_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_delete_fuel_in` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`fuel_in_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_fuel_in`
--

LOCK TABLES `tb_fuel_in` WRITE;
/*!40000 ALTER TABLE `tb_fuel_in` DISABLE KEYS */;
INSERT INTO `tb_fuel_in` VALUES ('IN-0001','923282-2323','2020-04-09','2020-04-11','PT.Sinar Jaya',2000,100000,200000000,NULL,'in','2020-04-09 14:59:21','0'),('IN-0002','321123-2321','2020-04-08','2020-04-12','PT.Mahmud ID',1000,98000,98000000,NULL,'in','2020-04-08 15:01:43','0'),('IN-0003','323231-4433','2020-04-09','2020-04-11','PT.Gelap Gulita',3000,110000,330000000,NULL,'in','2020-04-08 15:03:35','0'),('IN-0004','545455-2131','2020-04-09','2020-04-13','PT.Mari Sejahtera',1000,100000,100000000,NULL,'in','2020-04-09 15:05:10','0'),('IN-0005','313121-2311','2020-04-09','2020-04-10','PT.Bankrut Sejahtera',1000,90000,90000000,NULL,'in','2020-04-09 15:07:49','0'),('IN-0006','323111-2999','2020-04-08','2020-04-12','PT.Kanjeng Jaya',2000,90000,18000000,NULL,'in','2020-04-10 15:13:04','0'),('IN-0007','312321-8484','2020-04-08','2020-04-11','PT.Habib Gulita',1000,100000,100000000,'','in','2020-04-08 15:15:34','0'),('IN-0008','323134-5885','2020-04-08','2020-04-13','PT.Pastur ID',2000,100000,200000000,NULL,'in','2020-04-08 15:17:32','0'),('IN-0009','445452-8859','2020-04-09','2020-04-10','PT.Danbo EN',2000,90000,18000000,NULL,'in','2020-04-09 15:20:34','0'),('IN-0010','434341-2344','2020-04-08','2020-04-11','PT.Luhut Bintaro',1000,100000,100000000,NULL,'in','2020-04-08 15:21:55','0');
/*!40000 ALTER TABLE `tb_fuel_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_fuel_out`
--

DROP TABLE IF EXISTS `tb_fuel_out`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_fuel_out` (
  `fuel_out_id` varchar(15) NOT NULL,
  `fuel_out_date` date DEFAULT NULL,
  `fuel_out_name` varchar(60) DEFAULT NULL,
  `fuel_out_quantity` int(10) DEFAULT NULL,
  `fuel_out_info` text DEFAULT NULL,
  `fuel_out_status` enum('0','1') DEFAULT '0',
  `is_delete_fuel_out` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`fuel_out_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_fuel_out`
--

LOCK TABLES `tb_fuel_out` WRITE;
/*!40000 ALTER TABLE `tb_fuel_out` DISABLE KEYS */;
INSERT INTO `tb_fuel_out` VALUES ('OUT-0001','2020-04-08','Sell to PT.Bambang I',300,NULL,NULL,NULL),('OUT-0002','2020-04-07','Sell to PT.Joko Purn',600,NULL,NULL,NULL),('OUT-0003','2020-04-08','Logistik',300,NULL,NULL,NULL),('OUT-0004','2020-04-09','Sell PT.Doge EN',600,NULL,NULL,NULL),('OUT-0005','2020-04-08','Logistik',500,NULL,NULL,NULL),('OUT-0006','2020-04-08','Logistik',200,NULL,NULL,NULL),('OUT-0007','2020-04-08','Sell to PT.MAju Bers',600,NULL,NULL,NULL),('OUT-0008','2020-04-09','Logistik',400,NULL,NULL,NULL),('OUT-0009','2020-04-08','Logistik',300,NULL,NULL,NULL),('OUT-0010','2020-04-08','Sell to PT Berkabung',200,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tb_fuel_out` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_history`
--

DROP TABLE IF EXISTS `tb_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_history` (
  `history_id` varchar(20) NOT NULL,
  `action_description` varchar(100) DEFAULT NULL,
  `date` year(4) DEFAULT NULL,
  `original_table` varchar(100) DEFAULT NULL,
  `action_type` varchar(30) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_history`
--

LOCK TABLES `tb_history` WRITE;
/*!40000 ALTER TABLE `tb_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_invoice`
--

DROP TABLE IF EXISTS `tb_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_invoice` (
  `invoice_id` varchar(20) NOT NULL,
  `do_number` varchar(20) DEFAULT NULL,
  `spb_number` varchar(20) DEFAULT NULL,
  `invoice_costumer_id` varchar(20) DEFAULT NULL,
  `invoice_cash_in_id` varchar(20) DEFAULT NULL,
  `invoice_sales_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_invoice`
--

LOCK TABLES `tb_invoice` WRITE;
/*!40000 ALTER TABLE `tb_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_material`
--

DROP TABLE IF EXISTS `tb_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_material` (
  `material_id` varchar(15) NOT NULL,
  `material_name` varchar(20) DEFAULT NULL,
  `retail_price` int(10) DEFAULT NULL,
  `special_price` int(10) DEFAULT NULL,
  `distributor_price` int(10) DEFAULT NULL,
  `material_stock` int(10) DEFAULT NULL,
  `material_info` text DEFAULT NULL,
  `material_status` enum('0','1') DEFAULT NULL,
  `material_insert_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_delete_material` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_material`
--

LOCK TABLES `tb_material` WRITE;
/*!40000 ALTER TABLE `tb_material` DISABLE KEYS */;
INSERT INTO `tb_material` VALUES ('MTR-0001','semen',130000,150000,120000,1000,NULL,'1','2020-04-09 14:41:31','0'),('MTR-0002','bata',24000,25000,22000,5000,NULL,'1','2020-04-09 14:43:36','0'),('MTR-0003','pasir',13000,15000,12000,6000,NULL,'1','2020-04-09 14:46:50','0'),('MTR-0004','baja',40000,42000,39000,2000,NULL,'1','2020-04-09 14:48:41','0'),('MTR-0005','karet',30000,33000,28000,3000,NULL,'1','2020-04-09 14:49:50','0'),('MTR-0006','kaca',60000,63000,59000,1500,NULL,'1','2020-04-08 14:50:40','0'),('MTR-0007','batu',20000,23000,18000,3000,NULL,'1','2020-04-08 14:51:28','0'),('MTR-0008','aluminium',28000,30000,23000,4000,NULL,'1','2020-04-08 14:52:39','0'),('MTR-0009','polimer',40000,43000,39000,2000,NULL,'1','2020-04-08 14:53:24','0'),('MTR-0010','plastik',13000,15000,12000,3000,NULL,'1','2020-04-09 14:54:07','0');
/*!40000 ALTER TABLE `tb_material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_operation_cost`
--

DROP TABLE IF EXISTS `tb_operation_cost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_operation_cost` (
  `ops_cost_id` varchar(15) NOT NULL,
  `ops_cost_name` varchar(60) DEFAULT NULL,
  `ops_cost_info` text DEFAULT NULL,
  `cost_insert_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_delete_cost` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`ops_cost_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_operation_cost`
--

LOCK TABLES `tb_operation_cost` WRITE;
/*!40000 ALTER TABLE `tb_operation_cost` DISABLE KEYS */;
INSERT INTO `tb_operation_cost` VALUES ('OCO-0001','Pajak','pajak dari pemerintah','2020-04-09 18:39:38','0'),('OCO-0002','Maintenance',NULL,'2020-04-09 18:39:41','0'),('OCO-0003','Gaji pegawai',NULL,'2020-04-09 18:39:44','0'),('OCO-0004','Pembelian Bahan bakar',NULL,'2020-04-09 18:39:46','0'),('OCO-0005','Fasilitas umum',NULL,'2020-04-09 18:39:48','0'),('OCO-0006','Asuransi pegawai',NULL,'2020-04-09 18:39:50','0'),('OCO-0007','Pembelian perangkat ',NULL,'2020-04-09 18:39:53','0'),('OCO-0008','Periklanan',NULL,'2020-04-09 18:39:55','0'),('OCO-0009','Biaya sewa','biaya untuk sewa bangunan','2020-04-09 18:39:57','0'),('OCO-0010','Konsumsi','Konsumsi pegawai','2020-04-09 18:40:04','0'),('OCO-0011','Lisensi produk',NULL,'2020-04-07 19:57:25','0'),('OCO-0012','Biaya transportasi',NULL,'2020-04-09 19:57:29','0'),('OCO-0013','Kerusakan perangkat',NULL,'2020-04-09 19:57:31','0'),('OCO-0014','Biaya suplai',NULL,'2020-04-09 19:57:34','0'),('OCO-0015','Bunga bank',NULL,'2020-04-09 19:57:36','0');
/*!40000 ALTER TABLE `tb_operation_cost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_operation_exp`
--

DROP TABLE IF EXISTS `tb_operation_exp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_operation_exp` (
  `ops_exp_id` varchar(15) NOT NULL,
  `ops_exp_name` varchar(60) DEFAULT NULL,
  `ops_exp_info` text DEFAULT NULL,
  `exp_insert_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_delete_exp` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`ops_exp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_operation_exp`
--

LOCK TABLES `tb_operation_exp` WRITE;
/*!40000 ALTER TABLE `tb_operation_exp` DISABLE KEYS */;
INSERT INTO `tb_operation_exp` VALUES ('OEX-0001','Pembayaran lisensi',NULL,'2020-04-09 18:48:05','0'),('OEX-0002','Pembayaran listrik',NULL,'2020-04-09 18:48:09','0'),('OEX-0003','Penggantian produk',NULL,'2020-04-07 18:48:11','0'),('OEX-0004','Pembayaran pajak',NULL,'2020-04-08 18:48:15','0'),('OEX-0005','Pembayaran transport',NULL,'2020-04-08 18:48:19','0'),('OEX-0006','Pembayaran iklan',NULL,'2020-04-08 18:48:23','0'),('OEX-0007','Pembayaran asuransi','Pembayaran asuransi milik pegawai','2020-04-08 18:48:28','0'),('OEX-0008','Pembelian peralatan ',NULL,'2020-04-09 18:48:32','0'),('OEX-0009','Pembayaran suku bunga','Pembayaran suku bunga bank perusahaan','2020-04-09 18:48:35','0'),('OEX-0010','Pembayaran air',NULL,'2020-04-09 18:48:38','0');
/*!40000 ALTER TABLE `tb_operation_exp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_sales`
--

DROP TABLE IF EXISTS `tb_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_sales` (
  `do_id` varchar(20) DEFAULT NULL,
  `spb_id` varchar(20) DEFAULT NULL,
  `sales_id` varchar(15) NOT NULL,
  `sales_costumer_id` varchar(20) DEFAULT NULL,
  `sales_date` date DEFAULT NULL,
  `sales_name_costumer` varchar(100) DEFAULT NULL,
  `sales_type_costumer` enum('retail','special retail','distributor') DEFAULT NULL,
  `sales_vehicle_number` varchar(30) DEFAULT NULL,
  `sales_driver_name` varchar(255) DEFAULT NULL,
  `sales_material_id` varchar(20) DEFAULT NULL,
  `material_type` varchar(15) DEFAULT NULL,
  `sales_payment` varchar(20) DEFAULT NULL,
  `sales_location` varchar(30) DEFAULT NULL,
  `sales_status` enum('0','1') DEFAULT NULL,
  `sales_insert_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_delete_sales` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_sales`
--

LOCK TABLES `tb_sales` WRITE;
/*!40000 ALTER TABLE `tb_sales` DISABLE KEYS */;
INSERT INTO `tb_sales` VALUES ('0001/07/DO/2020','0001/09/SPB/2020','SLS-0001','SLS-0002','2020-04-07','nurul','retail','N-12PDS','Dimas','MTR-0001','semen','cash','bandung','1','2020-04-12 15:37:26','0'),('0002/08/DO/2020','0002/08/SPB/2020','SLS-0002','SLS-0006','2020-04-08','Bowo','distributor','A-39DJS','Ovel','MTR-0010','plastik','credit','denpasar','1','2020-04-12 15:37:29','0'),('0003/07/DO/2020','0003/07/SPB/2020','SLS-0003','SLS-0010','2020-04-07','Kanji','special retail','K-02SKD','Nur','MTR-0005','karet','cash','kupang','1','2020-04-12 15:37:33','0'),('0004/09/DO/2020','0004/09/SPB/2020','SLS-0004','SLS-0004','2020-04-09','Luhut','distributor','O-32KW','Hadi','MTR-0004','baja','credit','pontianak','1','2020-04-12 15:37:36','0'),('0005/06/DO/2020','0005/06/SPB/2020','SLS-0005','SLS-0007','2020-04-06','Kariem','special retail','P-23WOS','Cholis','MTR-0003','pasir','credit','jayapura','1','2020-04-12 15:37:40','0'),('0006/04/DO/2020','0006/04/SPB/2020','SLS-0006','SLS-0005','2020-04-04','Karisma','special retail','S-25DSO','Kholiq','MTR-0002','bata','cash','Jakarta','1','2020-04-12 15:37:43','0'),('0007/09/DO/2020','0007/09/SPB/2020','SLS-0007','SLS-0008','2020-04-09','Tito','retail','A-23SDA','Plate','MTR-0007','batu','credit','Surabaya','1','2020-04-12 15:37:46','0'),('0008/09/DO/2020','0008/09/SPB/2020','SLS-0008','SLS-0003','2020-04-08','Joko','distributor','T-12SDA','David','MTR-0006','kaca','cash','Pontianak','1','2020-04-12 15:37:49','0'),('0009/07/DO/2020','0009/07/SPB/2020','SLS-0009','SLS-0004','2020-04-07','Luhut','distributor','U-10DSA','Agus','MTR-0008','aluminium','cash','Banten','1','2020-04-12 15:37:52','0'),('0010/06/DO/2020','0010/06/SPB/2020','SLS-0010','SLS-0007','2020-04-06','Kariem','special retail','I-88SDKA','Bambang','MTR-0004','baja','cash','Jakarta','1','2020-04-12 15:37:59','0');
/*!40000 ALTER TABLE `tb_sales` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-12 18:09:39
