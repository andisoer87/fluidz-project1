/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : db_material

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-06-25 16:06:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin` (
  `user_id` varchar(15) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `admin_level` varchar(20) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `admin_insert_date` datetime DEFAULT NULL,
  `is_delete_admin` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES ('USR-0001', 'admin@email.com', 'admin', '$2y$10$WsVTB1nNM9CS3UBoHPtx9uDmVah0pz5V7Y5e1RjphAEz/mU0cxpw.', 'admin', null, '2020-04-09 14:24:35', '0');
INSERT INTO `tb_admin` VALUES ('USR-0002', 'user@email.com  ', 'user', '$2y$10$pKvmW4JC/IWmcZEBOdFcneI45hqhh8X1jJvnVqqq/uCMoyDx6VxHu', 'admin', null, '2020-04-09 14:27:18', '0');
INSERT INTO `tb_admin` VALUES ('USR-0003', 'sukma@email.com', 'sukma', '$2y$10$4X7aAAY6/FRhYTeBayok.ehf/C2uvKCLSQRVJOuNHq5HQwB6RfUyK', 'officer', null, '2020-04-08 14:28:04', '0');
INSERT INTO `tb_admin` VALUES ('USR-0004', 'janti@gmail.com', 'janti', '$2y$10$GUJplgGwrr7oOdwQpKv1NO2L23ggT5aJhdtUM3up0W1Wg758SZvEi', 'officer', null, '2020-04-08 14:29:00', '0');
INSERT INTO `tb_admin` VALUES ('USR-0005', 'bayu@gmail.com', 'bayu', '$2y$10$SoBWv/JwA6vBj4Y9Alf6AuF4edcIVTxNDpzuBlx9SR5YgPVKlTVku', 'officer', null, '2020-04-08 14:29:45', '0');
INSERT INTO `tb_admin` VALUES ('USR-0006', 'dayyan@email.coms', 'dayyan s', '$2y$10$pKvmW4JC/IWmcZEBOdFcneI45hqhh8X1jJvnVqqq/uCMoyDx6VxHu', 'admin', '', '2020-05-14 22:51:55', '0');
INSERT INTO `tb_admin` VALUES ('USR-0007', 'achazzam91@gmail.com', 'Azzam', '$2y$10$UXhgEWfGP.nSNpxUgp65SexKr1j8mYIEpwbqJIxsqaAQRT1g4nNMW', 'manager', '', '2020-06-22 14:15:45', '0');

-- ----------------------------
-- Table structure for tb_balance
-- ----------------------------
DROP TABLE IF EXISTS `tb_balance`;
CREATE TABLE `tb_balance` (
  `balance_id` varchar(15) NOT NULL,
  `balance_name` varchar(30) DEFAULT NULL,
  `balance_info` text,
  `balance_operation` varchar(255) DEFAULT NULL,
  `balance_status` enum('1','0') NOT NULL DEFAULT '0',
  `is_delete_balance` enum('1','0') NOT NULL DEFAULT '0',
  `balance_created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`balance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_balance
-- ----------------------------
INSERT INTO `tb_balance` VALUES ('BS-0001', 'Penjualan pertama', null, 'cash in', '0', '0', '2020-04-21 02:51:18');
INSERT INTO `tb_balance` VALUES ('BS-0002', 'THR', null, 'cash out', '0', '0', '2020-02-18 02:51:21');
INSERT INTO `tb_balance` VALUES ('BS-0003', 'Santunan Pegawai', null, 'cash out', '0', '0', '2020-04-14 02:51:26');
INSERT INTO `tb_balance` VALUES ('BS-0004', 'Penjualan kedua', null, 'cash in', '0', '0', '2020-02-12 02:51:32');
INSERT INTO `tb_balance` VALUES ('BS-0005', 'Pajak', null, 'cash out', '0', '0', '2020-02-18 02:51:36');
INSERT INTO `tb_balance` VALUES ('BS-0006', 'Penjualan keempat', 'bruhs                                                       ', 'cash in', '0', '0', '2020-03-11 02:51:42');
INSERT INTO `tb_balance` VALUES ('BS-0007', 'panjualan keempat', null, 'cash in', '0', '0', '2020-04-01 02:51:46');
INSERT INTO `tb_balance` VALUES ('BS-0008', 'Pajak', null, 'cash out', '0', '0', '2020-03-24 02:51:51');
INSERT INTO `tb_balance` VALUES ('BS-0009', 'Penjualan kelima', '                                            kok                                                                                  ', 'cash out', '0', '0', '2020-03-18 02:51:56');
INSERT INTO `tb_balance` VALUES ('BS-0010', 'Penjualan keenam', null, 'cash in', '0', '0', '2020-01-14 02:52:01');
INSERT INTO `tb_balance` VALUES ('BS-0011', 'adads', 'adadda', 'cash out', '0', '1', '2020-05-13 02:55:21');

-- ----------------------------
-- Table structure for tb_cash_in
-- ----------------------------
DROP TABLE IF EXISTS `tb_cash_in`;
CREATE TABLE `tb_cash_in` (
  `cash_in_id` varchar(15) NOT NULL,
  `invoice_number` varchar(20) DEFAULT NULL,
  `cash_in_date` date DEFAULT NULL,
  `cash_in_costumer_id` varchar(20) DEFAULT NULL,
  `costumer_name` varchar(50) DEFAULT NULL,
  `costumer_type` varchar(15) DEFAULT NULL,
  `cash_in_material_id` varchar(20) DEFAULT NULL,
  `material_type` varchar(15) DEFAULT NULL,
  `cash_in_payment` varchar(10) DEFAULT NULL,
  `cash_in_amount` int(15) DEFAULT NULL,
  `cash_in_status` varchar(10) DEFAULT NULL,
  `cash_in_source` varchar(16) DEFAULT NULL,
  `cash_in_insert_date` datetime(6) DEFAULT NULL,
  `is_date_cash_in` enum('0','1') NOT NULL DEFAULT '0',
  `cash_in_info` text,
  PRIMARY KEY (`cash_in_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_cash_in
-- ----------------------------
INSERT INTO `tb_cash_in` VALUES ('CIN-0001', '0001/09/INV/2020', '2020-04-10', 'CUST-0003', 'Joko', 'Distributor', 'MTR-0002', 'bata', 'Kas', '2600000', '', 'invoice', '2020-05-12 20:39:28.138996', '0', '');
INSERT INTO `tb_cash_in` VALUES ('CIN-0002', '0002/07/INV/2020', '2020-04-07', 'CUST-0006', 'Bowo', '', 'MTR-0010', 'plastik', 'Kas', '120000', '', 'invoice', '2020-05-12 20:41:42.772624', '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0003', '0003/06/INV/2020', '2020-04-07', 'CUST-0010', 'Kanji', 'Special retail', 'MTR-0005', 'karet', 'Bank', '1320000', '', 'invoice', '2020-05-12 20:39:49.359585', '0', '');
INSERT INTO `tb_cash_in` VALUES ('CIN-0004', '0004/09/INV/2020', '2020-04-09', 'CUST-0004', 'Luhut', '', 'MTR-0004', 'baja', 'Bank', '1170000', '', 'invoice', '2020-05-12 20:39:43.397063', '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0005', '0005/07/INV/2020', '2020-04-07', 'CUST-0007', 'Kariem', '', 'MTR-0003', 'pasir', 'Kas', '450000', '', 'invoice', '2020-05-12 20:39:33.233514', '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0006', '0006/06/INV/2020', '2020-04-06', 'CUST-0005', 'Karisma', '', 'MTR-0002', 'bata', 'Bank', '1000000', '', 'invoice', '2020-05-12 20:39:36.804626', '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0007', '0007/04/INV/2020', '2020-04-04', 'CUST-0008', 'Tito', '', 'MTR-0007', 'batu', 'Bank', '1000000', '', 'invoice', '2020-05-12 20:41:38.649691', '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0008', '0008/01/INV/2020', '2020-04-01', 'CUST-0003', 'Joko', 'Distributor', 'MTR-0006', 'kaca', 'Bank', '2950000', '', 'invoice', '2020-05-12 20:39:55.849269', '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0009', '0009/05/INV/2020', '2020-04-05', 'CUST-0004', 'Luhut', '', 'MTR-0008', 'aluminium', 'Bank', '460000', '', 'invoice', '2020-05-12 20:40:06.082318', '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0010', '0010/03/INV/2020', '2020-04-03', 'CUST-0007', 'Kariem', '', 'MTR-0004', 'baja', 'Kas', '1260000', '', 'invoice', '2020-05-12 20:39:58.177369', '1', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0014', '0014/04/INV/2020', '2020-04-03', 'CUST-0004', 'Luhut', '', 'MTR-0008', 'aluminium', 'Kas', '3000000', null, 'invoice', '2020-05-12 20:42:13.242229', '1', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0015', '0014/04/INV/2020', '2020-04-06', 'CUST-0004', 'Luhut', '', 'MTR-0008', 'aluminium', 'Kas', '3000000', null, 'invoice', '2020-05-12 20:42:23.642417', '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0016', '0001/07/INV/2020', '2020-04-01', 'CUST-0004', 'Luhut', '', 'MTR-0008', 'aluminium', 'Bank', '12500000', null, 'invoice', '2020-05-12 20:53:02.985308', '0', 'bruh');
INSERT INTO `tb_cash_in` VALUES ('CIN-0017', '0014/04/INV/2020', null, 'CUST-0004', 'Luhut', 'Distributor', 'MTR-0008', 'aluminium', 'Bank', '3000000', null, 'invoice', '0000-00-00 00:00:00.000000', '1', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0018', '0013/04/INV/2020', null, 'CUST-0008', 'Tito', 'Retail', 'MTR-0009', 'polimer', 'Kas', '1000000', null, 'invoice', null, '1', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0019', '0012/04/INV/2020', '2020-05-14', null, 'Bowo', 'Distributor', null, 'aluminium', 'Kas', '1000000', null, 'invoice', null, '0', 'dasdasada');
INSERT INTO `tb_cash_in` VALUES ('CIN-0020', '0001/09/INV/2020', '2020-05-15', 'CUST-0003', 'Joko', 'Distributor', 'MTR-0006', 'kaca', 'Bank', '200000', null, 'invoice', null, '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0021', 'BS-0004', '2020-05-16', 'CUST-0001', 'Bambang', 'Distributor', 'MTR-0003', 'pasir', 'Kas', '12312', null, 'balance', null, '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0022', '0017/05/INV/2020', '2020-05-23', null, 'Nurul', 'Retail', null, 'semen', 'Bank', '1950000', null, 'invoice', null, '0', 'bangsat');
INSERT INTO `tb_cash_in` VALUES ('CIN-0023', '0019/05/INV/2020', '2020-05-24', 'CUST-0009', 'Joni', 'Retail', 'MTR-0004', 'baja', 'Bank', '800000', null, 'invoice', null, '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0024', '0018/05/INV/2020', '2020-05-24', 'CUST-0003', 'Joko', 'Distributor', 'MTR-0003', 'pasir', 'Kas', '1200000', null, 'invoice', null, '0', null);
INSERT INTO `tb_cash_in` VALUES ('CIN-0025', '0013/04/INV/2020', '2020-05-24', 'CUST-0008', 'Tito', 'Retail', 'MTR-0009', 'polimer', 'Bank', '1000000', null, 'invoice', null, '0', 'O FLORA                ');
INSERT INTO `tb_cash_in` VALUES ('CIN-0026', null, '2020-05-24', null, null, null, null, null, 'Bank', '1001', null, 'balance', null, '0', '                                    Cleric                                ');
INSERT INTO `tb_cash_in` VALUES ('CIN-0027', 'BS-0007', '2020-05-25', null, null, null, null, null, 'Bank', '2000', null, 'balance', null, '0', 'FAG');
INSERT INTO `tb_cash_in` VALUES ('CIN-0028', '0001/07/INV/2020', '2020-05-25', null, 'Nurul', 'Retail', null, 'semen', 'Kas', '1500000', null, 'invoice', null, '0', 'PRAISE KEK');
INSERT INTO `tb_cash_in` VALUES ('CIN-0029', '0020/05/INV/2020', '2020-05-25', null, 'Joko', 'Distributor', null, 'pasir', 'Kas', '144000', null, 'invoice', null, '0', 'AW');
INSERT INTO `tb_cash_in` VALUES ('CIN-0030', '0021/05/INV/2020', '2020-05-29', 'CUST-0004', 'Luhut', 'Distributor', 'MTR-0003', 'pasir', 'Kas', '144000', null, 'invoice', null, '0', 'sDM');
INSERT INTO `tb_cash_in` VALUES ('CIN-0031', 'BS-0006', '2020-05-29', null, null, null, null, null, 'Kas', '20000', null, 'balance', null, '0', 'Dayyan');
INSERT INTO `tb_cash_in` VALUES ('CIN-0032', 'BS-0007', '2020-05-29', null, null, null, null, null, 'Kas', '1000', null, 'balance', null, '0', '');
INSERT INTO `tb_cash_in` VALUES ('CIN-0033', '0004/09/INV/2020', '2020-05-30', 'CUST-0004', 'Luhut', 'Distributor', 'MTR-0004', 'baja', 'Bank', '700000', null, 'invoice', null, '0', 'Pemerintah');
INSERT INTO `tb_cash_in` VALUES ('CIN-0034', 'BS-0004', '2020-05-30', null, null, null, null, null, 'Kas', '200000', null, 'balance', null, '0', 'Oposisi');
INSERT INTO `tb_cash_in` VALUES ('CIN-0035', '0011/04/INV/2020', '2020-06-01', 'CUST-0005', 'Karisma', 'Special retail', 'MTR-0005', 'karet', 'Kas', '2000000', null, 'invoice', null, '0', '');
INSERT INTO `tb_cash_in` VALUES ('CIN-0036', 'BS-0001', '2020-06-21', null, null, null, null, null, 'Kas', '240000', null, 'balance', null, '0', 'ffs');
INSERT INTO `tb_cash_in` VALUES ('CIN-0037', 'BS-0001', '2020-06-23', null, null, null, null, null, 'Kas', '700000', null, 'balance', null, '0', '');
INSERT INTO `tb_cash_in` VALUES ('CIN-0038', 'BS-0001', '2020-06-25', null, null, null, null, null, 'Kas', '2100000', null, 'balance', null, '1', '');

-- ----------------------------
-- Table structure for tb_cash_out
-- ----------------------------
DROP TABLE IF EXISTS `tb_cash_out`;
CREATE TABLE `tb_cash_out` (
  `cash_out_id` varchar(15) NOT NULL,
  `cash_out_date` date DEFAULT NULL,
  `cash_out_operation_id` varchar(20) DEFAULT NULL,
  `operation_type` varchar(100) DEFAULT NULL,
  `cash_out_payment` varchar(10) DEFAULT NULL,
  `cash_out_amount` int(100) DEFAULT NULL,
  `cash_out_info` text,
  `cash_out_insert_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete_cash_out` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`cash_out_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_cash_out
-- ----------------------------
INSERT INTO `tb_cash_out` VALUES ('COU-0001', '2020-04-08', 'OCO-0001', 'Pajak', 'Kas', '1000000', null, '2020-05-30 00:40:36', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0002', '2020-04-07', 'OCO-0002', 'Maintenance', 'Bank', '1400000', null, '2020-05-30 00:40:53', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0003', '2020-04-09', 'OCO-0006', 'Asuransi Pegawai', 'Kas', '10000000', null, '2020-05-30 00:40:36', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0004', '2020-04-09', 'OCO-0010', 'Konsumsi', 'Kas', '600000', null, '2020-05-30 00:40:36', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0005', '2020-04-08', 'OCO-0008', 'Periklanan', 'Kas', '2000000', null, '2020-05-30 00:40:36', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0006', '2020-04-07', 'OCO-0003', 'Gaji Pegawai', 'Kas', '3000000', null, '2020-05-30 00:40:36', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0007', '2020-04-04', 'OCO-0005', 'Fasilitas umum', 'Bank', '2000000', null, '2020-05-30 00:40:53', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0008', '2020-04-09', 'OCO-0011', 'Lisensi produk', 'Bank', '1300000', null, '2020-05-30 00:40:53', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0009', '2020-04-07', 'OCO-0015', 'Bunga Bank', 'Bank', '200000', '', '2020-05-30 00:40:53', '1');
INSERT INTO `tb_cash_out` VALUES ('COU-0010', '2020-04-09', 'OCO-0011', 'Lisensi produk', 'Kas', '300000', '', '2020-05-30 00:40:36', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0011', '2020-04-06', 'OCO-1283', 'Asuransi', 'Bank', '1000000', '', '2020-05-30 00:40:53', '1');
INSERT INTO `tb_cash_out` VALUES ('COU-0012', '2020-04-05', 'OCO-0004', 'Pembelian Bahan bakar', 'Bank', '1500000', 'buat korupsi', '2020-05-30 00:40:53', '1');
INSERT INTO `tb_cash_out` VALUES ('COU-0013', '2020-05-04', 'OEX-0002', 'Pembayaran listrik', 'Kas', '20000', 'dada', '2020-05-30 00:40:36', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0014', '2020-05-18', 'OEX-0007', 'Pembayaran asuransi', 'Kas', '100000', 'saasa', '2020-05-30 00:40:36', '0');
INSERT INTO `tb_cash_out` VALUES ('COU-0015', '2020-05-04', 'BS-0003', 'Santunan Pegawai', 'Bank', '300000', '', '2020-05-30 00:40:53', '0');

-- ----------------------------
-- Table structure for tb_costumer
-- ----------------------------
DROP TABLE IF EXISTS `tb_costumer`;
CREATE TABLE `tb_costumer` (
  `costumer_id` varchar(15) NOT NULL,
  `costumer_name` varchar(50) DEFAULT NULL,
  `costumer_type` enum('Retail','Special retail','Distributor') DEFAULT NULL,
  `address` text,
  `city` varchar(20) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `fax` varchar(25) DEFAULT NULL,
  `costumer_info` text,
  `costumer_insert_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete_costumer` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`costumer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_costumer
-- ----------------------------
INSERT INTO `tb_costumer` VALUES ('CUST-0001', 'Bambang', 'Distributor', 'jl.Gadang gg 6', 'Malang', '0812913348585', '101212-12312-2312', '', '2020-04-13 14:21:13', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0002', 'Nurul', 'Retail', 'Jl.Jendral sudirman no 16', 'Bogor', '0822123747479', '344312-23127-4542', null, '2020-04-13 14:21:07', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0003', 'Joko', 'Distributor', 'Jl.Pejuang no 67', 'Surabaya', '0812332375751', '312311-23213-1231', null, '2020-04-08 15:37:23', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0004', 'Luhut', 'Distributor', 'Jl.Proklamasi no 10', 'Jakarta', '0851233156483', '232131-1232-1235', null, '2020-04-07 15:38:19', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0005', 'Karisma', 'Special retail', 'Jl.Karimun no 77', 'Jepara', '0872312321312', '232111-1238-2321', null, '2020-04-14 17:51:36', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0006', 'Bowo', 'Distributor', 'Jl.Pahlawan no 96', 'Yogyakarta', '0872338457333', '323580-3231-2131', null, '2020-04-08 15:46:36', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0007', 'Kariem', 'Special retail', 'Jl.Madura no 44', 'Banyuwangi', '0852384758199', '382320-2323-1231', null, '2020-04-14 17:51:39', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0008', 'Tito', 'Retail', 'Jl.Kupang no 66', 'Bandung', '0835757583399', '475849-3848-4849', null, '2020-04-06 15:51:33', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0009', 'Joni', 'Retail', 'Jl.Jayapura no 46', 'Manokwari', '0863293293932', '348590-8483-8484', null, '2020-04-07 15:52:43', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0010', 'Kanji', 'Special retail', 'Jl.Soekarno no 19', 'Kupang', '0893747422185', '838382-2384-493', null, '2020-05-13 16:33:59', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0011', 'Habib', 'Distributor', 'jl. Siliwangi no 31', 'Malang', '081736492754', '11268589190', 'Muantap', '2020-04-13 09:48:29', '0');
INSERT INTO `tb_costumer` VALUES ('CUST-0012', 'zon', 'Retail', 'jl.pegangsaan no 50', 'Jakarta', '08675943221', '8328831928483', 'kader', '2020-04-14 17:54:52', '1');
INSERT INTO `tb_costumer` VALUES ('CUST-0013', 'sukar', 'Distributor', 'jl. Merdeka  no 30', 'Jakarta', '08441654645', '312341456546', '', '2020-04-15 10:58:04', '0');

-- ----------------------------
-- Table structure for tb_fuel_in
-- ----------------------------
DROP TABLE IF EXISTS `tb_fuel_in`;
CREATE TABLE `tb_fuel_in` (
  `fuel_in_id` varchar(15) NOT NULL,
  `po_number` varchar(20) DEFAULT NULL,
  `fuel_in_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `fuel_in_desc` varchar(100) DEFAULT NULL,
  `fuel_in_supplier` varchar(50) DEFAULT NULL,
  `fuel_in_supplier_address` varchar(255) DEFAULT NULL,
  `fuel_in_supplier_phone` varchar(20) DEFAULT NULL,
  `fuel_in_supplier_fax` varchar(30) DEFAULT NULL,
  `fuel_in_quantity` int(10) DEFAULT NULL,
  `fuel_in_real` int(10) DEFAULT NULL,
  `unit_price` int(15) DEFAULT NULL,
  `fuel_in_total` int(30) DEFAULT NULL,
  `fuel_in_info` text,
  `fuel_in_status` enum('in','out') DEFAULT NULL,
  `gr_status` enum('1','0') DEFAULT '0',
  `fuel_in_insert_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete_fuel_in` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`fuel_in_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_fuel_in
-- ----------------------------
INSERT INTO `tb_fuel_in` VALUES ('IN-0001', 'PO-0001', '2020-04-09', '2020-04-11', null, 'PT.Sinar Jaya', null, null, null, '2000', '1800', '100000', '200000000', null, 'in', '1', '2020-05-16 19:58:08', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0002', 'PO-0002', '2020-04-08', '2020-04-12', null, 'PT.Mahmud ID', null, null, null, '1000', '800', '98000', '98000000', null, 'in', '1', '2020-05-16 19:58:13', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0003', 'PO-0003', '2020-04-09', '2020-04-11', null, 'PT.Gelap Gulita', null, null, null, '3000', '2500', '110000', '330000000', null, 'in', '1', '2020-05-16 19:58:15', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0004', 'PO-0004', '2020-04-09', '2020-04-13', null, 'PT.Mari Sejahtera', null, null, null, '1000', null, '100000', '100000000', null, 'in', '0', '2020-05-29 14:19:56', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0005', 'PO-0005', '2020-04-09', '2020-04-10', null, 'PT.Bankrut Sejahtera', null, null, null, '1000', null, '90000', '90000000', null, 'in', '0', '2020-05-29 14:19:56', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0006', 'PO-0006', '2020-04-08', '2020-04-12', null, 'PT.Kanjeng Jaya', null, null, null, '2000', null, '90000', '18000000', null, 'in', '0', '2020-05-29 14:19:56', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0007', 'PO-0007', '2020-04-08', '2020-04-11', null, 'PT.Habib Gulita', null, null, null, '1000', null, '100000', '100000000', '', 'in', '0', '2020-05-29 14:19:56', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0008', 'PO-0008', '2020-04-08', '2020-04-13', null, 'PT.Pastur ID', null, null, null, '2000', null, '100000', '200000000', null, 'in', '0', '2020-05-29 14:19:56', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0009', 'PO-0009', '2020-04-09', '2020-04-10', null, 'PT.Danbo EN', null, null, null, '2004', null, '90000', '0', '', 'in', '0', '2020-05-17 14:45:59', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0010', 'PO-0010', '2020-04-08', '2020-04-11', 'avtur', 'PT.Luhut Bintaro', null, null, null, '2000', '900', '100000', '0', '', 'in', '1', '2020-05-18 18:07:34', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0011', 'PO-0011', '2020-04-07', '2020-04-10', 'Avtur', 'PT.Luhut EX', null, null, null, '106', '100', '1000', '106000', '', 'in', '1', '2020-05-17 20:12:55', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0012', 'PO-0012', '2020-04-06', '2020-04-10', 'Solar', 'pemerintah', null, null, null, '10000', '10000', '8000', '80000000', '', 'in', '1', '2020-05-16 19:58:46', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0013', 'PO-0013', '2020-05-22', '2020-05-22', 'Korupsi', 'PT.Luhut EX', null, null, null, '100', '10', '1000', '100000', '', 'in', '1', '2020-05-23 14:37:59', '0');
INSERT INTO `tb_fuel_in` VALUES ('IN-0014', 'PO-0014', '2020-06-24', '2020-07-01', 'sfaf', 'ads', 'Jl. malik', '081736492754', '12471-7129-1248', '1400', null, '9000', '12600000', 'hello', 'in', '0', '2020-06-25 15:08:04', '0');

-- ----------------------------
-- Table structure for tb_fuel_out
-- ----------------------------
DROP TABLE IF EXISTS `tb_fuel_out`;
CREATE TABLE `tb_fuel_out` (
  `fuel_out_id` varchar(15) NOT NULL,
  `fuel_out_date` date DEFAULT NULL,
  `fuel_out_name` varchar(60) DEFAULT NULL,
  `fuel_out_unit` varchar(30) DEFAULT NULL,
  `fuel_out_quantity` int(10) DEFAULT NULL,
  `fuel_out_info` text,
  `fuel_out_status` enum('in','out') DEFAULT 'out',
  `is_delete_fuel_out` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`fuel_out_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_fuel_out
-- ----------------------------
INSERT INTO `tb_fuel_out` VALUES ('OUT-0001', '2020-04-08', 'Sell to PT.Bambang I', 'U-0003', '300', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0002', '2020-04-07', 'Sell to PT.Joko Purn', 'U-0011', '600', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0003', '2020-04-08', 'Logistik', 'U-0001', '300', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0004', '2020-04-09', 'Sell PT.Doge EN', 'U-0017', '600', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0005', '2020-04-08', 'Logistik', 'U-0003', '500', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0006', '2020-04-08', 'Logistik', 'U-0011', '200', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0007', '2020-04-08', 'Sell to PT.MAju Bers', 'U-0011', '600', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0008', '2020-04-09', 'Logistik', 'U-0017', '400', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0009', '2020-04-08', 'Logistik', 'U-0011', '300', null, 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0010', '2020-04-09', 'Sell to PT Berkabung', 'U-0009', '205', '', 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0011', '2020-04-06', 'Dayyan Syehan Al Akbar', 'U-0001', '200', 'kode merah', 'out', '1');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0012', '2020-04-06', 'sell to kecamatan sukun', 'U-0003', '100', '', 'out', '1');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0013', '2020-04-11', 'Logistik antar pulau', 'U-0017', '1000', '', 'out', '0');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0014', '2020-05-05', 'adw', 'U-0006', '1223', 'dawdaw', 'out', '1');
INSERT INTO `tb_fuel_out` VALUES ('OUT-0015', '2020-06-02', 'Makar', 'U-0003', '2000', '', 'out', '0');

-- ----------------------------
-- Table structure for tb_history
-- ----------------------------
DROP TABLE IF EXISTS `tb_history`;
CREATE TABLE `tb_history` (
  `history_id` varchar(40) NOT NULL,
  `user_input` varchar(100) DEFAULT NULL,
  `action_description` varchar(100) DEFAULT NULL,
  `date` year(4) DEFAULT NULL,
  `original_table` varchar(100) DEFAULT NULL,
  `action_type` varchar(30) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  PRIMARY KEY (`history_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_history
-- ----------------------------
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:50:00', 'USR-0001', 'Create Incoming dengan Nomor : IN-0011', '2020', 'tb_fuel_in', 'Create', '2020-04-15 10:50:00');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:52:58', 'USR-0001', 'Update Incoming dengan Nomor : IN-0011', '2020', 'tb_fuel_in', 'Update', '2020-04-15 10:52:58');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:54:06', 'USR-0001', 'Create Material dengan Nomor : MTR-0013', '2020', 'tb_material', 'Create', '2020-04-15 10:54:06');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:54:25', 'USR-0001', 'Update Material dengan Nomor : MTR-0013', '2020', 'tb_material', 'Update', '2020-04-15 10:54:25');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:55:12', 'USR-0001', 'Create Operation Cost dengan Nomor : OCO-0018', '2020', 'tb_operation_cost', 'Create', '2020-04-15 10:55:12');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:56:43', 'USR-0001', 'Create Operation Expenses dengan Nomor : OEX-0013', '2020', 'tb_operation_expenses', 'Create', '2020-04-15 10:56:43');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:57:03', 'USR-0001', 'Update Operation Expenses dengan Nomor : OEX-0013', '2020', 'tb_operation_expenses', 'Update', '2020-04-15 10:57:03');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:58:04', 'USR-0001', 'Create Customer dengan Nomor : CUST-0013', '2020', 'tb_costumer', 'Create', '2020-04-15 10:58:04');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:59:12', 'USR-0001', 'Create Incoming dengan Nomor : IN-0012', '2020', 'tb_fuel_in', 'Create', '2020-04-15 10:59:12');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 10:59:36', 'USR-0001', 'Update Incoming dengan Nomor : IN-0012', '2020', 'tb_fuel_in', 'Update', '2020-04-15 10:59:36');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 11:00:03', 'USR-0001', 'Create Outgoing dengan Nomor : OUT-0013', '2020', 'tb_fuel_out', 'Create', '2020-04-15 11:00:03');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 11:00:33', 'USR-0001', 'Create Sales dengan Nomor : SLS-0014', '2020', 'tb_sales', 'Create', '2020-04-15 11:00:33');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 11:00:55', 'USR-0001', 'Update Sales dengan Nomor : SLS-0014', '2020', 'tb_sales', 'Update', '2020-04-15 11:00:55');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 11:01:22', 'USR-0001', 'Update Sales dengan Nomor : SLS-0014', '2020', 'tb_sales', 'Update', '2020-04-15 11:01:22');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 11:01:29', 'USR-0001', 'Create Invoice dengan Nomor : 0014/04/INV/2020', '2020', 'tb_incoive', 'Create', '2020-04-15 11:01:29');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 12:52:53', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0014', '2020', 'tb_cash_in', 'Update', '2020-04-15 12:52:53');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 12:56:43', 'USR-0001', 'Delete Cash Out dengan Nomor : COU-0009', '2020', 'tb_cash_out', 'Delete', '2020-04-15 12:56:43');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 13:52:13', 'USR-0001', 'Update Status Goods Receive Incoming dengan Nomor : IN-0012', '2020', 'tb_fuel_in', 'Update', '2020-04-15 13:52:13');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 14:46:20', 'USR-0001', 'Create Sales dengan Nomor : SLS-0015', '2020', 'tb_sales', 'Create', '2020-04-15 14:46:20');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 14:49:28', 'USR-0001', 'Delete Material dengan Nomor : MTR-0013', '2020', 'tb_material', 'Delete', '2020-04-15 14:49:28');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 14:50:34', 'USR-0001', 'Update Status Goods Receive Incoming dengan Nomor : IN-0005', '2020', 'tb_fuel_in', 'Update', '2020-04-15 14:50:34');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-15 14:50:42', 'USR-0001', 'Update Status Goods Receive Incoming dengan Nomor : IN-0001', '2020', 'tb_fuel_in', 'Update', '2020-04-15 14:50:42');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-22 16:09:43', 'USR-0001', 'Update User ( petugas ) dengan Nomor : USR-0003', '2020', 'tb_material', 'Update', '2020-04-22 16:09:43');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-22 16:10:31', 'USR-0001', 'Update User ( petugas ) dengan Nomor : USR-0004', '2020', 'tb_material', 'Update', '2020-04-22 16:10:31');
INSERT INTO `tb_history` VALUES ('HSTR/2020-04-22 16:10:41', 'USR-0001', 'Update User ( guest ) dengan Nomor : USR-0005', '2020', 'tb_material', 'Update', '2020-04-22 16:10:41');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-12 20:26:33', 'USR-0002', 'Delete Cash In dengan Nomor : CIN-0010', '2020', 'tb_cash_in', 'Delete', '2020-05-12 20:26:33');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-12 20:28:30', 'USR-0002', 'Delete Cash In dengan Nomor : CIN-0014', '2020', 'tb_cash_in', 'Delete', '2020-05-12 20:28:30');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-12 20:28:44', 'USR-0002', 'Create Cash In dengan Nomor : CIN-0015', '2020', 'tb_cash_in', 'Create', '2020-05-12 20:28:44');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-12 20:29:06', 'USR-0002', 'Create Cash In dengan Nomor : CIN-0016', '2020', 'tb_cash_in', 'Create', '2020-05-12 20:29:06');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-12 20:29:22', 'USR-0002', 'Update Cash In dengan Nomor : CIN-0015', '2020', 'tb_cash_in', 'Update', '2020-05-12 20:29:22');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-12 20:45:41', 'USR-0002', 'Delete Cash Out dengan Nomor : COU-0012', '2020', 'tb_cash_out', 'Delete', '2020-05-12 20:45:41');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-12 20:48:27', 'USR-0002', 'Create Cash In dengan Nomor : CIN-0017', '2020', 'tb_cash_in', 'Create', '2020-05-12 20:48:27');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-12 21:19:51', 'USR-0002', 'Create Cash In dengan Nomor : CIN-0018', '2020', 'tb_cash_in', 'Create', '2020-05-12 21:19:51');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:16:24', '', 'Create PIT location dengan Nomor : ', '2020', 'tb_operation_cost', 'Create', '2020-05-13 01:16:24');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:21:08', '', 'Delete Machine dengan Nomor : ', '2020', 'tb_machine', 'Create', '2020-05-13 01:21:08');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:23:02', '', 'Delete Balance Sheet dengan Nomor : ', '2020', 'tb_balance', 'Create', '2020-05-13 01:23:02');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:24:00', '', 'Delete Machine Type dengan Nomor : ', '2020', 'tb_unit', 'Create', '2020-05-13 01:24:00');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:25:32', '', 'Delete Balance Sheet dengan Nomor : ', '2020', 'tb_balance', 'Create', '2020-05-13 01:25:32');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:25:44', '', 'Delete Machine Type dengan Nomor : ', '2020', 'tb_unit', 'Create', '2020-05-13 01:25:44');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:29:48', 'PIT-0010', 'Delete PIT location dengan Nomor : ', '2020', 'tb_location', 'Delete', '2020-05-13 01:29:48');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:30:44', 'PIT-0009', 'Delete PIT location dengan Nomor : ', '2020', 'tb_location', 'Delete', '2020-05-13 01:30:44');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:34:08', 'OCO-0007', 'Create Operation Cost dengan Nomor : ', '2020', 'tb_operation_cost', 'Create', '2020-05-13 01:34:08');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 01:51:32', 'USR-0002', 'Create Operation Cost dengan Nomor : PIT-0011', '2020', 'tb_operation_cost', 'Create', '2020-05-13 01:51:32');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 02:04:30', 'USR-0002', 'Create Operation Cost dengan Nomor : OCO-0019', '2020', 'tb_operation_cost', 'Create', '2020-05-13 02:04:30');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 02:04:39', 'OCO-0019', 'Create Operation Cost dengan Nomor : ', '2020', 'tb_operation_cost', 'Create', '2020-05-13 02:04:39');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 02:05:18', 'USR-0002', 'Create Operation Cost dengan Nomor : PIT-0011', '2020', 'tb_operation_cost', 'Create', '2020-05-13 02:05:18');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 02:38:02', 'USR-0002', 'Create PIT location dengan Nomor : PIT-0011', '2020', 'tb_location', 'Create', '2020-05-13 02:38:02');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 02:38:11', 'PIT-0011', 'Delete PIT location dengan Nomor : ', '2020', 'tb_location', 'Delete', '2020-05-13 02:38:11');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 02:55:21', 'USR-0002', 'Create Balance Sheet dengan Nomor : BS-0011', '2020', 'tb_balance', 'Create', '2020-05-13 02:55:21');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 02:55:29', 'BS-0011', 'Delete Balance Sheet dengan Nomor : ', '2020', 'tb_balance', 'Delete', '2020-05-13 02:55:29');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 03:09:21', 'USR-0002', 'Create Machine type dengan Nomor : M-0011', '2020', 'tb_unit', 'Create', '2020-05-13 03:09:21');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 03:09:27', 'M-0011', 'Delete Machine Type dengan Nomor : ', '2020', 'tb_unit', 'Delete', '2020-05-13 03:09:27');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 14:21:00', 'USR-0001', 'Edit PIT Location dengan Nomor : ', '2020', 'tb_location', 'Edit', '2020-05-13 14:21:00');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 14:23:42', 'USR-0001', 'Edit PIT Location dengan Nomor : ', '2020', 'tb_location', 'Edit', '2020-05-13 14:23:42');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 14:25:16', 'USR-0001', 'Create Sales dengan Nomor : SLS-0016', '2020', 'tb_sales', 'Create', '2020-05-13 14:25:16');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 14:26:14', 'USR-0001', 'Create Invoice dengan Nomor : 0015/05/INV/2020', '2020', 'tb_incoive', 'Create', '2020-05-13 14:26:14');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 14:40:07', 'USR-0001', 'Edit PIT Location dengan Nomor : ', '2020', 'tb_location', 'Edit', '2020-05-13 14:40:07');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 14:54:18', 'USR-0001', 'Edit Balance Sheet dengan Nomor : ', '2020', 'tb_balance', 'Edit', '2020-05-13 14:54:18');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 14:56:26', 'USR-0001', 'Edit Balance Sheet dengan Nomor : BS-0009', '2020', 'tb_balance', 'Edit', '2020-05-13 14:56:26');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 14:56:37', 'USR-0001', 'Edit Balance Sheet dengan Nomor : BS-0009', '2020', 'tb_balance', 'Edit', '2020-05-13 14:56:37');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 15:21:37', 'USR-0001', 'Edit Balance Sheet dengan Nomor : M-0007', '2020', 'tb_unit', 'Edit', '2020-05-13 15:21:37');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 15:23:01', 'USR-0001', 'Edit Balance Sheet dengan Nomor : M-0001', '2020', 'tb_unit', 'Edit', '2020-05-13 15:23:01');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 15:24:06', 'USR-0001', 'Edit Balance Sheet dengan Nomor : M-0011', '2020', 'tb_unit', 'Edit', '2020-05-13 15:24:06');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 15:24:17', 'M-0011', 'Delete Machine Type dengan Nomor : ', '2020', 'tb_unit', 'Delete', '2020-05-13 15:24:17');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 15:38:44', 'USR-0001', 'Create Machine utilization dengan Nomor : MU-0011', '2020', 'tb_machine', 'Create', '2020-05-13 15:38:44');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 15:45:53', 'USR-0001', 'Create Invoice dengan Nomor : 0016/05/INV/2020', '2020', 'tb_incoive', 'Create', '2020-05-13 15:45:53');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:26:24', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0011', '2020', 'tb_machine', 'Edit', '2020-05-13 16:26:24');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:26:51', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0011', '2020', 'tb_machine', 'Edit', '2020-05-13 16:26:51');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:29:19', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0011', '2020', 'tb_machine', 'Edit', '2020-05-13 16:29:19');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:29:51', 'USR-0001', 'Create Machine utilization dengan Nomor : MU-0012', '2020', 'tb_machine', 'Create', '2020-05-13 16:29:51');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:30:10', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0012', '2020', 'tb_machine', 'Edit', '2020-05-13 16:30:10');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:30:54', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0012', '2020', 'tb_machine', 'Edit', '2020-05-13 16:30:54');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:31:02', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0011', '2020', 'tb_machine', 'Edit', '2020-05-13 16:31:02');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:31:55', 'USR-0001', 'Delete Material dengan Nomor : MTR-0011', '2020', 'tb_material', 'Delete', '2020-05-13 16:31:55');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-13 16:33:41', 'USR-0001', 'Delete Customer dengan Nomor : CUST-0010', '2020', 'tb_costumer', 'Delete', '2020-05-13 16:33:41');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 16:38:21', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0019', '2020', 'tb_cash_in', 'Create', '2020-05-14 16:38:21');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 16:49:29', 'USR-0001', 'Create Sales dengan Nomor : SLS-0017', '2020', 'tb_sales', 'Create', '2020-05-14 16:49:29');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 16:56:02', 'USR-0001', 'Create Sales dengan Nomor : SLS-0018', '2020', 'tb_sales', 'Create', '2020-05-14 16:56:02');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 17:10:21', 'USR-0001', 'Create Sales dengan Nomor : SLS-0019', '2020', 'tb_sales', 'Create', '2020-05-14 17:10:21');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 17:11:35', 'USR-0001', 'Create Sales dengan Nomor : SLS-0020', '2020', 'tb_sales', 'Create', '2020-05-14 17:11:35');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 22:15:30', 'USR-0001', 'Create Sales dengan Nomor : SLS-0021', '2020', 'tb_sales', 'Create', '2020-05-14 22:15:30');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 22:17:33', 'USR-0001', 'Update Sales dengan Nomor : SLS-0021', '2020', 'tb_sales', 'Update', '2020-05-14 22:17:33');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 22:21:45', 'USR-0001', 'Update Sales dengan Nomor : SLS-0021', '2020', 'tb_sales', 'Update', '2020-05-14 22:21:45');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 22:24:22', 'USR-0001', 'Update Sales dengan Nomor : SLS-0021', '2020', 'tb_sales', 'Update', '2020-05-14 22:24:22');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 22:51:55', 'USR-0001', 'Create User ( officer ) dengan ID : USR-0006', '2020', 'tb_admin', 'Create', '2020-05-14 22:51:55');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 22:52:08', 'USR-0001', 'Update User ( admin ) dengan Nomor : USR-0006', '2020', 'tb_material', 'Update', '2020-05-14 22:52:08');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 22:52:17', 'USR-0001', 'Update User ( admin ) dengan Nomor : USR-0006', '2020', 'tb_material', 'Update', '2020-05-14 22:52:17');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-14 22:52:27', 'USR-0001', 'Delete user dengan ID : USR-0006', '2020', 'tb_admin', 'Delete', '2020-05-14 22:52:27');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 00:37:25', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0017', '2020', 'tb_cash_in', 'Update', '2020-05-15 00:37:25');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 00:37:35', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0018', '2020', 'tb_cash_in', 'Update', '2020-05-15 00:37:35');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 00:44:11', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0020', '2020', 'tb_cash_in', 'Create', '2020-05-15 00:44:11');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 00:45:48', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0020', '2020', 'tb_cash_in', 'Update', '2020-05-15 00:45:48');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 16:00:25', 'USR-0001', 'Create Machine type dengan Nomor : M-0012', '2020', 'tb_unit', 'Create', '2020-05-15 16:00:25');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 16:08:15', 'USR-0001', 'Create Sales dengan Nomor : SLS-0022', '2020', 'tb_sales', 'Create', '2020-05-15 16:08:15');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 16:15:27', 'USR-0001', 'Create Machine type dengan Nomor : M-0013', '2020', 'tb_unit', 'Create', '2020-05-15 16:15:27');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 16:16:00', 'M-0013', 'Delete Machine Type dengan Nomor : ', '2020', 'tb_unit', 'Delete', '2020-05-15 16:16:00');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 16:16:16', 'USR-0001', 'Create Machine type dengan Nomor : M-0014', '2020', 'tb_unit', 'Create', '2020-05-15 16:16:16');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 16:25:16', 'USR-0001', 'Update User ( petugas ) dengan Nomor : USR-0005', '2020', 'tb_material', 'Update', '2020-05-15 16:25:16');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-15 16:25:57', 'USR-0001', 'Update User ( officer ) dengan Nomor : USR-0005', '2020', 'tb_material', 'Update', '2020-05-15 16:25:57');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 18:47:16', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0021', '2020', 'tb_cash_in', 'Create', '2020-05-16 18:47:16');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 19:20:56', 'USR-0001', 'Create Outgoing dengan Nomor : OUT-0014', '2020', 'tb_fuel_out', 'Create', '2020-05-16 19:20:56');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 19:25:01', 'USR-0001', 'Update Outgoing dengan Nomor : OUT-0014', '2020', 'tb_fuel_out', 'Update', '2020-05-16 19:25:01');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 19:25:44', 'USR-0001', 'Update Outgoing dengan Nomor : OUT-0014', '2020', 'tb_fuel_out', 'Update', '2020-05-16 19:25:44');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 19:26:01', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0012', '2020', 'tb_machine', 'Edit', '2020-05-16 19:26:01');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 19:26:36', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0012', '2020', 'tb_machine', 'Edit', '2020-05-16 19:26:36');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 19:26:48', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0012', '2020', 'tb_machine', 'Edit', '2020-05-16 19:26:48');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 19:27:47', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0012', '2020', 'tb_machine', 'Edit', '2020-05-16 19:27:47');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 19:27:58', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0011', '2020', 'tb_machine', 'Edit', '2020-05-16 19:27:58');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 20:03:02', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0021', '2020', 'tb_cash_in', 'Update', '2020-05-16 20:03:02');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 20:03:11', 'USR-0001', 'Delete Cash In dengan Nomor : CIN-0017', '2020', 'tb_cash_in', 'Delete', '2020-05-16 20:03:11');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 20:03:17', 'USR-0001', 'Delete Cash In dengan Nomor : CIN-0018', '2020', 'tb_cash_in', 'Delete', '2020-05-16 20:03:17');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 20:22:30', 'USR-0001', 'Create Invoice dengan Nomor : 0017/05/INV/2020', '2020', 'tb_incoive', 'Create', '2020-05-16 20:22:30');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 20:28:39', 'USR-0001', 'Update Sales dengan Nomor : SLS-0021', '2020', 'tb_sales', 'Update', '2020-05-16 20:28:39');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 20:32:02', 'USR-0001', 'Update Sales dengan Nomor : SLS-0020', '2020', 'tb_sales', 'Update', '2020-05-16 20:32:02');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-16 20:51:48', 'USR-0001', 'Update Outgoing dengan Nomor : OUT-0014', '2020', 'tb_fuel_out', 'Update', '2020-05-16 20:51:48');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 14:50:07', 'USR-0001', 'Create Machine type dengan Nomor : M-0015', '2020', 'tb_unit', 'Create', '2020-05-17 14:50:07');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 14:50:41', 'USR-0001', 'Create Machine type dengan Nomor : M-0016', '2020', 'tb_unit', 'Create', '2020-05-17 14:50:41');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 14:51:15', 'M-0016', 'Delete Machine Type dengan Nomor : ', '2020', 'tb_unit', 'Delete', '2020-05-17 14:51:15');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 14:51:20', 'M-0015', 'Delete Machine Type dengan Nomor : ', '2020', 'tb_unit', 'Delete', '2020-05-17 14:51:20');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 15:05:54', 'USR-0001', 'Create Machine type dengan Nomor : M-0017', '2020', 'tb_unit', 'Create', '2020-05-17 15:05:54');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 15:22:37', 'USR-0001', 'Edit PIT Location dengan Nomor : ', '2020', 'tb_location', 'Edit', '2020-05-17 15:22:37');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 15:55:01', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0010', '2020', 'tb_machine', 'Edit', '2020-05-17 15:55:01');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 15:55:20', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0005', '2020', 'tb_machine', 'Edit', '2020-05-17 15:55:20');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 15:58:27', 'USR-0001', 'Create Machine utilization dengan Nomor : MU-0013', '2020', 'tb_machine', 'Create', '2020-05-17 15:58:27');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 15:59:54', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0009', '2020', 'tb_machine', 'Edit', '2020-05-17 15:59:54');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:00:04', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0008', '2020', 'tb_machine', 'Edit', '2020-05-17 16:00:04');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:00:10', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0002', '2020', 'tb_machine', 'Edit', '2020-05-17 16:00:10');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:00:18', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0001', '2020', 'tb_machine', 'Edit', '2020-05-17 16:00:18');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:02:26', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0005', '2020', 'tb_machine', 'Edit', '2020-05-17 16:02:26');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:02:58', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0005', '2020', 'tb_machine', 'Edit', '2020-05-17 16:02:58');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:05:56', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0009', '2020', 'tb_machine', 'Edit', '2020-05-17 16:05:56');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:06:24', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0005', '2020', 'tb_machine', 'Edit', '2020-05-17 16:06:24');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:09:05', 'USR-0001', 'Update Sales dengan Nomor : SLS-0022', '2020', 'tb_sales', 'Update', '2020-05-17 16:09:05');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:15:07', 'USR-0001', 'Update Outgoing dengan Nomor : OUT-0010', '2020', 'tb_fuel_out', 'Update', '2020-05-17 16:15:07');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:15:16', 'USR-0001', 'Delete Outgoing dengan Nomor : OUT-0014', '2020', 'tb_fuel_out', 'Delete', '2020-05-17 16:15:16');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:16:16', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0004', '2020', 'tb_machine', 'Edit', '2020-05-17 16:16:16');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:16:27', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0006', '2020', 'tb_machine', 'Edit', '2020-05-17 16:16:27');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:16:34', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0007', '2020', 'tb_machine', 'Edit', '2020-05-17 16:16:34');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:16:40', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0003', '2020', 'tb_machine', 'Edit', '2020-05-17 16:16:40');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:16:50', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0013', '2020', 'tb_machine', 'Edit', '2020-05-17 16:16:50');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 16:56:17', 'USR-0001', 'Create Sales dengan Nomor : SLS-0023', '2020', 'tb_sales', 'Create', '2020-05-17 16:56:17');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 17:02:44', 'USR-0001', 'Update Sales dengan Nomor : SLS-0023', '2020', 'tb_sales', 'Update', '2020-05-17 17:02:44');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 17:07:29', 'USR-0001', 'Update Sales dengan Nomor : SLS-0022', '2020', 'tb_sales', 'Update', '2020-05-17 17:07:29');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 20:11:40', 'USR-0001', 'Update Status Goods Receive Incoming dengan Nomor : IN-0012', '2020', 'tb_fuel_in', 'Update', '2020-05-17 20:11:40');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 20:11:50', 'USR-0001', 'Update Status Goods Receive Incoming dengan Nomor : IN-0012', '2020', 'tb_fuel_in', 'Update', '2020-05-17 20:11:50');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 20:12:56', 'USR-0001', 'Update Status Goods Receive Incoming dengan Nomor : IN-0011', '2020', 'tb_fuel_in', 'Update', '2020-05-17 20:12:56');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 22:11:38', 'USR-0001', 'Create Machine utilization dengan Nomor : MU-0014', '2020', 'tb_machine', 'Create', '2020-05-17 22:11:38');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 22:20:42', 'USR-0001', 'Create Machine utilization dengan Nomor : MU-0015', '2020', 'tb_machine', 'Create', '2020-05-17 22:20:42');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 22:26:38', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0014', '2020', 'tb_machine', 'Edit', '2020-05-17 22:26:38');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 22:26:58', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0014', '2020', 'tb_machine', 'Edit', '2020-05-17 22:26:58');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 22:27:22', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0015', '2020', 'tb_machine', 'Edit', '2020-05-17 22:27:22');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 22:27:49', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0014', '2020', 'tb_machine', 'Edit', '2020-05-17 22:27:49');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 22:27:56', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0015', '2020', 'tb_machine', 'Edit', '2020-05-17 22:27:56');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-17 22:40:14', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0021', '2020', 'tb_cash_in', 'Update', '2020-05-17 22:40:14');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-18 13:54:40', 'USR-0001', 'Create Cash Out dengan Nomor : COU-0013', '2020', 'tb_cash_out', 'Create', '2020-05-18 13:54:40');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-18 13:58:22', 'USR-0001', 'Create Cash Out dengan Nomor : COU-0014', '2020', 'tb_cash_out', 'Create', '2020-05-18 13:58:22');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-18 14:04:02', 'USR-0001', 'Create Cash Out dengan Nomor : COU-0015', '2020', 'tb_cash_out', 'Create', '2020-05-18 14:04:02');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-18 14:06:08', 'USR-0001', 'Edit Cash Out dengan Nomor : COU-0014', '2020', 'tb_cash_out', 'Edit', '2020-05-18 14:06:08');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-18 14:06:20', 'USR-0001', 'Edit Cash Out dengan Nomor : COU-0013', '2020', 'tb_cash_out', 'Edit', '2020-05-18 14:06:20');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-18 16:06:50', 'USR-0001', 'Edit PIT Location dengan Nomor : ', '2020', 'tb_location', 'Edit', '2020-05-18 16:06:50');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-18 16:36:01', 'USR-0001', 'Create Machine type dengan Nomor : M-0018', '2020', 'tb_unit', 'Create', '2020-05-18 16:36:01');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:23:15', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0022', '2020', 'tb_cash_in', 'Create', '2020-05-23 14:23:15');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:26:53', 'USR-0001', 'Create Sales dengan Nomor : SLS-0024', '2020', 'tb_sales', 'Create', '2020-05-23 14:26:53');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:27:25', 'USR-0001', 'Update Sales dengan Nomor : SLS-0024', '2020', 'tb_sales', 'Update', '2020-05-23 14:27:25');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:30:05', 'USR-0001', 'Update Sales dengan Nomor : SLS-0024', '2020', 'tb_sales', 'Update', '2020-05-23 14:30:05');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:30:17', 'USR-0001', 'Update Sales dengan Nomor : SLS-0023', '2020', 'tb_sales', 'Update', '2020-05-23 14:30:17');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:30:25', 'USR-0001', 'Create Invoice dengan Nomor : 0018/05/INV/2020', '2020', 'tb_incoive', 'Create', '2020-05-23 14:30:25');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:33:30', 'USR-0001', 'Update Sales dengan Nomor : SLS-0021', '2020', 'tb_sales', 'Update', '2020-05-23 14:33:30');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:35:57', 'USR-0001', 'Create Sales dengan Nomor : SLS-0025', '2020', 'tb_sales', 'Create', '2020-05-23 14:35:57');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:36:11', 'USR-0001', 'Update Sales dengan Nomor : SLS-0025', '2020', 'tb_sales', 'Update', '2020-05-23 14:36:11');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:36:20', 'USR-0001', 'Create Invoice dengan Nomor : 0019/05/INV/2020', '2020', 'tb_incoive', 'Create', '2020-05-23 14:36:20');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:37:35', 'USR-0001', 'Create Incoming dengan Nomor : IN-0013', '2020', 'tb_fuel_in', 'Create', '2020-05-23 14:37:35');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-23 14:47:44', 'USR-0001', 'Update Sales dengan Nomor : SLS-0002', '2020', 'tb_sales', 'Update', '2020-05-23 14:47:44');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-24 23:22:35', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0023', '2020', 'tb_cash_in', 'Create', '2020-05-24 23:22:35');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-24 23:24:53', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0023', '2020', 'tb_cash_in', 'Update', '2020-05-24 23:24:53');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-24 23:25:10', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0023', '2020', 'tb_cash_in', 'Update', '2020-05-24 23:25:10');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-24 23:26:06', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0023', '2020', 'tb_cash_in', 'Update', '2020-05-24 23:26:06');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-24 23:27:26', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0024', '2020', 'tb_cash_in', 'Create', '2020-05-24 23:27:26');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-24 23:27:37', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0024', '2020', 'tb_cash_in', 'Update', '2020-05-24 23:27:37');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-24 23:28:05', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0025', '2020', 'tb_cash_in', 'Create', '2020-05-24 23:28:05');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-24 23:32:35', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0026', '2020', 'tb_cash_in', 'Create', '2020-05-24 23:32:35');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 00:06:36', 'USR-0001', 'Update Cash In dengan Nomor : ', '2020', 'tb_cash_in', 'Update', '2020-05-25 00:06:36');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 00:29:44', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0027', '2020', 'tb_cash_in', 'Create', '2020-05-25 00:29:44');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 00:30:26', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0028', '2020', 'tb_cash_in', 'Create', '2020-05-25 00:30:26');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 00:40:57', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0008', '2020', 'tb_cash_in', 'Update', '2020-05-25 00:40:57');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 00:45:00', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0025', '2020', 'tb_cash_in', 'Update', '2020-05-25 00:45:00');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 15:26:16', 'USR-0001', 'Create Sales dengan Nomor : SLS-0026', '2020', 'tb_sales', 'Create', '2020-05-25 15:26:16');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 15:26:28', 'USR-0001', 'Update Sales dengan Nomor : SLS-0026', '2020', 'tb_sales', 'Update', '2020-05-25 15:26:28');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 15:26:35', 'USR-0001', 'Create Invoice dengan Nomor : 0020/05/INV/2020', '2020', 'tb_incoive', 'Create', '2020-05-25 15:26:35');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-25 15:27:15', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0029', '2020', 'tb_cash_in', 'Create', '2020-05-25 15:27:15');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-26 18:33:07', 'USR-0001', 'Update User ( admin ) dengan Nomor : USR-0001', '2020', 'tb_material', 'Update', '2020-05-26 18:33:07');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-26 18:33:35', 'USR-0001', 'Update User ( admin ) dengan Nomor : USR-0001', '2020', 'tb_material', 'Update', '2020-05-26 18:33:35');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 20:30:45', 'USR-0001', 'Create Sales dengan Nomor : SLS-0027', '2020', 'tb_sales', 'Create', '2020-05-29 20:30:45');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 20:30:58', 'USR-0001', 'Update Sales dengan Nomor : SLS-0027', '2020', 'tb_sales', 'Update', '2020-05-29 20:30:58');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 20:31:03', 'USR-0001', 'Create Invoice dengan Nomor : 0021/05/INV/2020', '2020', 'tb_incoive', 'Create', '2020-05-29 20:31:03');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 21:28:28', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0030', '2020', 'tb_cash_in', 'Create', '2020-05-29 21:28:28');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 21:28:48', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0030', '2020', 'tb_cash_in', 'Update', '2020-05-29 21:28:48');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 21:29:41', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0031', '2020', 'tb_cash_in', 'Create', '2020-05-29 21:29:41');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 21:30:03', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0031', '2020', 'tb_cash_in', 'Update', '2020-05-29 21:30:03');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 21:30:32', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0032', '2020', 'tb_cash_in', 'Create', '2020-05-29 21:30:32');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-29 21:31:14', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0032', '2020', 'tb_cash_in', 'Update', '2020-05-29 21:31:14');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-30 00:45:32', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0033', '2020', 'tb_cash_in', 'Create', '2020-05-30 00:45:32');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-30 00:45:55', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0033', '2020', 'tb_cash_in', 'Update', '2020-05-30 00:45:55');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-30 00:46:06', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0033', '2020', 'tb_cash_in', 'Update', '2020-05-30 00:46:06');
INSERT INTO `tb_history` VALUES ('HSTR/2020-05-30 00:46:24', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0034', '2020', 'tb_cash_in', 'Create', '2020-05-30 00:46:24');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-01 22:17:57', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0035', '2020', 'tb_cash_in', 'Create', '2020-06-01 22:17:57');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-01 22:18:10', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0035', '2020', 'tb_cash_in', 'Update', '2020-06-01 22:18:10');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-05 18:11:01', 'USR-0001', 'Create Outgoing dengan Nomor : OUT-0015', '2020', 'tb_fuel_out', 'Create', '2020-06-05 18:11:01');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-21 19:03:00', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0036', '2020', 'tb_cash_in', 'Create', '2020-06-21 19:03:00');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-22 14:15:45', 'USR-0001', 'Create User ( manager ) dengan ID : USR-0007', '2020', 'tb_admin', 'Create', '2020-06-22 14:15:45');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-22 20:31:04', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0037', '2020', 'tb_cash_in', 'Create', '2020-06-22 20:31:04');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-22 20:57:07', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0038', '2020', 'tb_cash_in', 'Create', '2020-06-22 20:57:07');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-24 00:58:16', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0038', '2020', 'tb_cash_in', 'Create', '2020-06-24 00:58:16');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-24 22:08:33', 'USR-0001', 'Create Cash In dengan Nomor : CIN-0038', '2020', 'tb_cash_in', 'Create', '2020-06-24 22:08:33');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 11:41:48', 'USR-0001', 'Update Sales dengan Nomor : SLS-0027', '2020', 'tb_sales', 'Update', '2020-06-25 11:41:48');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 11:41:59', 'USR-0001', 'Update Sales dengan Nomor : SLS-0027', '2020', 'tb_sales', 'Update', '2020-06-25 11:41:59');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 12:48:35', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0001', '2020', 'tb_cash_in', 'Update', '2020-06-25 12:48:35');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 12:49:59', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0038', '2020', 'tb_cash_in', 'Update', '2020-06-25 12:49:59');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 12:50:20', 'USR-0001', 'Delete Cash In dengan Nomor : CIN-0038', '2020', 'tb_cash_in', 'Delete', '2020-06-25 12:50:20');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 12:58:05', 'USR-0001', 'Create Machine utilization dengan Nomor : MU-0016', '2020', 'tb_machine', 'Create', '2020-06-25 12:58:05');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 13:02:00', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0016', '2020', 'tb_machine', 'Edit', '2020-06-25 13:02:00');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 13:03:09', 'USR-0001', 'Edit Machine utilization dengan Nomor : MU-0016', '2020', 'tb_machine', 'Edit', '2020-06-25 13:03:09');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 13:03:40', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0003', '2020', 'tb_cash_in', 'Update', '2020-06-25 13:03:40');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 13:04:00', 'USR-0001', 'Update Cash In dengan Nomor : CIN-0037', '2020', 'tb_cash_in', 'Update', '2020-06-25 13:04:00');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 15:07:03', 'USR-0001', 'Create Incoming dengan Nomor : IN-0014', '2020', 'tb_fuel_in', 'Create', '2020-06-25 15:07:03');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 15:07:41', 'USR-0001', 'Update Incoming dengan Nomor : IN-0014', '2020', 'tb_fuel_in', 'Update', '2020-06-25 15:07:41');
INSERT INTO `tb_history` VALUES ('HSTR/2020-06-25 15:08:04', 'USR-0001', 'Update Incoming dengan Nomor : IN-0014', '2020', 'tb_fuel_in', 'Update', '2020-06-25 15:08:04');

-- ----------------------------
-- Table structure for tb_invoice
-- ----------------------------
DROP TABLE IF EXISTS `tb_invoice`;
CREATE TABLE `tb_invoice` (
  `invoice_id` varchar(20) NOT NULL,
  `do_number` varchar(20) DEFAULT NULL,
  `spb_number` varchar(20) DEFAULT NULL,
  `invoice_cash_in_id` varchar(20) DEFAULT NULL,
  `invoice_sales_id` varchar(20) DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `invoice_costumer_id` varchar(20) DEFAULT NULL,
  `invoice_costumer_name` varchar(100) DEFAULT NULL,
  `invoice_costumer_type` varchar(20) DEFAULT NULL,
  `invoice_address` text,
  `invoice_city` varchar(20) DEFAULT NULL,
  `invoice_telephone` varchar(30) DEFAULT NULL,
  `invoice_fax` varchar(30) DEFAULT NULL,
  `invoice_police_number` varchar(20) DEFAULT NULL,
  `invoice_driver_name` varchar(100) DEFAULT NULL,
  `invoice_material_id` varchar(20) DEFAULT NULL,
  `invoice_material_type` varchar(20) DEFAULT NULL,
  `invoice_location` varchar(30) DEFAULT NULL,
  `invoice_payment` varchar(20) DEFAULT NULL,
  `invoice_amount` int(20) DEFAULT NULL,
  `invoice_insert_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete_invoice` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_invoice
-- ----------------------------
INSERT INTO `tb_invoice` VALUES ('0001/04/INV/2020', '0001/04/DO/2020', '0001/04/SPB/2020', 'CIN-0001', 'SLS-0001', '2020-04-07', 'CUST-0002', 'Nurul', 'Retail', 'Jl.Jendral sudirman no 16', 'Bogor', '0822123747479', '344312-23127-4542', 'N-12PDS', 'Dimas', 'MTR-0001', 'semen', 'bandung', 'cash', '1500000', '2020-04-14 18:00:06', '0');
INSERT INTO `tb_invoice` VALUES ('0002/04/INV/2020', '0002/04/DO/2020', '0002/04/SPB/2020', 'CIN-0002', 'SLS-0002', '2020-04-08', 'CUST-0006', 'Bowo', 'Distributor', 'Jl.Pahlawan no 96', 'Yogyakarta', '0872338457333', '323580-3231-2131', 'A-39DJS', 'Ovel', 'MTR-0010', 'plastik', 'denpasar', 'credit', '2100000', '2020-04-14 18:01:02', '0');
INSERT INTO `tb_invoice` VALUES ('0003/04/INV/2020', '0003/04/DO/2020', '0003/04/SPB/2020', 'CIN-0003', 'SLS-0003', '2020-04-08', 'CUST-0010', 'Kanji', 'Special retail', 'Jl.Soekarno no 19', 'Kupang', '0893747422185', '838382-2384-493', 'K-02SKD', 'Nur', 'MTR-0005', 'karet', 'kupang', 'cash', '500000', '2020-04-14 18:01:19', '0');
INSERT INTO `tb_invoice` VALUES ('0004/04/INV/2020', '0004/04/DO/2020', '0004/04/SPB/2020', 'CIN-0004', 'SLS-0004', '2020-04-09', 'CUST-0004', 'Luhut', 'Distributor', 'Jl.Proklamasi no 10', 'Jakarta', '0851233156483', '232131-1232-1235', 'O-231KS', 'Hadi', 'MTR-0004', 'baja', 'pontianak', 'credit', '700000', '2020-04-14 18:01:02', '0');
INSERT INTO `tb_invoice` VALUES ('0005/04/INV/2020', '0005/04/DO/2020', '0005/04/SPB/2020', 'CIN-0005', 'SLS-0005', '2020-04-06', 'CUST-0007', 'Kariem', 'Special retail', 'Jl.Madura no 44', 'Banyuwangi', '0852384758199', '382320-2323-1231', 'P-23WOS', 'Cholis', 'MTR-0003', 'pasir', 'jayapura', 'credit', '1200000', '2020-04-14 18:01:20', '0');
INSERT INTO `tb_invoice` VALUES ('0006/04/INV/2020', '0006/04/DO/2020', '0006/04/SPB/2020', 'CIN-0006', 'SLS-0006', '2020-04-04', 'CUST-0005', 'Karisma', 'Special retail', 'Jl.Karimun no 77', 'Jepara', '0872312321312', '232111-1238-2321', 'S-25DSO', 'Kholiq', 'MTR-0002', 'bata', 'Jakarta', 'cash', '2300000', '2020-04-14 18:01:21', '0');
INSERT INTO `tb_invoice` VALUES ('0007/04/INV/2020', '0007/04/DO/2020', '0007/04/SPB/2020', 'CIN-0007', 'SLS-0007', '2020-04-09', 'CUST-0008', 'Tito', 'Retail', 'Jl.Kupang no 66', 'Bandung', '0835757583399', '475849-3848-4849', 'A-23SDA', 'Plate', 'MTR-0007', 'batu', 'Surabaya', 'credit', '3200000', '2020-04-14 18:00:10', '0');
INSERT INTO `tb_invoice` VALUES ('0008/04/INV/2020', '0008/04/DO/2020', '0008/04/SPB/2020', 'CIN-0008', 'SLS-0008', '2020-04-08', 'CUST-0003', 'Joko', 'Distributor', 'Jl.Pejuang no 67', 'Surabaya', '0812332375751', '312311-23213-1231', 'T-12SDA', 'David', 'MTR-0006', 'kaca', 'Pontianak', 'cash', '200000', '2020-04-14 18:01:02', '0');
INSERT INTO `tb_invoice` VALUES ('0009/04/INV/2020', '0009/04/DO/2020', '0009/04/SPB/2020', 'CIN-0009', 'SLS-0009', '2020-04-07', 'CUST-0004', 'Luhut', 'Distributor', 'Jl.Proklamasi no 10', 'Jakarta', '0851233156483', '232131-1232-1235', 'U-10DSA', 'Agus', 'MTR-0008', 'aluminium', 'Banten', 'cash', '12500000', '2020-04-14 18:01:02', '0');
INSERT INTO `tb_invoice` VALUES ('0010/04/INV/2020', '0010/04/DO/2020', '0010/04/SPB/2020', 'CIN-0010', 'SLS-0010', '2020-04-06', 'CUST-0007', 'Kariem', 'Special retail', 'Jl.Madura no 44', 'Banyuwangi', '0852384758199', '382320-2323-1231', 'I-88SDKA', 'Bambang', 'MTR-0004', 'baja', 'Jakarta', 'cash', '6000000', '2020-04-14 18:01:22', '0');
INSERT INTO `tb_invoice` VALUES ('0011/04/INV/2020', '0011/04/DO/2020', '0011/04/SPB/2020', null, 'SLS-0011', '2020-04-14', 'CUST-0005', 'Karisma', 'Special retail', 'Jl.Karimun no 77', 'Jepara', '0872312321312', '232111-1238-2321', 'U 923112', 'Gan', 'MTR-0005', 'karet', 'Lampung', 'credit', '2000000', '2020-04-14 08:12:42', '0');
INSERT INTO `tb_invoice` VALUES ('0012/04/INV/2020', '0012/04/DO/2020', '0012/04/SPB/2020', null, 'SLS-0012', '2020-04-14', 'CUST-0006', 'Bowo', 'Distributor', 'Jl.Pahlawan no 96', 'Yogyakarta', '0872338457333', '323580-3231-2131', 'A 31233', 'Azaf', 'MTR-0008', 'aluminium', 'Ngawi', 'cash', '1000000', '2020-04-14 08:15:29', '0');
INSERT INTO `tb_invoice` VALUES ('0013/04/INV/2020', '0013/04/DO/2020', '0013/04/SPB/2020', null, 'SLS-0013', '2020-04-14', 'CUST-0008', 'Tito', 'Retail', 'Jl.Kupang no 66', 'Bandung', '0835757583399', '475849-3848-4849', 'Z 23394', 'Joni', 'MTR-0009', 'polimer', 'banten', 'credit', '1000000', '2020-04-14 09:33:54', '0');
INSERT INTO `tb_invoice` VALUES ('0014/04/INV/2020', '0014/04/DO/2020', '0014/04/SPB/2020', null, 'SLS-0014', '2020-04-15', 'CUST-0004', 'Luhut', 'Distributor', 'Jl.Proklamasi no 10', 'Jakarta', '0851233156483', '232131-1232-1235', 'K 3213', 'Binto', 'MTR-0008', 'aluminium', 'Serpong', 'cash', '3000000', '2020-04-15 11:01:29', '0');
INSERT INTO `tb_invoice` VALUES ('0015/05/INV/2020', '0016/05/DO/2020', '0015/05/SPB/2020', null, 'SLS-0016', '2020-05-13', 'CUST-0007', 'Kariem', 'Special retail', 'Jl.Madura no 44', 'Banyuwangi', '0852384758199', '382320-2323-1231', 'U 923112', 'sad', 'MTR-0005', 'karet', null, null, '0', '2020-05-13 02:26:14', '0');
INSERT INTO `tb_invoice` VALUES ('0016/05/INV/2020', '0015/04/DO/2020', '0016/05/SPB/2020', null, 'SLS-0015', '2020-05-13', 'CUST-0005', 'Karisma', 'Special retail', 'Jl.Karimun no 77', 'Jepara', '0872312321312', '232111-1238-2321', 'N1234MM', 'rufi', 'MTR-0011', 'pipa', null, null, null, '2020-05-13 03:45:53', '0');
INSERT INTO `tb_invoice` VALUES ('0017/05/INV/2020', '0022/05/DO/2020', '0017/05/SPB/2020', null, 'SLS-0022', '2020-05-16', 'CUST-0002', 'Nurul', 'Retail', 'Jl.Jendral sudirman no 16', 'Bogor', '0822123747479', '344312-23127-4542', 'dawawd', 'dawadw', 'MTR-0001', 'semen', null, 'kas', '1950000', '2020-05-16 08:22:30', '0');
INSERT INTO `tb_invoice` VALUES ('0018/05/INV/2020', '0024/05/DO/2020', '0018/05/SPB/2020', null, 'SLS-0024', '2020-05-23', 'CUST-0003', 'Joko', 'Distributor', 'Jl.Pejuang no 67', 'Surabaya', '0812332375751', '312311-23213-1231', '23ddsa', 'da', 'MTR-0003', 'pasir', 'Demak', 'cash', '1200000', '2020-05-23 02:30:25', '0');
INSERT INTO `tb_invoice` VALUES ('0019/05/INV/2020', '0025/05/DO/2020', '0019/05/SPB/2020', null, 'SLS-0025', '2020-05-23', 'CUST-0009', 'Joni', 'Retail', 'Jl.Jayapura no 46', 'Manokwari', '0863293293932', '348590-8483-8484', '12 sde0 00', 'man', 'MTR-0004', 'baja', 'Demak', 'cash', '800000', '2020-05-23 02:36:20', '0');
INSERT INTO `tb_invoice` VALUES ('0020/05/INV/2020', '0026/05/DO/2020', '0020/05/SPB/2020', null, 'SLS-0026', '2020-05-25', 'CUST-0003', 'Joko', 'Distributor', 'Jl.Pejuang no 67', 'Surabaya', '0812332375751', '312311-23213-1231', 'A 31233', 'ADA', 'MTR-0003', 'pasir', 'Ampat', 'cash', '144000', '2020-05-25 03:26:35', '0');
INSERT INTO `tb_invoice` VALUES ('0021/05/INV/2020', '0027/05/DO/2020', '0021/05/SPB/2020', null, 'SLS-0027', '2020-05-29', 'CUST-0004', 'Luhut', 'Distributor', 'Jl.Proklamasi no 10', 'Jakarta', '0851233156483', '232131-1232-1235', 'dfgad', 'sadda', 'MTR-0003', 'pasir', 'Kidam', 'cash', '144000', '2020-05-29 08:31:03', '0');

-- ----------------------------
-- Table structure for tb_location
-- ----------------------------
DROP TABLE IF EXISTS `tb_location`;
CREATE TABLE `tb_location` (
  `pit_id` varchar(20) NOT NULL,
  `pit_name` varchar(30) DEFAULT NULL,
  `pit_info` text,
  `is_delete_pit` enum('1','0') DEFAULT '0',
  `pit_status` enum('1','0') DEFAULT '0',
  `pit_created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`pit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_location
-- ----------------------------
INSERT INTO `tb_location` VALUES ('PIT-0001', 'Maruks', '                                            bruh               s                                                          ', '0', '0', '2020-05-12 01:49:30');
INSERT INTO `tb_location` VALUES ('PIT-0002', 'Lohor', 'karma                                                               ', '0', '0', '2020-04-28 01:49:34');
INSERT INTO `tb_location` VALUES ('PIT-0003', 'Demak', null, '0', '0', '2020-01-15 01:49:38');
INSERT INTO `tb_location` VALUES ('PIT-0004', 'Ampat', null, '0', '0', '2020-02-12 01:49:44');
INSERT INTO `tb_location` VALUES ('PIT-0005', 'Kidul', null, '0', '0', '2020-04-22 01:49:50');
INSERT INTO `tb_location` VALUES ('PIT-0006', 'Kidam', null, '0', '0', '2020-01-13 01:49:54');
INSERT INTO `tb_location` VALUES ('PIT-0007', 'Fosan', null, '0', '0', '2020-03-13 01:49:59');
INSERT INTO `tb_location` VALUES ('PIT-0008', 'Kediri', null, '0', '0', '2020-03-13 01:50:05');
INSERT INTO `tb_location` VALUES ('PIT-0009', 'Surabaya', null, '0', '0', '2020-05-05 01:50:08');
INSERT INTO `tb_location` VALUES ('PIT-0010', 'Malang', null, '0', '0', '2020-03-13 01:50:14');
INSERT INTO `tb_location` VALUES ('PIT-0011', 'asadsa', 'asdadada', '1', '0', '2020-05-13 02:38:02');

-- ----------------------------
-- Table structure for tb_machine
-- ----------------------------
DROP TABLE IF EXISTS `tb_machine`;
CREATE TABLE `tb_machine` (
  `machine_id` varchar(15) NOT NULL,
  `machine_date` date NOT NULL,
  `machine_unit` varchar(20) NOT NULL,
  `machine_model` varchar(20) NOT NULL,
  `machine_operator` varchar(50) NOT NULL,
  `machine_start` int(20) DEFAULT NULL,
  `machine_finish` int(20) DEFAULT NULL,
  `machine_range` int(15) NOT NULL DEFAULT '0',
  `machine_info` text,
  `is_delete_machine` enum('1','0') DEFAULT '0',
  `machine_created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`machine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_machine
-- ----------------------------
INSERT INTO `tb_machine` VALUES ('MU-0001', '2020-04-30', 'U-0003', 'Model-03', 'Bowo', '1000', '1300', '1000', '                                                                                    ', '0', '2020-04-30 02:53:26');
INSERT INTO `tb_machine` VALUES ('MU-0002', '2020-04-21', 'U-0004', 'Model-04', 'Owo', '1200', '1500', '1300', '                                                                                    ', '0', '2020-04-21 02:53:33');
INSERT INTO `tb_machine` VALUES ('MU-0003', '2020-03-23', 'U-0001', 'Model-01', 'Ozon', '1200', '1550', '350', '                                                                                    ', '0', '2020-03-23 02:53:42');
INSERT INTO `tb_machine` VALUES ('MU-0004', '2020-03-16', 'U-0004', 'Model-06', 'Poss', '1500', '1650', '150', '                                                                                    ', '0', '2020-03-16 02:53:48');
INSERT INTO `tb_machine` VALUES ('MU-0005', '2020-04-28', 'U-0017', 'Model-05', 'Pir', '1100', '1500', '400', '                                                                                                                                                                                                                                                                                                                                                ', '0', '2020-04-28 02:53:53');
INSERT INTO `tb_machine` VALUES ('MU-0006', '2020-01-01', 'U-0009', 'Model-09', 'Koha', '1400', '1600', '200', '                                                                                    ', '0', '2020-01-01 02:53:58');
INSERT INTO `tb_machine` VALUES ('MU-0007', '2020-02-25', 'U-0003', 'Model-03', 'Hadi', '1300', '1400', '100', '                                                                                    ', '0', '2020-02-25 02:54:05');
INSERT INTO `tb_machine` VALUES ('MU-0008', '2020-02-21', 'U-0004', 'Model-10', 'Jaka', '1331', '1650', '1321', '                                                                                    ', '0', '2020-02-21 02:54:10');
INSERT INTO `tb_machine` VALUES ('MU-0009', '2020-04-07', 'U-0008', 'Model-08', 'Joko', '1389', '1600', '211', '                                                                                                                                                                        ', '0', '2020-04-07 02:54:19');
INSERT INTO `tb_machine` VALUES ('MU-0010', '2020-02-02', 'U-0008', 'Model-01', 'Loss', '1002', '1400', '1392', '                                                                                    ', '0', '2020-02-02 02:54:24');
INSERT INTO `tb_machine` VALUES ('MU-0011', '2020-03-29', 'U-0017', '', 'aaaaaaaa', '222', '3333', '3111', '                                                                                        111111111                                                                                                                                              ', '0', '2020-05-13 15:38:44');
INSERT INTO `tb_machine` VALUES ('MU-0012', '2020-04-27', 'U-0003', '', 'sadads', '111111', '222222', '111111', '                                                                                                                                                                                                                                                                        ewewwaea                                                                                                                                                                                                                                                ', '0', '2020-05-13 16:29:51');
INSERT INTO `tb_machine` VALUES ('MU-0013', '2020-04-26', 'U-0003', '', 'sad', '23', '53', '30', '                                            ewae                                        ', '0', '2020-05-17 15:58:27');
INSERT INTO `tb_machine` VALUES ('MU-0014', '2020-05-03', 'U-0003', 'Model-03', 'pq45', '1233', '1500', '267', '                                                                                                                                                                                                                                                            ', '0', '2020-05-17 22:11:38');
INSERT INTO `tb_machine` VALUES ('MU-0015', '2020-05-02', 'U-0017', 'Model-17', 'ggfgfss', '1222', '4333', '3111', '                                                                                        2313fsfss                                                                                ', '0', '2020-05-17 22:20:42');
INSERT INTO `tb_machine` VALUES ('MU-0016', '2020-06-24', 'U-0001', 'Model-01', 'asasf', '10000', '100000', '90000', '', '0', '2020-06-25 12:58:05');

-- ----------------------------
-- Table structure for tb_material
-- ----------------------------
DROP TABLE IF EXISTS `tb_material`;
CREATE TABLE `tb_material` (
  `material_id` varchar(15) NOT NULL,
  `material_name` varchar(20) DEFAULT NULL,
  `retail_price` int(10) DEFAULT NULL,
  `special_price` int(10) DEFAULT NULL,
  `distributor_price` int(10) DEFAULT NULL,
  `material_stock` int(10) DEFAULT NULL,
  `material_info` text,
  `material_status` enum('0','1') DEFAULT NULL,
  `material_insert_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete_material` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_material
-- ----------------------------
INSERT INTO `tb_material` VALUES ('MTR-0001', 'semen', '130000', '150000', '120000', '1000', null, '1', '2020-04-09 14:41:31', '0');
INSERT INTO `tb_material` VALUES ('MTR-0002', 'bata', '24000', '25000', '22000', '5000', null, '1', '2020-04-09 14:43:36', '0');
INSERT INTO `tb_material` VALUES ('MTR-0003', 'pasir', '13000', '15000', '12000', '6000', null, '1', '2020-04-09 14:46:50', '0');
INSERT INTO `tb_material` VALUES ('MTR-0004', 'baja', '40000', '42000', '39000', '2000', null, '1', '2020-04-09 14:48:41', '0');
INSERT INTO `tb_material` VALUES ('MTR-0005', 'karet', '30000', '33000', '28000', '3000', null, '1', '2020-04-09 14:49:50', '0');
INSERT INTO `tb_material` VALUES ('MTR-0006', 'kaca', '60000', '63000', '59000', '1500', null, '1', '2020-04-08 14:50:40', '0');
INSERT INTO `tb_material` VALUES ('MTR-0007', 'batu', '20000', '23000', '18000', '3000', null, '1', '2020-04-14 14:26:40', '1');
INSERT INTO `tb_material` VALUES ('MTR-0008', 'aluminium', '28000', '30000', '23000', '4000', null, '1', '2020-04-08 14:52:39', '0');
INSERT INTO `tb_material` VALUES ('MTR-0009', 'polimer', '40000', '43000', '39000', '2000', null, '1', '2020-04-08 14:53:24', '0');
INSERT INTO `tb_material` VALUES ('MTR-0010', 'plastik', '13000', '15000', '12000', '3000', null, '1', '2020-04-13 09:12:53', '0');
INSERT INTO `tb_material` VALUES ('MTR-0011', 'pipa', '6000', '5000', '5000', '1400', 'pipa pvc ukuran 1 meter', '0', '2020-05-13 16:31:55', '1');
INSERT INTO `tb_material` VALUES ('MTR-0012', 'cat', '200000', '300000', '350000', '300', '', '0', '2020-04-14 16:03:32', '1');
INSERT INTO `tb_material` VALUES ('MTR-0013', 'Aspal', '200000', '230000', '180000', '1000', 'aspal untuk jalan', '0', '2020-05-13 16:32:10', '0');

-- ----------------------------
-- Table structure for tb_operation_cost
-- ----------------------------
DROP TABLE IF EXISTS `tb_operation_cost`;
CREATE TABLE `tb_operation_cost` (
  `ops_cost_id` varchar(15) NOT NULL,
  `ops_cost_name` varchar(60) DEFAULT NULL,
  `ops_cost_info` text,
  `cost_insert_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete_cost` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`ops_cost_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_operation_cost
-- ----------------------------
INSERT INTO `tb_operation_cost` VALUES ('OCO-0001', 'Pajak', 'pajak dari pemerintah', '2020-04-09 18:39:38', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0002', 'Maintenance', null, '2020-04-09 18:39:41', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0003', 'Gaji pegawai', null, '2020-04-09 18:39:44', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0004', 'Pembelian Bahan bakar', null, '2020-04-09 18:39:46', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0005', 'Fasilitas umum', null, '2020-04-09 18:39:48', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0006', 'Asuransi pegawai', null, '2020-04-09 18:39:50', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0007', 'Pembelian perangkat ', null, '2020-05-13 01:34:08', '1');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0008', 'Periklanan', null, '2020-04-14 14:26:51', '1');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0009', 'Biaya sewa', 'biaya untuk sewa bangunan', '2020-04-09 18:39:57', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0010', 'Konsumsi', 'Konsumsi pegawai', '2020-04-09 18:40:04', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0011', 'Lisensi produk', null, '2020-04-07 19:57:25', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0012', 'Biaya transportasi', null, '2020-04-14 15:57:35', '1');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0013', 'Kerusakan perangkat', null, '2020-04-09 19:57:31', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0014', 'Biaya suplai', '                  kampang                                                                  ', '2020-04-14 16:28:37', '1');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0015', 'Bunga bank', null, '2020-04-09 19:57:36', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0016', 'Pengambilan Barang', 'Biaya untuk pengambilan barang', '2020-04-13 09:51:16', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0017', 'biaya perlindungan', '', '2020-05-13 01:34:26', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0018', 'pesangon ormas', '', '2020-04-15 10:55:12', '0');
INSERT INTO `tb_operation_cost` VALUES ('OCO-0019', 'dasda', 'asdasdaa', '2020-05-13 02:04:39', '1');

-- ----------------------------
-- Table structure for tb_operation_exp
-- ----------------------------
DROP TABLE IF EXISTS `tb_operation_exp`;
CREATE TABLE `tb_operation_exp` (
  `ops_exp_id` varchar(15) NOT NULL,
  `ops_exp_name` varchar(60) DEFAULT NULL,
  `ops_exp_info` text,
  `exp_insert_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete_exp` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`ops_exp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_operation_exp
-- ----------------------------
INSERT INTO `tb_operation_exp` VALUES ('OEX-0001', 'Pembayaran lisensi', null, '2020-04-09 18:48:05', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0002', 'Pembayaran listrik', null, '2020-04-09 18:48:09', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0003', 'Penggantian produk', null, '2020-04-07 18:48:11', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0004', 'Pembayaran pajak', null, '2020-04-08 18:48:15', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0005', 'Pembayaran transport', null, '2020-04-08 18:48:19', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0006', 'Pembayaran iklan', null, '2020-04-08 18:48:23', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0007', 'Pembayaran asuransi', 'Pembayaran asuransi milik pegawai', '2020-04-08 18:48:28', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0008', 'Pembelian peralatan ', null, '2020-04-09 18:48:32', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0009', 'Pembayaran suku bunga', 'Pembayaran suku bunga bank perusahaan', '2020-04-09 18:48:35', '0');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0010', 'Pembayaran air', null, '2020-04-14 14:27:12', '1');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0011', 'Pembelian Konsumsi', '', '2020-04-14 15:57:51', '1');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0012', 'daw', 'asad', '2020-04-14 16:05:49', '1');
INSERT INTO `tb_operation_exp` VALUES ('OEX-0013', 'pembayaran untuk ormas', 'ormas FPI', '2020-04-15 10:57:03', '0');

-- ----------------------------
-- Table structure for tb_sales
-- ----------------------------
DROP TABLE IF EXISTS `tb_sales`;
CREATE TABLE `tb_sales` (
  `do_id` varchar(20) DEFAULT NULL,
  `do_status` enum('1','0') NOT NULL DEFAULT '0',
  `sales_invoice` varchar(20) DEFAULT NULL,
  `spb_status` enum('1','0') NOT NULL DEFAULT '0',
  `spb_id` varchar(20) DEFAULT NULL,
  `sales_id` varchar(15) NOT NULL,
  `sales_costumer_id` varchar(20) DEFAULT NULL,
  `sales_date` date DEFAULT NULL,
  `sales_name_costumer` varchar(100) DEFAULT NULL,
  `sales_amount` int(10) DEFAULT NULL,
  `sales_type_costumer` enum('Retail','Special retail','Distributor') DEFAULT NULL,
  `sales_vehicle_number` varchar(30) DEFAULT NULL,
  `sales_driver_name` varchar(255) DEFAULT NULL,
  `sales_material_id` varchar(20) DEFAULT NULL,
  `sales_material_amount` int(10) DEFAULT NULL,
  `material_type` varchar(15) DEFAULT NULL,
  `sales_due_date` date DEFAULT NULL,
  `sales_payment` varchar(20) DEFAULT NULL,
  `sales_term` varchar(10) DEFAULT NULL,
  `sales_location` varchar(30) DEFAULT NULL,
  `sales_status` enum('0','1') DEFAULT NULL,
  `sales_insert_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_delete_sales` enum('0','1') DEFAULT '0',
  `sales_act_status` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_sales
-- ----------------------------
INSERT INTO `tb_sales` VALUES ('0001/04/DO/2020', '0', '0001/07/INV/2020', '0', '0001/09/SPB/2020', 'SLS-0001', 'CUST-0002', '2020-04-07', 'Nurul', '1500000', 'Retail', 'N-12PDS', 'Dimas', 'MTR-0001', null, 'semen', '2020-04-11', 'cash', null, 'bandung', '0', '2020-05-25 00:30:26', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0002/04/DO/2020', '0', '0002/08/INV/2020', '0', '0002/08/SPB/2020', 'SLS-0002', 'CUST-0006', '2020-04-08', 'Bowo', '2100000', 'Distributor', 'A-39DJS', 'Ovel', 'MTR-0010', '9', 'plastik', '2020-04-09', 'credit', '1', 'Lohor', '1', '2020-05-23 14:47:44', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0003/04/DO/2020', '0', '0003/07/INV/2020', '1', '0003/07/SPB/2020', 'SLS-0003', 'CUST-0010', '2020-04-07', 'Kanji', '500000', 'Special retail', 'K-02SKD', 'Nur', 'MTR-0005', null, 'karet', '2020-04-08', 'cash', null, 'kupang', '1', '2020-05-23 14:34:09', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0004/04/DO/2020', '0', '0004/09/INV/2020', '0', '0004/09/SPB/2020', 'SLS-0004', 'CUST-0004', '2020-04-09', 'Luhut', '700000', 'Distributor', 'O-231KS', 'Hadi', 'MTR-0004', null, 'baja', '2020-04-10', 'credit', null, 'pontianak', '0', '2020-05-30 00:45:32', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0005/04/DO/2020', '1', '0001/06/INV/2020', '1', '0005/06/SPB/2020', 'SLS-0005', 'CUST-0007', '2020-04-06', 'Kariem', '1200000', 'Special retail', 'P-23WOS', 'Cholis', 'MTR-0003', null, 'pasir', '2020-04-09', 'credit', null, 'jayapura', '1', '2020-05-23 14:34:33', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0006/04/DO/2020', '0', '0001/04/INV/2020', '1', '0006/04/SPB/2020', 'SLS-0006', 'CUST-0005', '2020-04-04', 'Karisma', '2300000', 'Special retail', 'S-25DSO', 'Kholiq', 'MTR-0002', null, 'bata', '2020-04-07', 'cash', null, 'Jakarta', '1', '2020-05-23 14:34:10', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0007/04/DO/2020', '0', '0001/09/INV/2020', '0', '0007/09/SPB/2020', 'SLS-0007', 'CUST-0008', '2020-04-09', 'Tito', '3200000', 'Retail', 'A-23SDA', 'Plate', 'MTR-0007', null, 'batu', '2020-04-08', 'credit', null, 'Surabaya', '0', '2020-05-23 14:34:33', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0008/04/DO/2020', '0', '0001/09/INV/2020', '1', '0008/09/SPB/2020', 'SLS-0008', 'CUST-0003', '2020-04-08', 'Joko', '200000', 'Distributor', 'T-12SDA', 'David', 'MTR-0006', null, 'kaca', '2020-04-06', 'cash', null, 'Pontianak', '0', '2020-05-23 14:34:10', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0009/04/DO/2020', '0', '0001/07/INV/2020', '0', '0009/07/SPB/2020', 'SLS-0009', 'CUST-0004', '2020-04-07', 'Luhut', '12500000', 'Distributor', 'U-10DSA', 'Agus', 'MTR-0008', null, 'aluminium', '2020-04-02', 'credit', null, 'Banten', '0', '2020-05-25 00:30:26', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0010/04/DO/2020', '1', '0001/06/INV/2020', '0', '0010/06/SPB/2020', 'SLS-0010', 'CUST-0007', '2020-04-06', 'Kariem', '6000000', 'Special retail', 'I-88SDKA', 'Bambang', 'MTR-0004', null, 'baja', '2020-04-07', 'cash', null, 'Jakarta', '1', '2020-05-23 14:34:10', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0011/04/DO/2020', '0', '0011/04/INV/2020', '0', '0011/04/SPB/2020', 'SLS-0011', 'CUST-0005', '2020-04-06', 'Karisma', '2000000', 'Special retail', 'U 923112', 'Gan', 'MTR-0005', null, 'karet', '2020-04-06', 'credit', null, 'Lampung', '0', '2020-06-01 22:17:57', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0012/04/DO/2020', '0', '0012/04/INV/2020', '0', '0012/04/SPB/2020', 'SLS-0012', 'CUST-0006', '2020-04-01', 'Bowo', '1000000', 'Distributor', 'A 31233', 'Azaf', 'MTR-0008', null, 'aluminium', null, 'cash', null, 'Ngawi', '0', '2020-05-23 14:34:10', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0013/04/DO/2020', '0', '0013/04/INV/2020', '0', '0013/04/SPB/2020', 'SLS-0013', 'CUST-0008', '2020-04-06', 'Tito', '1000000', 'Retail', 'Z 23394', 'Joni', 'MTR-0009', null, 'polimer', '1970-01-01', 'credit', null, 'banten', '0', '2020-05-24 23:28:05', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0014/04/DO/2020', '1', '0014/04/INV/2020', '1', '0014/04/SPB/2020', 'SLS-0014', 'CUST-0004', '2020-04-09', 'Luhut', '3000000', 'Distributor', 'K 3213', 'Binto', 'MTR-0008', null, 'aluminium', '1970-01-01', 'cash', null, 'Serpong', '1', '2020-05-23 14:34:10', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0015/04/DO/2020', '0', '0016/05/INV/2020', '0', '0016/05/SPB/2020', 'SLS-0015', 'CUST-0005', '2020-04-15', 'Karisma', null, 'Special retail', 'N1234MM', 'rufi', 'MTR-0011', null, 'pipa', null, 'credit', null, null, '1', '2020-05-23 14:34:33', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0016/05/DO/2020', '0', '0015/05/INV/2020', '0', '0015/05/SPB/2020', 'SLS-0016', 'CUST-0007', '2020-04-27', 'Kariem', '0', 'Special retail', 'U 923112', 'sad', 'MTR-0005', null, 'karet', null, 'credit', null, null, '1', '2020-05-23 14:34:33', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0017/05/DO/2020', '0', null, '0', null, 'SLS-0017', 'CUST-0004', '2020-05-19', 'Luhut', '39000', 'Distributor', '12a sdasa', 'asddffff', 'MTR-0009', null, 'polimer', null, 'cash', null, null, '1', '2020-05-29 14:25:28', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0018/05/DO/2020', '0', null, '0', null, 'SLS-0018', 'CUST-0006', '2020-05-01', 'Bowo', '28000', 'Distributor', 'ewadas', 'sadda', 'MTR-0005', null, 'karet', null, 'credit', null, null, '1', '2020-05-29 14:25:29', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0019/05/DO/2020', '0', null, '0', null, 'SLS-0019', 'CUST-0001', '2020-05-13', 'Bambang', '12000', 'Distributor', 'adw', 'addadw', 'MTR-0003', '10', 'pasir', null, 'cash', null, null, '1', '2020-05-23 14:34:10', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0020/05/DO/2020', '0', null, '0', null, 'SLS-0020', 'CUST-0010', '2020-05-12', 'Kanji', '0', 'Special retail', 'rrrrer', 'dadasad', 'MTR-0010', '6', 'plastik', '1970-01-01', 'cash', '', 'Kidul', '1', '2020-05-29 14:25:29', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0021/05/DO/2020', '0', null, '0', null, 'SLS-0021', 'CUST-0005', '2020-05-05', 'Karisma', '1000000', 'Special retail', 'dfdghfdsadsf', 'sdfsfsfsfd', 'MTR-0003', '14', 'pasir', '1970-01-01', 'credit', '', 'Demak', '1', '2020-05-23 14:34:33', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0022/05/DO/2020', '0', '0017/05/INV/2020', '0', '0017/05/SPB/2020', 'SLS-0022', 'CUST-0002', '2020-05-04', 'Nurul', '1950000', 'Retail', 'dawawd', 'dawadw', 'MTR-0001', '15', 'semen', '1970-01-01', 'credit', null, 'Demak', '0', '2020-05-29 14:26:29', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0023/05/DO/2020', '0', null, '0', null, 'SLS-0023', 'CUST-0008', '2020-05-05', 'Tito', '247000', 'Retail', 'aea', 'dsae', 'MTR-0003', '19', 'pasir', '1970-01-01', 'cash', null, 'Ampat', '1', '2020-05-29 14:26:12', '0', 'DO');
INSERT INTO `tb_sales` VALUES ('0024/05/DO/2020', '0', '0018/05/INV/2020', '0', '0018/05/SPB/2020', 'SLS-0024', 'CUST-0003', '2020-05-19', 'Joko', '1200000', 'Distributor', '23ddsa', 'da', 'MTR-0003', '100', 'pasir', '1970-01-01', 'cash', null, 'Demak', '0', '2020-05-24 23:27:26', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0025/05/DO/2020', '0', '0019/05/INV/2020', '0', '0019/05/SPB/2020', 'SLS-0025', 'CUST-0009', '2020-05-20', 'Joni', '800000', 'Retail', '12 sde0 00', 'man', 'MTR-0004', '20', 'baja', '1970-01-01', 'credit', null, 'Demak', '0', '2020-05-24 23:22:35', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0026/05/DO/2020', '0', '0020/05/INV/2020', '0', '0020/05/SPB/2020', 'SLS-0026', 'CUST-0003', '2020-05-24', 'Joko', '144000', 'Distributor', 'A 31233', 'ADA', 'MTR-0003', '12', 'pasir', '1970-01-01', 'cash', null, 'Ampat', '0', '2020-05-25 15:27:15', '0', 'SPB, INV');
INSERT INTO `tb_sales` VALUES ('0027/05/DO/2020', '0', '0021/05/INV/2020', '0', '0021/05/SPB/2020', 'SLS-0027', 'CUST-0004', '2020-05-29', 'Luhut', '144000', 'Distributor', 'dfgad', 'sadda', 'MTR-0003', '12', 'pasir', '1970-01-01', 'cash', null, 'Kidam', '0', '2020-06-25 11:41:59', '0', 'SPB, INV');

-- ----------------------------
-- Table structure for tb_unit
-- ----------------------------
DROP TABLE IF EXISTS `tb_unit`;
CREATE TABLE `tb_unit` (
  `unit_machine_id` varchar(20) NOT NULL,
  `unit_id` varchar(20) NOT NULL,
  `unit_model` varchar(30) NOT NULL,
  `unit_year` int(10) NOT NULL,
  `unit_owner` varchar(50) NOT NULL,
  `unit_rate` varchar(20) DEFAULT NULL,
  `unit_rental` varchar(20) DEFAULT NULL,
  `unit_time` varchar(10) NOT NULL,
  `unit_info` text NOT NULL,
  `is_delete_unit` enum('1','0') NOT NULL DEFAULT '0',
  `unit_created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`unit_machine_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tb_unit
-- ----------------------------
INSERT INTO `tb_unit` VALUES ('M-0001', 'U-0001', 'Model-01', '2005', 'Sumanto', null, '3', 'days', '                                            dadada                                        ', '0', '2020-05-05 02:52:12');
INSERT INTO `tb_unit` VALUES ('M-0002', 'U-0002', 'Model-02', '2010', 'Feri', null, '3', 'weeks', '', '0', '2020-02-26 02:52:16');
INSERT INTO `tb_unit` VALUES ('M-0003', 'U-0003', 'Model-03', '2012', 'Sujarwo', null, '2', 'weeks', '', '0', '2020-02-28 02:52:20');
INSERT INTO `tb_unit` VALUES ('M-0004', 'U-0004', 'Model-04', '2009', 'Andik', null, '4', 'days', '', '0', '2020-03-10 02:52:24');
INSERT INTO `tb_unit` VALUES ('M-0005', 'U-0005', 'Model-05', '2019', 'Jarot', null, '3', 'days', '', '0', '2020-04-08 02:52:28');
INSERT INTO `tb_unit` VALUES ('M-0006', 'U-0006', 'Model-06', '2018', 'Yoga', null, '3', 'weeks', '', '0', '2020-01-06 02:52:33');
INSERT INTO `tb_unit` VALUES ('M-0007', 'U-0017', 'Model-17', '2006', 'Lote', null, '3', 'hours', '        addada                                                                            ', '0', '2020-02-11 02:52:38');
INSERT INTO `tb_unit` VALUES ('M-0008', 'U-0008', 'Model-08', '2007', 'Kamil', null, '23', 'hours', '', '0', '2020-02-04 02:52:42');
INSERT INTO `tb_unit` VALUES ('M-0009', 'U-0009', 'Model-09', '2013', 'Fasat', null, '3', 'days', '', '0', '2020-01-15 02:52:46');
INSERT INTO `tb_unit` VALUES ('M-0010', 'U-0010', 'Model-10', '2016', 'Aidit', null, '2', 'months', '', '0', '2020-03-17 02:52:51');
INSERT INTO `tb_unit` VALUES ('M-0011', 'U-0011', 'Model-11', '11111', 'aaaa', null, '1', 'months', 'aaaaaaaaaaaaaa                         ', '1', '2020-05-13 03:09:20');
INSERT INTO `tb_unit` VALUES ('M-0012', '3123', 'adsdasads', '1233', 'adwdaaw', null, '2', 'days', 'adwad', '1', '2020-05-15 16:00:25');
INSERT INTO `tb_unit` VALUES ('M-0013', 'dsadasd', 'fdfs', '1233', 'dadeeew', null, '2', 'weeks', '', '1', '2020-05-15 16:15:27');
INSERT INTO `tb_unit` VALUES ('M-0014', 'adwdaw', 'sadasa', '2323', 'dawaw', null, '12', 'weeks', '', '1', '2020-05-15 16:16:15');
INSERT INTO `tb_unit` VALUES ('M-0015', 'dawdwa', 'sdasda', '5310', 'feeefe', null, null, 'days', '', '1', '2020-05-17 14:50:07');
INSERT INTO `tb_unit` VALUES ('M-0016', 'adwawd', 'sds', '1232', 'dwaad', null, null, 'hour', '', '1', '2020-05-17 14:50:41');
INSERT INTO `tb_unit` VALUES ('M-0017', '2131', 'wdaadw', '2332', 'daw', '231 days', '231', 'days', '313123', '0', '2020-05-17 15:05:54');
INSERT INTO `tb_unit` VALUES ('M-0018', 'U-0020', 'Model-20', '2010', 'Parno', '20 weeks', '20', 'weeks', 'Off', '0', '2020-05-18 16:36:01');
